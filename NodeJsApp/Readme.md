#Install NodeJS

sudo apt-get update

sudo apt-get install nodejs

sudo apt-get install npm

npm install -g nodemon

cd appNode

npm install

#Run
nodemon Server.js

Open Browser:
http://localhost:3000/


# The Server.js use redis, the reason is to save the Callbacks.

#Tutorial Example
https://www.codementor.io/codeforgeek/build-website-from-scratch-using-expressjs-and-bootstrap-du107sby7


#Docs 
The NodeApp contains the file Server.js that connects with UI by websockets, also it is connected with Redis.

#Coding
1. Server.js - It is the server
2. scripts/tools.js - It has tools to study the flo. looking for errors in the connections.
