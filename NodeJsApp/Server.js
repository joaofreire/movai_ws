var express = require("express");
var app = express();
var router = express.Router();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var fs = require('fs');
var tools = require('./scripts/tools');
var path_nodes = "ros/";
var python_process = [];

var path = __dirname + '/views/';

// REDIS
var redis = require("redis");
var r = redis.createClient(6379, '10.10.0.60');
console.log(r)
r.on("error", function(err) {
    console.log("Error - Redis " + err);
});

r.on('connect', function() {
    console.log('Redis client connected');
});
// NOFLO
var library = {};

// CONSOLE
var PythonShell = require('python-shell');
var shell = require('shelljs');
var child_process = require('child_process');
var options = {
    mode: 'text',
    pythonOptions: ['-u'],
    scriptPath: 'ros'
}
PythonShell.defaultOptions = options



// ROUTE
router.use(function(req, res, next) {
    console.log("/" + req.method);
    next();
});

router.get("/", function(req, res) {
    res.sendFile(path + "index.html");
});

//~ router.get("/about",function(req,res){
//~ res.sendFile(path + "about.html");
//~ });



app.use("/", router); //route by default

//~ app.use("*",function(req,res){
//~ res.sendFile(path + "404.html");
//~ });

//~ start our web server and socket.io server listening
server.listen(5000, function() {
    console.log('listening ws on *:5000');
});


//Websockets
io.on('connection', function(client) {
    console.log('Client connected...');

    // Click in Save project - it updates in Redis the graph and the library
    client.on('saveproject', function(data) {
        var lib = data.lib;
        console.log("Click on save project")
        console.log(lib)
        var graph = data.graph;
        r.set("noflo-graph", graph, redis.print);
        r.set("noflo-lib", lib, redis.print);

    });

    // Click in loadproject - it retrieves from Redis the graph and the library
    client.on('loadproject', function(data) {
        library = JSON.parse('{}') //Reset lib
        r.get("noflo-graph", function(err, reply) { //request for graph
            if (!err) {
                var graph = reply; //get the graph

                r.get("noflo-lib", function(err, reply) { //request for lib
                    if (!err) {
                        var lib = reply
                        library = JSON.parse(lib);

                        //Data to send by websockets
                        var data = {
                            graph: graph,
                            lib: library
                        }
                        io.emit('load_project', data); //Send by websocket
                    }
                });
            }
        });
        


    });

    // Click on Create Project - It will create the nodes python in ./ros folder
    client.on('createFiles', function(data) {
        createFiles();
        createAlert("Create Project","The project has been created");
    });

    // Click in Run - It will run the project. It consists in 1.Save 2.Create Project, 3.Study the flow, 4.Run the nodes
    client.on('runcode_cb', function(data) {

        //1. Save project
        var lib = data.lib;
        var graph = data.graph;
        r.set("noflo-graph", graph, redis.print);
        r.set("noflo-lib", lib, redis.print);
        library = JSON.parse(lib);

        //2. Create Project
        console.log("RUN - CREATE FILE")
        shell.exec('python god.py ./ros', function(code, stdout, stderr) {
            console.log('[GOD] Exit code:', code);
            console.log('[GOD] output:', stdout);
            console.log('[GOD] stderr:', stderr);
            if (code == 0) {
                //3. Study first the connections
                console.log("RUN - STUDYNET")
                var graph_update = tools.study(graph, library)
                var lib = library;
                var data = {
                    graph: graph_update,
                    lib: library
                }
                console.log(JSON.stringify(graph_update))
                var err_connection = "{\"route\":10}";
                if (JSON.stringify(graph_update).includes(err_connection)) {
                    // The flow connections in the graph are not valid
                    console.log("RUN Error - It is not possible to RUN, You must check the flo");
                    io.emit('load_project', data);
                    io.emit('cb_stop_project', "");
                    createAlert("Error","It is not possible to RUN, You must check the connections");

                } else {
                    //4. Run the project
                    console.log("RUN - PROJECT");
                    io.emit('load_project', data);
                    runProject(graph)
                }
            }
        });
    });

    // Click in Stop - it will stops all the nodes
    client.on('stopcode_cb', function(data) {
        stopProject();
    });

    // Click in Delete All - It removes the Redis Database
    client.on('deleteall_cb', function(data) {
        deleteAll();
        createAlert("Delete Project","The project has been deleted");
    });

    // Click in Delete Node - It removes the Redis Database asociate on that Node, inports, outpors, params, callbacks, code
    client.on('delete_node', function(data) {
        deleteNode(data);
        createAlert("Delete Node","The node has been saved");
    });

    // Click in Create new Node - It creates the data in Redis about the Node.
    client.on('createNode_cb', function(data) {

        var key = JSON.parse(data);
        var key_redis_hw = "project/" + key.name + "/nodeType";
        r.set(key_redis_hw, key.nodeType, redis.print); //Creates node type

        if (key.nodeType === 'ROS') { //Creates pkg and file if it is Ros node
            var key_redis_pkg = "project/" + key.name + "/pkg";
            var key_redis_file = "project/" + key.name + "/file";
            r.set(key_redis_pkg, key.pkg, redis.print);
            r.set(key_redis_file, key.file, redis.print);
        }

        //Add Inports
        for (var i = 0; i < key.inports.length; i++) 
        {
            if (key.nodeType === 'SW') 
            { //Add the callbacks if it is Sw node
                var key_redis_cb = "project/" + key.name + "/cb/" + key.inports[i].name;
                r.set(key_redis_cb, "#write code", redis.print);
            }
            
            var key_redis_st = "project/" + key.name + "/inports/" + key.inports[i].name; 
            if (key.nodeType === 'HW') 
            { // The HW nodes cant be remaped
                r.hset(key_redis_st, "name", "/" + key.inports[i].name, redis.print);
                
            } 
            else 
            {
                r.hset(key_redis_st, "name", key.name + "/" + key.inports[i].name, redis.print);
            }
            r.hset(key_redis_st, "type", key.inports[i].type, redis.print);
            r.hset(key_redis_st, "port_type", key.inports[i].port_type, redis.print);

        }

        //Add the outports
        for (var i = 0; i < key.outports.length; i++) 
        {
            var key_redis_st = "project/" + key.name + "/outports/" + key.outports[i].name;
            if (key.nodeType === 'HW') 
            {// The HW nodes cant be remaped
                r.hset(key_redis_st, "name", "/" + key.outports[i].name, redis.print);
            } 
            else 
            {
                r.hset(key_redis_st, "name", key.name + "/" + key.outports[i].name, redis.print);
            }
            r.hset(key_redis_st, "type", key.outports[i].type, redis.print);
            r.hset(key_redis_st, "port_type", key.outports[i].port_type, redis.print);

        }

        //Add the params for a node
        for (var i = 0; i < key.params.length; i++) 
        {
            var key_redis_st = "project/" + key.name + "/params/" + key.params[i].name;
            r.set(key_redis_st, key.params[i].value, redis.print);

        }
        
        //createAlert("Create node","The node has been created");
    });


    // Click in Create new Node - It creates the data in Redis about the Node.
    client.on('createNodeInstance_cb', function(data) {

        var key = JSON.parse(data);
        var key_redis_hw = "library/" + key.name + "/nodeType";
        r.set(key_redis_hw, key.nodeType, redis.print); //Creates node type

        if (key.nodeType === 'ROS') { //Creates pkg and file if it is Ros node
            var key_redis_pkg = "library/" + key.name + "/pkg";
            var key_redis_file = "library/" + key.name + "/file";
            r.set(key_redis_pkg, key.pkg, redis.print);
            r.set(key_redis_file, key.file, redis.print);
        }

        //Add Inports
        for (var i = 0; i < key.inports.length; i++) 
        {
            if (key.nodeType === 'SW') 
            { //Add the callbacks if it is Sw node
                var key_redis_cb = "project/" + key.name + "/cb/" + key.inports[i].name;
                r.set(key_redis_cb, "#write code", redis.print);
            }
            
            var key_redis_st = "project/" + key.name + "/inports/" + key.inports[i].name; 
            if (key.nodeType === 'HW') 
            { // The HW nodes cant be remaped
                r.hset(key_redis_st, "name", "/" + key.inports[i].name, redis.print);
                
            } 
            else 
            {
                r.hset(key_redis_st, "name", key.name + "/" + key.inports[i].name, redis.print);
            }
            r.hset(key_redis_st, "type", key.inports[i].type, redis.print);
            r.hset(key_redis_st, "port_type", key.inports[i].port_type, redis.print);

        }

        //Add the outports
        for (var i = 0; i < key.outports.length; i++) 
        {
            var key_redis_st = "project/" + key.name + "/outports/" + key.outports[i].name;
            if (key.nodeType === 'HW') 
            {// The HW nodes cant be remaped
                r.hset(key_redis_st, "name", "/" + key.outports[i].name, redis.print);
            } 
            else 
            {
                r.hset(key_redis_st, "name", key.name + "/" + key.outports[i].name, redis.print);
            }
            r.hset(key_redis_st, "type", key.outports[i].type, redis.print);
            r.hset(key_redis_st, "port_type", key.outports[i].port_type, redis.print);

        }

        //Add the params for a node
        for (var i = 0; i < key.params.length; i++) 
        {
            var key_redis_st = "project/" + key.name + "/params/" + key.params[i].name;
            r.set(key_redis_st, key.params[i].value, redis.print);

        }
        
        //createAlert("Create node","The node has been created");
    });


    //Return all keys on node library
    client.on('get_nodes_library', function(data) 
    {
        var key_redis_cb = "library/"
        r.get(key_redis_cb, function(err, reply)
        {
            if (!err) 
            {
                io.emit('get_nodes_library_reply', reply);
            }
        });
    });


    // Click in Show Code - It returns the code of the SW node
    client.on('get_cb_code', function(data) 
    {
        var key = data;
        var key_redis_cb = "project/" + key.nodename + "/cb/" + key.inportname;
        //var cb_code = r.get(key_redis_cb, redis.print);
        r.get(key_redis_cb, function(err, reply) 
        {
            if (!err) 
            {
                io.emit('cb_code_update', reply);
            }
        });


    });

    // Click in Save Code - It saves the code of a node
    client.on('save_cb', function(data) 
    {
        var key = data
        var key_redis = "project/" + key.nodename + "/cb/" + key.callback
        r.set(key_redis, key.code, redis.print);
        createAlert("Code","The code of SW node has been saved");
    });

    // Click in Study Flo - It studies the flow and returns the graph after. (With or Without erros) The errors will be show in Red
    client.on('study_net', function(data) 
    {
        var graph = data.graph;
        var graph_update = tools.study(graph, library)
        var lib = library;
        var data = 
        {
            graph: graph_update,
            lib: lib
        }
        io.emit('load_project', data);
        
        var err_connection = "{\"route\":10}";
        if (JSON.stringify(graph_update).includes(err_connection)) 
        {
            // The flow connections in the graph are not valid
            createAlert("Error","It is not possible to RUN, You must check the connections");
        }
        else
        {
            createAlert("Study flo","The flow is correct");
        }
        
    });

});

/**
 * Run the project Funtion
 * Get the graph and create the comand line for each node, to run in the shell  
 **/
function runProject(graph) 
{
    console.log("RUN Project - ")
    graph = JSON.parse(graph)
    python_process = [];
    var nodes = graph.processes
    console.log(nodes)
    var keys = Object.keys(nodes);

    for (var key in keys) 
    {
        createComandLine(keys[key])
    }
}

/**
 * Create the command line for each node with the remaps and the params
 **/
function createComandLine(process) 
{
    var lib = library
    var p = lib[process]

    var inports = p["inports"]
    var outports = p["outports"]
    var params = p["params"]

    var dic = []
    //Searching remaps
    if (inports != null) 
    {
        for (var i = 0; i < inports.length; i++) 
        {
            dic[i] = inports[i]["name"];
        }
    }
    if (outports != null) 
    {
        for (var i = 0; i < outports.length; i++) 
        {
            dic[dic.length] = outports[i]["name"]
        }
    }


    redis_multi = r.multi()
    for (var i = 0; i < dic.length; i++) 
    {
        redis_multi.get("remap/" + process + "/" + dic[i]);
    }
    redis_multi.exec(function(err, results) 
    {
        if (err) 
        {
            throw err;
        }
        var args = []
        for (var i = 0; i < results.length; i++) 
        {
            if (results[i] != null && results[i] != "") 
            {
                args[i] = process + "/" + dic[i] + ":=" + results[i];
            }
        }

        //Add Params
        if (params != null) 
        {
            for (var i = 0; i < params.length; i++) 
            {
                args[args.length] = "_" + params[i]["name"] + ":=" + params[i]["value"];
            }
        }

        //Remap the same node
        if (p["nodeType"] === "ROS") 
        {
            if (inports != null) 
            {
                for (var i = 0; i < inports.length; i++) 
                {
                    args[args.length] = inports[i]["name"] + ":=" + p["name"] + "/" + inports[i]["name"];
                }
            }
            if (outports != null) 
            {
                for (var i = 0; i < outports.length; i++) 
                {
                    args[args.length] = outports[i]["name"] + ":=" + p["name"] + "/" + outports[i]["name"];
                }
            }

        }

        console.log("Process " + process + "nodeType: " + p["nodeType"]);
        if (p["nodeType"] === "SW") { 
            executeNodeSW(args, process)
            args = []
        } else if (p["nodeType"] === "ROS") {
            executeNodeRos(args, p['pkg'], p['file'])
            args = []
        }
    });

}

/**
 * Execute the SW nodes with arguments as parameters and remaps. 
 * This function use the PythonShell and it generates the stdout and stderr
 **/
function executeNodeSW(args, nodename) {
    var options = {
        mode: 'text',
        pythonOptions: ['-u'],
        scriptPath: 'ros',
        args: args
    }
    PythonShell.defaultOptions = options
    console.log("EXECUTE NODE SW" + nodename);
    console.log("ARGS: " + nodename);
    console.log(args);
    var pyshell = new PythonShell(nodename);

    pyshell.end(function(err) {
        if (err) {
            console.log(err);
        }
    });
    pyshell.stdout.on('data', function(data) {
        console.log(data);
        io.emit('terminal_cb', "<p>[" + nodename + "] " + data + "</p>");
    });
    pyshell.stderr.on('data', function(data) {
        console.log(data);
        io.emit('terminal_cb', "<p style=\"color:red\">[" + nodename + "] " + data + "</p>");
    });
    python_process.push(pyshell.childProcess);
    //Back options to default
    var options = {
        mode: 'text',
        pythonOptions: ['-u'],
        scriptPath: 'ros',
        args: []
    }
    PythonShell.defaultOptions = options
}


/**
 * Execute the ROS nodes with arguments as parameters and remaps. 
 * This function use the Child_Process and it generates the stdout and stderr
 * It is necessary to put the ros folder in str_exe where are the execution files
 **/
function executeNodeRos(args, pkg, file) {
    var str_exe = "/opt/ros/kinetic/lib/" + pkg + "/./" + file;

    console.log("EXECUTE ROS NODE")
    console.log(str_exe)
    console.log("Args:");
    console.log(args)
    var sh = child_process.spawn(str_exe, args); //, function(code, stdout, stderr) {
    sh.on('exit', function(code) {
        console.log('[' + file + ']child process exited with code ');
    });
    sh.stdout.on('data', function(data) {
        console.log(data);
        io.emit('terminal_cb', "<p>[" + file + "] " + data + "</p>");
    });
    sh.stderr.on('data', function(data) {
        console.log(data);
        io.emit('terminal_cb', "<p style=\"color:red\">[" + file + "] " + data + "</p>");
    });
    python_process.push(sh)

}


/**
 * Stop and kill the nodes
 * This function uses Sigint because is like ctrl + c
 **/ 
function stopProject() {
    console.log("STOP - PROJECT")
    for (var i in python_process) {
        //python_process[i].kill('SIGKILL');
        python_process[i].kill('SIGINT');
    }
    deleteRemapsRedis();
}


/**
 * Create the SW nodes in the folder ./ros using the GOD
 **/ 
function createFiles() {
    shell.exec('python god.py ./ros', function(code, stdout, stderr) {
        console.log('[GOD] Exit code:', code);
        console.log('[GOD] output:', stdout);
        console.log('[GOD] stderr:', stderr);
    });
}


/**
 * Delete a node in the database
 * This function will remove everything about the node 
 **/
function deleteNode(component) {
    console.log("DELETE NODE - " + component['name']);

    r.keys("*", function(err, keys) {
        keys.forEach(function(key, pos) {
            if (key.includes("project/" + component['name'] + "/")) {
                r.del(key);
            }
        });
    });
}

/**
 * Delete everything in the database and also the SW nodes inside of ./ros folder
 **/
function deleteAll() {
    //Delete BD
    console.log("DELETE ALL - PROJECT")

    r.keys("*", function(err, keys) {
        keys.forEach(function(key, pos) {
            if (key.includes("project")) {
                r.del(key);
            }
        });
    });


    //r.flushdb(function(err, succeeded) { //Delete the DB
    //    if (succeeded) {
            //DB has been deleted
            //Delete Nodes on system
            shell.exec('cd ros && rm -rf * ', function(code, stdout, stderr) {
                console.log('[DELETE_NODES] Exit code:', code);
                console.log('[DELETE_NODES] output:', stdout);
                console.log('[DELETE_NODES] stderr:', stderr);
            });

            //Delete Graph and lib
            var data = {
                graph: "{}",
                lib: "{}"
            }
            r.set("noflo-graph", data["graph"], redis.print);
            r.set("noflo-lib", data["lib"], redis.print);
            io.emit('load_project', data); //Send everything to the UI
      //  } else {
            //Error
        //    console.log('[DELETE_ALL] Error')
        //}
    //});
}


/**
 * Delete the Remaps in the Redis database
 **/
function deleteRemapsRedis() {

    r.keys("*", function(err, keys) {
        keys.forEach(function(key, pos) {
            if (key.includes("remap/")) {
                console.log("DEL " + key);
                r.del(key);
            }
        });
    });

}

/**
 * To send an alert to the UI
 **/

function createAlert(title, msg){
    var data = {
        title: title,
        msg: msg
    }
    io.emit('alert_popup', data); //Send everything to the UI
}


///// NOT USED FUNCTIONS

//~ // This function is to save a file in the server - It is not used
//~ function createFile(filename, data) {
    //~ fs.writeFile(filename, data, function(err) {
        //~ if (err) {
            //~ return console.log(err);
        //~ }

        //~ console.log("The file was saved!");
    //~ });
//~ }


//~ //This function is to load a file from the server - It is not used
//~ function loadFile(filename) {

    //~ //SYNC
    //~ return fs.readFileSync(filename, 'utf8')
    //~ //ASYNC
    //~ fs.readFile( filename, function (err, data) {
    //~ if (err) {
    //~ throw err; 
    //~ }
    //~ console.log("read")
    //~ console.log(data.toString());
    //~ return data.toString();
    //~ });
//~ }

/*
 * Function to remap nodes in the database - it is not used
 */
//~ function remapNodesInDataBase(graph) {

    //~ graph = JSON.parse(graph)
    //~ var connections = graph.connections
    //~ console.log(connections)

    //~ for (var i = 0; i < connections.length; i++) {
        //~ var con = connections[i];
        //~ var src = con.src;
        //~ var tgt = con.tgt;
        //~ var key_redis_src = "project/" + src.process + "/outports/" + src.port;
        //~ var key_redis_tgt = "project/" + tgt.process + "/inports/" + tgt.port;

        //~ //var name = r.hget(key_redis_src, "name", redis.print);
        //~ //console.log(key_redis_src)
        //~ r.hget(key_redis_src, "name", function(err, name) {
            //~ //Remap ports
            //~ r.hset(key_redis_tgt, "name", name, redis.print);
        //~ });

    //~ }
//~ }
