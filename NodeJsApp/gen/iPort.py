import rospy
import pickle
import redis

class iPort:
    def __init__(self, port):
        self.topic = port.topic
        self.type = port.type
        self.cb = self.handle_cb(port.cb_key)
        self.pub = rospy.Subscriber(self.topic, self.type, self.handle_cb)
        self.cb_key = port.cb_key #needs to get it via the callback class


    def handle_cb(self, data):
        print 'this is a callback'
        
