#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from GD import GD
#from callbacks import CB
from sensor_msgs.msg import LaserScan 


###############################################
#                                             #
#     THIS CODE WAS AUTO-GENERATED FROM       #
#        MOV.AI SOFTWARE FOR ROBOTICS         #
#     FOR MORE INFORMATION VISIT MOV.AI       #
#                                             #
###############################################


gd = GD('laser')

class laserNode(object):
	def __init__(self, name):

		# Init Imputs
		gd.cb = {'handle_db_laserIn':self.handle_db_laserIn}
		gd.sub['laserIn'] = rospy.Subscriber("laser/laserIn", LaserScan, self.laserIn_cb)
		gd.pub['laserOut'] = rospy.Publisher("laser/laserOut", LaserScan, queue_size = 10)
		gd.add_db_sub('laserIn', 'robot')

		# Init Outputs
		#exec(self.cb.laser_code_init())



	#DB laser callbacks of type robot
	def handle_db_laserIn(self, result):
 		data = gd.getVar(result['pattern'].replace('__keyspace@0__:', ''))
		gd.call("laserOut", data)
 		exec(str(gd.getCb('project/laser/cb/laserIn')))


	# Subscriber callback for input 'laserIn' 
	def laserIn_cb(self, data):
		gd.setVarRobot('laserIn', data)
		#exec(str(gd.getCb('project/laser/cb/laserIn')))


if __name__ == '__main__':
	rospy.init_node('laser')
	node = laserNode(rospy.get_name())
	rospy.spin()
