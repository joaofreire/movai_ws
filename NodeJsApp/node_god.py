#!/usr/bin/env python

import os
import json
import rosmsg, rospkg

class Import():
    def __init__(self, _type, _name):
        self.type = _type
        self.name = _name

class Port():
    def __init__(self, _type, _name, _message, _topic):
        self.type = _type
        self.name = _name
        self.topic = _topic
        self.message = _message

class Node():
    def __init__(self):
        self.name = ''
        self.type = ''
        self.description = ''
        self.params = list()
        self.imports = list()
        self.inputs = list()
        self.outputs = list()
        self.outcomes = list()

'''
Class that generates a custom node for the Mov.ai software  
'''
class GenerateNode():

    def __init__(self):
        self.nodes_list = list()

    def head(self):
        s = "#!/usr/bin/env python\n"
        s+= "# -*- coding: utf-8 -*-\n\n"
        s+= "import rospy\n"
        s+= "from GD import GD\n"   
        s+= "from callbacks import CB\n"    #TEMPORARY TO TEST!!
        return s

    def imports(self, _package, _array):
        s="from " + _package + " import " 
        for item in _array:
            s+= item 
            if item != _array[-1]:
                s+= ", "
            else:
                s+= "\n"
        return s

    def information(self):
        s="\n\n"
        s+= "###############################################\n"
        s+= "#                                             #\n"
        s+= "#     THIS CODE WAS AUTO-GENERATED FROM       #\n"
        s+= "#        MOV.AI SOFTWARE FOR ROBOTICS         #\n"
        s+= "#     FOR MORE INFORMATION VISIT MOV.AI       #\n"
        s+= "#                                             #\n"
        s+= "###############################################\n"
        s+= "\n\n"
        return s 


    def init_class(self, _name):
        s= "gd = GD('%s')\n\n"%_name
        s+= "class %sNode(object):\n"%(_name)
        s+= "\tdef __init__(self, name):\n"
        return s

    def params(self, _params):
        s = "\n"
        for param in _params:
            s+="\t\tgd.setVarNode('%s', %s)\n"%(param, param)
        return s


    ### ONE LINERS IN INIT
    def sub(self, _input_name, _topic, _type):
        return "\t\tgd.sub['%s'] = rospy.Subscriber(\"%s\", %s, self.%s_cb)\n" %(_input_name, _topic, _type, _input_name)

    def pub(self, _output_name, _topic, _type):
        return "\t\tgd.pub['%s'] = rospy.Publisher(\"%s\", %s, queue_size=10)\n"%(_output_name, _topic, _type)

    def srv_server(self, _input_name, _topic, _type):
        return "\t\tgd.srv_server['%s'] = rospy.Service(\"%s\", %s, self.%s_cb)\n"%(_input_name, _topic, _type, _input_name)

    def srv_client(self, _output_name, _topic, _type):
        return "\t\tgd.srv_client['%s'] = rospy.ServiceProxy(\"%s\", %s)\n"%(_output_name, _topic, _type)

    def action_server(self, _input_name, _topic, _type):
        return "\t\tgd.action_server['%s'] = actionlib.SimpleActionServer(\"%s\", %s, execute_cb = self.%s_cb)\n"%(_input_name, _topic, _type, _input_name)

    def action_client(self, _output_name, _topic, _type):
        return "\t\tgd.action_client['%s'] = actionlib.SimpleActionClient(\"%s\", %s)\n"%(_output_name, _topic, _type)

    def timer(self, _input_name, _duration):
        return "\t\tgd.timer['%s'] = rospy.Timer(rospy.Duration(%s), self.%s_cb)"%(_input_name, _duration, _input_name)

    def db_client(self, _input_name, _type):
        return "\t\tgd.add_db_sub('%s', '%s')\n"%(_input_name, _type)

    def transition(self, _output_name):
        return "\t\tgd.transition['%s'] = True\n"%(_output_name)

    def cb_dict_action(self, _port):
        return "\t\tgd.cb = {'%s_active': self.%s_active, '%s_done': self.%s_done, '%s_feedback': self.%s_feedback}\n"%(_port, _port, _port, _port, _port, _port)


    #self._as = actionlib.SimpleActionServer(self._action_name, GoToAction, execute_cb=self.execute_cb, auto_start = False)

    #self.client_mb = actionlib.SimpleActionClient('/move_base', MoveBaseAction)
    #goal_mb = MoveBaseGoal()
    #self.client_mb.send_goal(goal_mb, self.done_cb, self.active_cb, self.feedback_cb)

    ### CALLBACKS
    def sub_cb(self, _node_name, _input_name):
        s = "\n\t# Subscriber callback for input '%s' \n"%(_input_name)
        s+= "\tdef %s_cb(self, data):\n"%(_input_name)
        s+= "\t\tgd.setVarNode('%s', data)\n"%(_input_name)
        s+= "\t\texec(str(gd.getCb('project/%s/cb/%s')))\n"%(_node_name,_input_name)
        return s

    def timer_cb(self, _node_name, _input_name):
        s = "\n\t# Timer callback for input '%s' \n"%(_input_name)
        s+= "\tdef %s_cb(self, event):\n"%(_input_name)
        s+= "\t\texec(str(gd.getCb('project/%s/cb/%s')))\n"%(_node_name,_input_name)
        return s

    def db_cb(self, _node_name, _input_name):
        s = "\n\t# Database callback for input '%s' \n"%(_input_name)
        s+= "\tdef %s_cb(self, data):\n"%(_input_name)
        s+= "\t\texec(str(gd.getCb('project/%s/cb/%s')))\n"%(_node_name,_input_name)
        return s

    def srv_server_cb(self, _node_name, _input_name, _type):
        s = "\n\t# Service callback for input '%s' \n"%(_input_name)
        s+= "\tdef %s_cb(self, data):\n"%(_input_name)
        s+= "\t\tresponse = %sResponse()\n"%(_type)
        s+= "\t\texec(str(gd.getCb('project/%s/cb/%s')))\n"%(_node_name,_input_name)
        s+= "\t\treturn response\n"
        return s

    def action_server_cb(self, _node_name, _input_name, _type):
        s = "\n\t# Action callback for input '%s' \n"%(_input_name)
        s+= "\tdef %s_cb(self, data):\n"%(_input_name)
        s+= "\t\tresult = %sResult()\n"%(_type)
        s+= "\t\tfeedback = %sFeedback()\n"%(_type)
        s+= "\t\texec(str(gd.getCb('project/%s/cb/%s')))\n"%(_node_name,_input_name)
        return s

    def action_client_cb(self, _node_name, _output_name, _type):
        s= "\n\t# Client Action callbacks for output '%s' \n"%(_output_name)
        s+= "\tdef %s_done(self, status, result):\n"%(_output_name)
        s+= "\t\texec(str(gd.getCb('project/%s/cb/%s/done')))\n"%(_node_name,_output_name)
        s+= "\tdef %s_active(self):\n"%(_output_name)
        s+= "\t\texec(str(gd.getCb('project/%s/cb/%s/active')))\n"%(_node_name,_output_name)
        s+= "\tdef %s_feedback(self,data):\n"%(_output_name)
        s+= "\t\texec(str(gd.getCb('project/%s/cb/%s/feedback')))\n"%(_node_name,_output_name)
        return s

    def tf_cb(self, _input_name):
        s= "\n\t# TF callback for input '%s' \n"%(_input_name)
        s+= "\tdef %s_cb(self, data):\n"%(_input_name)
        s+= "\t\texec(self.%s_code)\n"%(_input_name)
        s+= "\t\tprint 'THIS DOES NOT MAKE ANY SENSE'\n"
        s+= "\t\tprint 'PLS CREATE SOME gd.tf_transform()'\n"
        return s

    def init_cb(self, _node_name):
        return "\t\texec(str(gd.getCb('project/%s/cb/init')))\n\n"%_node_name

    ### MAIN
    def node_main(self, _name):
        s = "\n\nif __name__ == '__main__':\n"
        s+= "\trospy.init_node('%s')\n"%(_name)
        s+= "\tnode = %sNode(rospy.get_name())\n"%(_name)
        s+= "\trospy.spin()\n"
        return s

    ### FLEXBE STATE

    def flexbe_imports(self):
        return "from flexbe_core import EventState\n"

    def flexbe_class(self, _name, _outcomes, _params, _description):
        s= "gd = GD('%s')\n\n"%_name
        s+= "class %sState(EventState):\n"%(_name)
        s+= "\t'''\n\t%s\n\t'''\n\n"%_description
        s+= "\tdef __init__(self"
        for p in _params:
            s+= ", %s"%p
        s+="):\n" # HOW  DO I PUT PARAMETERS HERE ????
        s+= "\t\tsuper(%sState, self).__init__(outcomes = %s)\n\n"%(_name, _outcomes)
        #s+= '\t\tself.gd = GD()\n'                
        s+= "\t\tself.cb = CB()\n"                # TEMPORARY TO TEST!!
        return s

        #def __init__(self, log_description='', log_level=0, action='grab', timeout=3):
        #   super(GripperState, self).__init__(outcomes = ['success', 'failed'],

    def flexbe_cb(self, _name):
        s= "\n\tdef execute(self, userdata):\n"
        s+= "\t\texec(str(gd.getCb('project/%s/cb/execute')))\n"%_name
        s+= "\t\tif gd.change_state is not None:\n"
        s+= "\t\t\treturn gd.change_state\n\n"
        s+="\tdef on_enter(self, userdata):\n"
        s+= "\t\texec(str(gd.getCb('project/%s/cb/on_enter')))\n\n"%_name
        s+="\tdef on_exit(self, userdata):\n"
        s+= "\t\texec(str(gd.getCb('project/%s/cb/on_exit')))\n"%_name
        s+= "\t\tgd.change_state = None\n\n"
        return s

    def parse_node_json(self, _filename):
        with open(_filename) as json_data:
            data = json.load(json_data,) 
        for s in data:
            node = Node()
            node.name = str(s)
            node.type = data[s]["nodeType"]
            node.description = data[s]["description"]
            for p in data[s]["params"]:
                node.params.append(str(p))
            #node.params = str(data[s]["params"])
            if data[s]["inports"]:
                for in_port in data[s]["inports"]:
                    # Port class receives (_type, _name, _message, _topic)
                    port = Port(in_port["port_type"],in_port["name"],in_port["type"],node.name + "/" + in_port["name"])
                    node.inputs.append(port)
                    # Import class receives (_type, _name))
                    node.imports.append(Import(in_port["port_type"], in_port["type"]))

            if data[s]["outports"]:
                for out_port in data[s]["outports"]:
                    if out_port["port_type"] == 'Transition':
                        node.outcomes.append(str(out_port["name"]))
                    #else:
                    port = Port(out_port["port_type"],out_port["name"],out_port["type"],node.name + "/" + out_port["name"])
                    node.outputs.append(port)
                    # Import class receives (_type, _name))
                    node.imports.append(Import(out_port["port_type"], out_port["type"]))
            self.nodes_list.append(node)

    def generate_imports(self, node):
        s = '' 
        import_topic = list()
        import_service = list()
        import_action = list()
        import_tf = False

        for elem in node.imports:
            if elem.type == 'Topic':
                import_topic.append(str(elem.name))
            if elem.type == 'Service':
                import_service.append(str(elem.name))
            if elem.type == 'Action':
                import_action.append(str(elem.name))
            if elem.type == 'TF':
                import_tf = True

        if import_action:
            s+= 'import actionlib \n'

        if import_tf:
            s+= 'import tf \n'
            s+= 'from tf2_msgs.msg import TFMessage \n' 

        for t in list(set(import_topic)):
            found_msgs = list(rosmsg.rosmsg_search(rospkg.RosPack(), '.msg', t))
            if not found_msgs or len(found_msgs) > 1:
                print 'Error: Message does not exist or multiple messages found with same name: %s'%t
            else:                                   #WHAT HAPPENS WHEN BOTH ARE IMPORTED??? ERROR???
                splt =  found_msgs[0].split('/')
                s+= "from %s.msg import %s \n"%(splt[0], splt[1])

        for t in list(set(import_service)):
            found_msgs = list(rosmsg.rosmsg_search(rospkg.RosPack(), '.srv', t))
            if not found_msgs or len(found_msgs) > 1:
                print 'Error: Message does not exist or multiple messages found with same name: %s'%t
                if t == 'Empty':
                     s+= "from std_srvs.srv import Empty \n"
            else:
                splt =  found_msgs[0].split('/')
                s+= "from %s.srv import %s, %sRequest, %sResponse \n"%(splt[0], splt[1], splt[1], splt[1])

        for t in list(set(import_action)):
            found_msgs = list(rosmsg.rosmsg_search(rospkg.RosPack(), '.msg', t+'Action'))
            if not found_msgs or len(found_msgs) > 1:
                print 'Error: Message does not exist or multiple messages found with same name: %s'%t
            else:
                splt =  found_msgs[0].split('/')
                action = splt[1].replace("Action", "")
                s+= "from %s.msg import %s, %sFeedback, %sGoal, %sResult \n"%(splt[0],splt[1], action, action, action)
        return s 

    def generate_ports(self, node):
        s="\t\t# Init Imputs\n"
        for port in node.inputs:
            if port.type == "Topic": 
                s+=self.sub(port.name,port.topic, port.message)
                self.callbacks.append(self.sub_cb(node.name, port.name))
            if port.type == "Service":
                s+=self.srv_server(port.name,port.topic, port.message)
                self.callbacks.append(self.srv_server_cb(node.name, port.name, port.message))
            if port.type == "Action":
                s+=self.action_server(port.name,port.topic, port.message+'Action')
                self.callbacks.append(self.action_server_cb(node.name, port.name, port.message))
            if port.type == "Timer":
                s+= self.timer(port.name, port.message)
                self.callbacks.append(self.timer_cb(node.name, port.name))
            if port.type == "Database":
                s+=self.db_client(port.name, port.message)
                self.callbacks.append(self.db_cb(node.name, port.name))
            if port.type == "TF":  #Try to remove this
                s+=self.sub(port.name,'/tf', 'TFMessage')
                self.callbacks.append(self.tf_cb(port.name))

        s+="\t\t# Init Outputs\n"
        for port in node.outputs:
            if port.type == "Topic": 
                s+=self.pub(port.name,port.topic, port.message)
            if port.type == "Service":
                s+=self.srv_client(port.name,port.topic, port.message)
            if port.type == "Action":
                s+=self.action_client(port.name,port.topic, port.message+'Action')
                s+=self.cb_dict_action(port.name)
                self.callbacks.append(self.action_client_cb(node.name, port.name, port.message))
            if port.type == "Database":
                pass
            if port.type == "Transition":
                s+=self.transition(port.name)
        return s

    def generate_node(self, node):
        self.callbacks = list()
        #node = self.nodes_list[0]
        path = 'ros/%s.py'%(node.name)

        file = open(path, "w")
        file.write(self.head())
        file.write(self.generate_imports(node))
        if node.type == "State":
            file.write(self.flexbe_imports())    
        file.write(self.information())
        if node.type == "State":
            file.write(self.flexbe_class(node.name,node.outcomes, node.params, node.description))    
        else:
            file.write(self.init_class(node.name))
        
        file.write(self.params(node.params))
        file.write(self.generate_ports(node))
        file.write(self.init_cb(node.name))
        
        if node.type == "State":
            file.write(self.flexbe_cb(node.name))

        for cb in self.callbacks:
            file.write(cb)

        if node.type != "State":
            file.write(self.node_main(node.name))

        file.close()

        os.chmod(path, 0777)   #CHECK THIS

    def generate_nodes(self):
        for node in self.nodes_list:
            self.generate_node(node)



'''
STUFF TO REMEMBER

unsubscribe: sub1.unregister()




'''