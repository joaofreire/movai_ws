#!/usr/bin/env python
# -*- coding: latin-1 -*-
from GD_Redis import *
import time
import json
#from GD import GD

#gd = GD()

class Policy(object):
    __instance = None
    arr = {}
    gd = None

    
    def __new__(cls, gdClass):
        if Policy.__instance is None:
            Policy.__instance = object.__new__(cls)

            #GD_Singleton_Mongo.__initMongo("AGV1")
        return Policy.__instance

    def __init__(self, gdClass):
        self.__thread = None
        self.__db = GD_Redis()
        self.__policy = []
        #self.__getPolicy()
        self.gd = gdClass;
        #self.__getPolicy();
        #self.run()
        #~ self.__db.setVar('fleet/ejem1', 55)
        
    def ___str2bool(self, v):
        return v.lower() in ("yes", "true", "t", "1")
        
    def run(self, name):
        #fIRST GET POLICY
        self.__getPolicy(name)
        #Timer is neede no block the Redis
        time.sleep(0.2)
        
        ##self.__getPolicy() #Get new policy
        
        #print "-- RUN --"
        #print self.gd.call("out", "hehe")
        
        for data in self.__policy:
            #print self.__createEvalFun(data)
            if eval(self.__createEvalFun(data)):
                command = str(data["Code"])
                #print command
                exec(command)
                break;
        

    def gd():
      return self.__gd

    def __getPolicy(self, name):
        self.__policy = json.loads(str(self.__db.getVar("policy/%s"%name)))["data"]
    
    def __setGd(self, gdClass):
        self.__gd = gdClass;

    def __createEvalFun(self, data):
        evalFunction = "";
        isFirst = True;
        for key in data.keys():
            if key != str("Code") and key != str("Priority"):
                if data[key] != "-":
                    if not isFirst:
                        evalFunction = "%s and" %evalFunction 
                    str_funct = '%s %s'%(self.__db.getVar(key),data[key]) 
                    evalFunction = "%s %s" %(evalFunction, str_funct)
                    isFirst = False
        
        if evalFunction == "":
            evalFunction = "False"
        return evalFunction






#def main():
#Run the first time
    #p = Policy()
    #~ p.arr["ex"] = 3
    #~ print p.arr
    #~ p2 = Policy()
    #~ p2.arr["ex"] = 4;
    
    #~ print p.arr
    #~ print p2.arr
    
    #p.run() 
    
    
    
#if __name__ == '__main__':
#  main()
