// tools.js
// ========
//REDIS
var redis = require("redis");
var r = redis.createClient(6379, '10.10.0.60');
var fs = require('fs');

module.exports = {
    foo: function() {
        // whatever
    },
    study: function(graph_data, lib) {
        graph = JSON.parse(graph_data)
        var graph_bk = JSON.parse(graph_data)
        // Reset the color of the connections
        resetColorConnections(graph)
        resetColorConnections(graph_bk)

        var connections_bk = graph_bk.connections;
        var connections = graph.connections;
        var lib = lib;


        // Do the study
        reverse(connections, lib)
        forward(connections, lib)

        graph.connections = connections_bk;
        graph.connections = setColorConnectionsError(graph, connections);

        return graph

    }


};


/**
 * Study the flo in forward
 **/
function forward(connections, lib) {
    arr_tgt = []
    arr_toDelete = []
    for (var i = 0; i < connections.length; i++) {
        var con = connections[i];
        var tgt = con.tgt;
        if (!arrayContainsSRCObject(tgt, arr_tgt)) {
            arr_tgt.push(tgt);

            //Search all tgt with this source
            var con_same_tgt = [];
            for (var j = 0; j < connections.length; j++) {
                var c = connections[j];
                var t = c.tgt;
                if (srcAreEquals(t, tgt)) {
                    con_same_tgt.push(c)
                }
            }
            //Check the trt how many connections have
            var src_isOk = true;
            for (var j = 0; j < con_same_tgt.length; j++) {
                var c = con_same_tgt[j];
                var src = c.src;
                var arr_src = []
                for (var k = 0; k < connections.length; k++) {
                    var c_k = connections[k];
                    var src_k = c_k.src;
                    if (srcAreEquals(src, src_k)) {
                        arr_src.push(c_k)
                    }
                }
                if (arr_src.length > 1 || (lib[c.src.process]["nodeType"] != null && (lib[c.src.process]["nodeType"] === "HW" || lib[c.src.process]["nodeType"] === "ROS"))) {
                    src_isOk = false;
                    break;
                }
            }

            if (src_isOk) {
                //delete from connections
                for (var j = 0; j < con_same_tgt.length; j++) {
                    var c = con_same_tgt[j]
                    arr_toDelete.push(c);
                    var k = "remap/" + c.src.process + "/" + c.src.port
                    var n = "";
                    if (lib[c.tgt.process]["nodeType"] !== "HW") {
                        n = c.tgt.process + "/" + c.tgt.port
                    } else {
                        n = "/" + c.tgt.port
                    }
                    r.set(k, n, redis.print);
                }
            }
        }
    }
    for (var j = 0; j < arr_toDelete.length; j++) {
        deleteFromConnections(connections, arr_toDelete[j])
    }
}


/**
 * Study the flo in reverse
 **/
function reverse(connections, lib) {
    arr_src = []
    arr_toDelete = []
    for (var i = 0; i < connections.length; i++) {
        var con = connections[i];
        var src = con.src;
        if (!arrayContainsSRCObject(src, arr_src)) {
            arr_src.push(src);

            //Search all tgt with this source
            var con_same_src = [];
            for (var j = 0; j < connections.length; j++) {
                var c = connections[j];
                var s = c.src;
                if (srcAreEquals(s, src)) {
                    con_same_src.push(c)
                }
            }

            //Check the trt how many connections have
            var tgt_isOk = true;
            for (var j = 0; j < con_same_src.length; j++) {
                var c = con_same_src[j];
                var tgt = c.tgt;
                var arr_tgt = []

                for (var k = 0; k < connections.length; k++) {
                    var c_k = connections[k];
                    var tgt_k = c_k.tgt;
                    if (srcAreEquals(tgt, tgt_k)) {
                        arr_tgt.push(c_k)
                    }
                }

                if (arr_tgt.length > 1 || (lib[c.tgt.process] != null && (lib[c.tgt.process]["nodeType"] != null && (lib[c.tgt.process]["nodeType"] === "HW" || lib[c.tgt.process]["nodeType"] === "ROS")))) {
                    tgt_isOk = false;
                    break;
                }
            }

            if (tgt_isOk) {
                //delete from connections
                for (var j = 0; j < con_same_src.length; j++) {
                    var c = con_same_src[j]
                    arr_toDelete.push(c);
                    var k = "remap/" + c.tgt.process + "/" + c.tgt.port
                    var n = "";
                    if (lib[c.tgt.process] != null && lib[c.src.process]["nodeType"] !== "HW") {
                        n = c.src.process + "/" + c.src.port

                    } else {
                        n = "/" + c.src.port
                    }
                    r.set(k, n, redis.print);
                }
            }
        }
    }
    for (var j = 0; j < arr_toDelete.length; j++) {
        deleteFromConnections(connections, arr_toDelete[j])
    }
}


/**
 * Delete the connection in the connections_array
 * Is a util function
 **/
function deleteFromConnections(connections_arr, connection) {

    for (var i = 0; i < connections_arr.length; i++) {
        var c = connections_arr[i]
        if (srcAreEquals(connection.src, c.src) && srcAreEquals(connection.tgt, c.tgt)) {
            connections_arr.splice(i, 1);
        }

    }

}

/**
 * Get if an array contains the src
 **/
function arrayContainsSRCObject(src, array) {

    var process = src.process;
    var port = src.port;

    for (var i = 0; i < array.length; i++) {
        var src_i = array[i];
        if (src_i.port == port && src_i.process == process) {
            return true;
        }
    }
    return false;

}
/**
 * Get is 2 src are equal
 **/
function srcAreEquals(src1, src2) {

    var process = src1.process;
    var port = src1.port;


    if (src2.port == port && src2.process == process) {
        return true;
    } else {
        return false;
    }

}

/**
 * Set the connection color to red if there are a error in the study of the flo
 **/
function setColorConnectionsError(graph, connectionsError) {
    var connections = graph.connections;
    for (var i = 0; i < connections.length; i++) {
        var c_i = connections[i];
        //RESET THE COLOUR
        try {
            c_i.metadata.route = 0;
        } catch (err) {
            c_i["metadata"] = {
                "route": 0
            }
        }
        for (var j = 0; j < connectionsError.length; j++) {
            var c_j = connectionsError[j]
            if (c_i.src.process == c_j.src.process && c_i.src.port == c_j.src.port && c_i.tgt.process == c_j.tgt.process && c_i.tgt.port == c_j.tgt.port) {
                //SET TO RED
                try {
                    c_i.metadata.route = 10;
                } catch (err) {
                    c_i["metadata"] = {
                        "route": 10
                    }
                }
            }
        }
    }
    return connections;
}

/**
 * Reset the color of all the conections to white
 **/
function resetColorConnections(graph) {

    var connections = graph.connections;
    for (var i = 0; i < connections.length; i++) {
        var c_i = connections[i];
        //RESET THE COLOUR
        try {
            c_i.metadata.route = 0;
        } catch (err) {
            c_i["metadata"] = {
                "route": 0
            }
        }
    }
    graph.connections = connections;
}
