## INSTALL ROS_MONGO_STORE

sudo apt-get install python-pymongo mongodb

sudo apt-get install ros-kinetic-mongodb_store

### Start ROS_MONGODB_STORE

rosparam set mongodb_port 62345

rosparam set mongodb_host bob 

rosrun mongodb_store mongodb_server.py

### Test
The test is called MongoDBTest.py and is in db/src/db folder

# USE OUR MONGO GD

##INSTALL MONGO
See the url with the instructions: https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-16-04
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6

echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list

sudo apt-get update

sudo apt-get install -y mongodb-org

sudo service mongod start

##INSTALL MONGO ON PYTHON
python -m pip install pymongo

python -m pip install pymongo==3.1.1

## Using GD Mongo
The project is inside db/src/db folder. And it has 2 Clases:

1. Singeltone Mongo, it is only one object that all the nodes can call him and it is always the same, using Design Patterns. It manages the getters and setters with the database. It contains differents methods:
    1. InsertVarFleet or InsertVarRobot: to insert a variable in the database
    2. SetVarRobot or SetVarFleet: to set a variable in the data base, also you can use var.x to set only the x value.
    3. GetVarRobot or GetVarFleet: to get a variable from db, also you can use var.x to get only the x value.
    4. sub_varRobotDB or sub_varFleetDB: to subscribe a variable in the database, you can subs only var.x
    
    The varRobot is only for each robot
    The varFleet will be share between robots

    It is necessary to change the constructor of the Singeltone to put an create the ID on the database of each robot
    
2. MongoThread, it is used to do the polling to MongoDB and detect when something change and call the callback. Only it is used when one node subscribes to the database

#### Test
For testing use: python Test_Singletone.py in db/src/db folder


##INSTALL REDIS

wget http://download.redis.io/redis-stable.tar.gz

tar xvzf redis-stable.tar.gz

cd redis-stable

make

sudo apt-get install redis-server

sudo apt-get install redis-tools

#Start Redis
$ redis-server

#enable notifications
$ redis-cli config set notify-keyspace-events KEA

More info: http://tech.webinterpret.com/redis-notifications-python/


#Console redis
$ redis-cli

## Using GD Redis
The project is inside db/src/db folder. 

1. GD_Redis, it is only one object that all the nodes can call him and it is always the same, using Design Patterns. It manages the getters and setters with the database. It contains differents methods:
  
    1. SetVarRobot or SetVarFleet: to set a variable in the data base
    2. GetVarRobot or GetVarFleet: to get a variable from db
    3. sub_VarRobot or sub_VarFleet: to subscribe a variable in the database
    
    The varRobot is only for each robot
    The varFleet will be share between robots

    It is necessary to change the constructor of the Singeltone to put an create the ID on the database of each robot
    
    Remember start redis and start the notifications


#### Test
For testing use: python Test_Redis.py in db/src/db folder

