#!/usr/bin/env python
# -*- coding: latin-1 -*-
from pymongo import MongoClient
from MongoThread import *

class GD_Mongo:

    def __init__(self, robotID):
        self.__client = MongoClient('localhost:27017')
        self.__db = self.__client.movai
        self.__dbrobots = self.__client.movai.robots[robotID]
        self.__dbfleet = self.__client.movai.fleet
        self.__var_local = {}
        self.__var_node = {}
        
        self.__functions = {}
    
    """
        GET variables persistent across consecutive calls to given callback.
    """
    def getVarLocal(self, name):
        return self.__var_local[name] #Through Global variables
    
    """
        SET variables persistent across consecutive calls to given callback.
    """
    def setVarLocal(self, name, value):
        self.__var_local[name] = value

    """
        GET variables persistent across all callbacks in a given Node
    """
    def getVarNode(self, name):
        return self.__var_node[name] #Through Global variables
    
    """
        SET variables persistent across all callbacks in a given Node
    """
    def setVarNode(self, name, value):
        self.__var_node[name] = value
    
    """
        GET variables persistent across all Nodes in a given robot.
    """
    def getVarRobot(self, name):
        return self.__db_getVar('varRobot',name) #Through DB
    
    
    """
        SET variables persistent across all Nodes in a given robot.
    """
    def setVarRobot(self, name, value):
        self.__db_setVar('varRobot', name, value) #Through DB

    """
        GET variables persistent across all callbacks of all robots.
    """
    def getVarFleet(self, name):
        db = self.__dbfleet
        document = db.find_one()
                
        if document:
            return document[name]
        else:
            return None
            
    
    """
        SET variables persistent across all callbacks of all robots.
    """
    def setVarFleet(self, name, value):
        self.__dbfleet.update_one({},{'$set':{name : value}}, upsert = True)
       

    #MONGO DB SET AND GET VAR
    def __db_setVar(self, collection_name, name, value):
        self.__dbrobots.update_one({"var_type" : collection_name}, {'$set':{name : value}}, upsert = True);
    
    def __db_getVar(self, collection_name, name):
        document = self.__dbrobots.find_one({"var_type" : collection_name})
        if document:
            return document[name]
        else:
            return None
            
            
    #PUB/SUB
    def sub_varFleetDB(self, function, rate=60):
        uniqueKey = None
        for key in function:
            self.__functions['%s'%key] = function[key]
            uniqueKey = key
                
        thread = self.__new_thread(self.__functions[uniqueKey], rate, self.__dbfleet)
        thread.start()
        
    def sub_varRobotDB(self, function, rate=60):
        uniqueKey = None
        for key in function:
            self.__functions['%s'%key] = function[key]
            uniqueKey = key
                
        thread = self.__new_thread(self.__functions[uniqueKey], rate, self.__dbrobots)
        thread.start()
    
    def __new_thread(self, function, rate, collection):
        return MongoThread(function = function, rate = rate, collection = collection)
        
    def on_thread_finished(self, thread, data):
        print data
    
    

