#!/usr/bin/env python
# -*- coding: latin-1 -*-

from pymongo import MongoClient
from pymongo import CursorType
import time


def checkDocumentsKeys(docNex, docPre):

    if len(docNex.keys()) == len(docPre.keys()) and docNex.keys() == docPre.keys():
        for key in docNex.keys():
            if not checkAllValues(key, docNex, docPre):
                return False
        return True
    else:
        return False

def checkDocument(docNex, docPre):
    if checkDocumentsKeys(docNex, docPre):
        return True
    else:
        return False
        
def checkAllValues(key, docNex, docPre):
    if docNex[key] == docPre[key]:
        return True
    else:
        return False


    

client = MongoClient('localhost:27017')
db = client.movai
#dbrobots = db.robots[robotID]
coll = db.robots.AGV1
cursor = coll.find({})

docPre = None

while cursor.alive:
    try:
        doc = cursor.next()
        print doc
        if doc is not None and docPre is not None:
            print checkDocument(doc, docPre)
        docPre = doc
        cursor = coll.find({})
        time.sleep(1)
    except StopIteration:
        time.sleep(1)


