#!/usr/bin/env python
# -*- coding: latin-1 -*-

from GD_Redis import *
import rospy
from geometry_msgs.msg import Twist
import time
import pickle


GD = GD_Redis()

def handler(msg):  
    msg = pickle.loads(GD.getVarFleet("a"))
    comand = str(GD.getVarFleet("callback_test"))
    exec(comand)

    
if __name__ == "__main__":
    #setVariable
    rospy.init_node('Test_Redis', anonymous=True)
    
    GD.setVarFleet("callback_test", "if msg.linear.x > 0:\n  print GD.getVarFleet(\"aa\")\nelse:\n  print \"less than 0\"")
    GD.sub_VarFleet("cmd", handler)
    

    rospy.spin()
    
