#!/usr/bin/env python
# -*- coding: latin-1 -*-

from GD_Redis import *
import rospy
from geometry_msgs.msg import Twist
import time
import pickle


GD = GD_Redis()

def handler(msg):  
    comand = str(GD.getVarFleet("callback_test"))
    exec(comand)

    
if __name__ == "__main__":
    #setVariable
    rospy.init_node('Test_Redis', anonymous=True)
    
     
    msg = Twist()
    msg.linear.x = 1
    
    
    GD.setVarFleet("a", pickle.dumps(msg))
   
    print pickle.loads(GD.getVarFleet("a"))
    

    rospy.spin()
    
