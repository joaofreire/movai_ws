#!/usr/bin/env python
# -*- coding: latin-1 -*-

from GD_Singleton_Mongo import *
import pickle
import time

class Test:
    def test(self, data):
        print 'Test- receive: %s'%(str(data))

    def __init__(self):
        
        GD = GD_Singleton_Mongo()
        

        #Insert Variable
        GD.insertVarRobot("for", {"a":1})
        #Get Variable for
        print GD.getVarRobot("for")
        #Set Variable for.a
        GD.setVarRobot("for.a", 2)
        #Get Variable for.a
        print GD.getVarRobot("for.a")
        
        #Create a subscriber
        GD.sub_varRobotDB({"for":self.test}, 60)
        
        
        #Setting the for.a and check the subscriber
        count = 0
        while(True):
            
            GD.setVarRobot("for.a", count)
            count += 1
            time.sleep(1)
    

test = Test()

