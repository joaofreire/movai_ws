#!/usr/bin/env python
# -*- coding: latin-1 -*-
from pymongo import MongoClient
from MongoThread import *
import pickle

        
class GD_Singleton_Mongo(object):
    __instance = None
    
    def __new__(cls):
        if GD_Singleton_Mongo.__instance is None:
            GD_Singleton_Mongo.__instance = object.__new__(cls)
            #GD_Singleton_Mongo.__initMongo("AGV1")
        return GD_Singleton_Mongo.__instance

    def __init__(self):
         self.__initMongo("AGV1")
        
    def __initMongo(self, robotID):
        self.__client = MongoClient('localhost:27017')
        self.__db = self.__client.movai
        self.__dbrobots = self.__client.movai.robots[robotID]
        self.__dbfleet = self.__client.movai.fleet
        self.__var_local = {}
        self.__var_node = {}
        
        self.__functions = {}
    
    """
        GET variables persistent across consecutive calls to given callback.
    """
    def getVarLocal(self, name):
        return self.__var_local[name] #Through Global variables
    
    """
        SET variables persistent across consecutive calls to given callback.
    """
    def setVarLocal(self, name, value):
        self.__var_local[name] = value

    """
        GET variables persistent across all callbacks in a given Node
    """
    def getVarNode(self, name):
        return self.__var_node[name] #Through Global variables
    
    """
        SET variables persistent across all callbacks in a given Node
    """
    def setVarNode(self, name, value):
        self.__var_node[name] = value
    
    """
        GET variables persistent across all Nodes in a given robot.
    """
    def getVarRobot(self, name):
        return self.__db_getVar('varRobot',name) #Through DB
    
    
    """
        SET variables persistent across all Nodes in a given robot.
    """
    def setVarRobot(self, name, value):
        self.__db_setVar('varRobot', name, value) #Through DB
    
    """
        INSERT variables persistent across all Nodes in a given robot.
    """
    def insertVarRobot(self, name, value):
        self.__db_insertVar('varRobot', name, value) #Through DB


    """
        GET variables persistent across all callbacks of all robots.
    """
    def getVarFleet(self, name):
      return self.__db_getVar('varFleet',name) #Through DB
      
    
    """
        SET variables persistent across all callbacks of all robots.
    """
    def insertVarFleet(self, name, value):
        #~ self.__dbfleet.update_one({},{'$set':{name : value}}, upsert = True)
        self.__db_setVar('varFleet', name, value) #Through DB
       
    """
        INSERT variables persistent across all callbacks of all robots.
    """
    def insertVarFleet(self, name, value):
        #~ self.__dbfleet.update_one({},{'$set':{name : value}}, upsert = True)
        self.__db_insertVar('varFleet', name, value) #Through DB
    
    """
        MONGO
    """
    
    """
        Insert a var in Mongo with serialice
    """
    def __db_insertVar(self, collection_name, name, value):
      #value = pickle.dumps(value)
      
        if "." not in name:
            if collection_name == 'varFleet':
                self.__dbfleet.update_one({"var_type" : collection_name}, {'$set':{self.__getKeyZero(name) : pickle.dumps(value)}}, upsert = True)
                
            elif collection_name == 'varRobot':
                self.__dbrobots.update_one({"var_type" : collection_name}, {'$set':{self.__getKeyZero(name) : pickle.dumps(value)}}, upsert = True)
        else:
            print "It is impossible to insert %s because it includes a dot in the string" %name

    """
        Set a var in Mongo with serialice
    """
    def __db_setVar(self, collection_name, name, value):

        if  self.__db_getVar(collection_name, name) is not None:
            if collection_name == 'varFleet':
                document = self.__dbfleet.find_one({"var_type" : collection_name})
                doc = None
                try:
                    doc = self.__setValue(document, name, value)
                except:
                    doc = value
                self.__dbfleet.update_one({"var_type" : collection_name}, {'$set':{self.__getKeyZero(name) : pickle.dumps(doc)}}, upsert = True)
                
            elif collection_name == 'varRobot':
                document = self.__dbrobots.find_one({"var_type" : collection_name})
                #self.__dbrobots.update_one({"var_type" : collection_name}, {'$set':{name : value}}, upsert = True);
                doc = None
                try:
                    doc = self.__setValue(document, name, value)
                except:
                    doc = value
                self.__dbrobots.update_one({"var_type" : collection_name}, {'$set':{self.__getKeyZero(name) : pickle.dumps(doc)}}, upsert = True)
        else: 
            print 'It is impossible to set %s, because it don\'t exist in the DB' %(name) 
    
    """
        Push a var in Mongo
    """
    def __db_pushVar(self, collection_name, name, value):
      if collection_name == 'varFleet':
        self.__dbfleet.update_one({"var_type" : collection_name}, {'$push':{name : value}}, upsert = True);
        
      elif collection_name == 'varRobot':
        self.__dbrobots.update_one({"var_type" : collection_name}, {'$push':{name : value}}, upsert = True);
    
    """
        Get a Var in mongo
    """
    def __db_getVar(self, collection_name, name):
        document = None
        if collection_name == 'varFleet':
          document = self.__dbfleet.find_one({"var_type" : collection_name})
        elif collection_name == 'varRobot':
          document = self.__dbrobots.find_one({"var_type" : collection_name})
        if document:
            return self.__returnValue(document, name)
            #return document[name]
        else:
            return None
            
    """
        Return a value from a structure as: sensor.x
    """
    def __returnValue(self, doc, key_search):
      try:
        key_array = key_search.split(".")
        valueNext = doc
        for key in key_array:
          try:
            valueNext = pickle.loads(valueNext[key])
          except: #If it cant do a deserialize
            valueNext = valueNext[key]
          
          #valueNext = valueNext[key]
        return valueNext

      except:
        try:
            return pickle.loads(doc[key_search])
        except:
            try:
                return doc[key_search]
            except:
                return None
    """ 
        Set a value 
    """
    def __setValue(self, doc, key_search, val):
        value = None
        key_array = None
        key_cero = None
        try:
            key_array = key_search.split(".")
            value = doc[key_array[0]]
            key_cero = key_array[0]
        except:
            value = doc[key_search]
            key_cero = key_search
        
        #Do a deserialice
        try:
            value = pickle.loads(value)
        except:
            value = value
        
        doc = {key_cero: value}
        self.__nested_set(doc, key_array, val)
        
        return doc[key_cero]

    """
        Set a value in a dictionary
    """
    def __nested_set(self, dic, keys, value):
        for key in keys[:-1]:
            dic = dic.setdefault(key, {})
        dic[keys[-1]] = value
    
    """
        Get a Key from root
    """
    def __getKeyZero(self, key_search):
        try:
            key_array = key_search.split(".")
            return key_array[0]
        except:
            return key_search
            
    #PUB/SUB
    """
        Sub a var fleet
    """
    def sub_varFleetDB(self, function, rate=60):
        uniqueKey = None
        for key in function:
            self.__functions['%s'%key] = function[key]
            uniqueKey = key
                
        thread = self.__new_thread(self.__functions[uniqueKey], rate, self.__dbfleet, uniqueKey)
        thread.daemon = True
        thread.start()
    
    """
        Sub a var Robot
    """
    def sub_varRobotDB(self, function, rate=60):
        uniqueKey = None
        for key in function:
            self.__functions['%s'%key] = function[key]
            uniqueKey = key
                
        thread = self.__new_thread(self.__functions[uniqueKey], rate, self.__dbrobots, uniqueKey)
        thread.daemon = True
        thread.start()
    
    """
        Create the thread
    """
    def __new_thread(self, function, rate, collection, uniqueKey):
        return MongoThread(function = function, rate = rate, collection = collection, key_search=uniqueKey)
        
    
    

