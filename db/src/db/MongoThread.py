#!/usr/bin/env python
# -*- coding: latin-1 -*-

from threading import Thread
import time
from pymongo import MongoClient
from pymongo import CursorType
import pickle



class MongoThread(Thread):

    def __init__(self, function=None, rate = 60, collection = None, key_search = None):
        self.function = function
        self.rate = rate
        self.collection = collection
        self.key_search = key_search
        super(MongoThread, self).__init__()

    def run(self):
        self.initMongo()


    """
        Check the document Keys
    """
    def checkDocumentsKeys(self, docNex, docPre):
        if len(docNex.keys()) == len(docPre.keys()) and docNex.keys() == docPre.keys():
            for key in docNex.keys():
                if not self.checkAllValues(key, docNex, docPre):
                    #return docNex[key]
                    #val = self.returnValue(docNex, self.key_search)
                    return self.returnValue(docNex, docPre,self.key_search)
            return None
        else:
            return None

    """
        Get a value if it has change
    """
    def returnValue(self, docNext, docPre, key_search):
      try:
        key_array = key_search.split(".")
        valueNext = docNext
        valuePre = docPre
        for key in key_array:
          try:

            valueNext = pickle.loads(valueNext[key])
            valuePre = pickle.loads(valuePre[key])
          except: #If it cant do a deserialize
            
            valueNext = valueNext[key]
            valuePre = valuePre[key]
          
        if valueNext != valuePre:
          return valueNext
        else:
          return None
      except Exception, e:
        try: 
          return pickle.loads(docNext[key_search])
        except:
          return docNext[key_search]

    """
        Check the document previous and next
    """
    def checkDocument(self, docNex, docPre):
        if self.checkDocumentsKeys(docNex, docPre):
            self.function(self.checkDocumentsKeys(docNex, docPre))
            return True
        else:
            return False
        
    """
        Check the values of the keys
    """
    def checkAllValues(self, key, docNex, docPre):
        if docNex[key] == docPre[key]:
            return True
        else:
            return False

    """
        Init the thread with polling
    """
    def initMongo(self):
        coll = self.collection
        cursor = coll.find({})

        docPre = None

        while cursor.alive:
            try:
                doc = cursor.next()
                #print doc
                if doc is not None and docPre is not None:
                    #print self.checkDocument(doc, docPre)
                    self.checkDocument(doc, docPre)
                docPre = doc
                cursor = coll.find({})
                time.sleep(1/self.rate)
            except StopIteration:
                time.sleep(1)
