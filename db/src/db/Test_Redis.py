#!/usr/bin/env python
# -*- coding: latin-1 -*-

from GD_Redis import *
import rospy
from geometry_msgs.msg import Twist
import time


GD = GD_Redis()

def handler(msg):  
    print('Handler', msg)
    print "value-%s" %(str(GD.getVarFleet("a")))

def handler2(msg):
    
    print('Handler2', msg)
    print "value-%s" %(str(GD.getVarFleet("test2")))

if __name__ == "__main__":
    #setVariable
    rospy.init_node('Test_Redis', anonymous=True)
    
    
    #~ msg = Twist()
    #~ msg.linear.x = 1
    
    #~ GD.setVarRobot("test/a", msg)
    #~ print GD.getVarRobot("test/a")

    #~ print "Callback"
    #~ GD.sub_VarFleet("a", handler)
    #GD.sub_VarFleet("test2", handler2)
    
    print GD.getProject()
    
    

    rospy.spin()
    
