#!/usr/bin/env python
# -*- coding: latin-1 -*-


import csv
import time 


class CsvReader:
    def reader(self, fileName):
        with open(fileName, 'rb') as f:
            reader = csv.reader(f)
            for row in reader:
                arr = row[0].split(';')
                self.__csvFile.append(arr)
                
    def getHeader(self):
        return self.__csvFile[0]
    
    def getRow(self, row):
        return self.__csvFile[row]
        
    def getPolicyWithOutHeaderMatrix(self):
        policy = []
        for row in xrange(0, len(self.__csvFile)):
            if row != 0:
                policy.append(self.__csvFile[row])
        return policy
        
    def getPolicyMatrix(self):
        return self.__csvFile
    
    def __init__(self):
        self.__csvFile = []
        
        self.reader("test.csv")
        
        

