#!/usr/bin/env python
# -*- coding: latin-1 -*-
from CsvReader import *
import time

class Policy(object):
    
    def __init__(self):
        self.__reader = CsvReader()
        
        
        #params
        self.state = 0
        self.luzOn = False
        
        self.run()
        
    def ___str2bool(self, v):
        return v.lower() in ("yes", "true", "t", "1")
        
    def run(self):
        print "--- Run ---"

        #~ while(True):
            #~ if int(self.__reader.getRow(self.state + 1)[0]) == self.state and self. ___str2bool(self.__reader.getRow(self.state + 1)[1]) == self.luzOn:
                #~ self.doAction(self.__reader.getRow(self.state + 1)[2])
        
        
        if eval(self.__createEvalFun(self.__reader.getHeader(), self.__reader.getRow(self.state + 1))):
            self.doAction(self.__reader.getRow(self.state + 1)[2])
            
             
    def __createEvalFun(self, heather, row):
        num = len(heather) - 1
        s = ""
        for i in xrange(0,len(row) - 1):
            if s == '':
                s += '%s %s' %(str(heather[i]),str(row[i])) 
            else:
                s += ' and %s %s' %(str(heather[i]),str(row[i]))
        
        return s
        
    
    def doAction(self, action):
        print 'Action - %s' %action
        if action == 'turn_on':
            self.state = 1
            self.luzOn = True
            time.sleep(1)
        if action == 'turn_off':
            self.luzOn = False
            self.state = 0
            time.sleep(1)
         




p = Policy()
time.sleep(1)
p.run()
time.sleep(1)
p.run()
