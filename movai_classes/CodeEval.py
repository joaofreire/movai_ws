#!/usr/bin/env python

import sys, traceback
import importlib
from inspect import getframeinfo, stack

class VarException(Exception):
  def __init__(self, _type):
      self.lineno = getframeinfo(stack()[2][0]).lineno
      if _type == 1:
          message = 'Scope of Var does not exist, available scopes are: "CALLBACK", "NODE", "ROBOT", "FLEET"'
      if _type == 2:
          message = 'Key must be a string'
      if sys.version_info >= (3, 0):
          super().__init__(message)  
      else: 
          super(VarException, self).__init__(message)   

class FakeVar(object):
  def __init__(self):
      self.scopes = ['CALLBACK', 'NODE', 'ROBOT', 'FLEET']

  def set(self, scope, key, var):
      if scope not in self.scopes:
          raise VarException(1)
      if type(key) is not str:
          raise VarException(2)
  def get(self,scope, key):
      if scope not in self.scopes:
          raise VarException(1)
      if type(key) is not str:
          raise VarException(2)
  def delete(self, scope, key):
      if scope not in self.scopes:
          raise VarException(1)
      if type(key) is not str:
          raise VarException(2)

class gd(object):

  #need to do something more here

  node_name = ''
  iport = {}
  oport = {}
  params= {}
  def __init__(self):
      pass

class UserCodeEval(object):

  def __init__(self, _cb_data):
      self.code = _cb_data['code']
      #maybe I need to perform some changes here in the code...

      gd_list = ['gd.params', 'gd.iport', 'gd.oport', 'gd.node_name']
      for elem in gd_list:
          self.code = self.code.replace(elem, '""#' + elem) #Ignoring everithing that has a gd attribute -> not the solution... but I have nothing better for now

      Var = FakeVar()
      gd1 = gd()

      self.globals = {"gd":gd1, "Var":Var}
      for lib in _cb_data['imports']:
          self.globals[lib['label']] = importlib.import_module(lib['value'])

      module, self.msg_name = _cb_data['message'].split('/')

      mod = importlib.import_module(module)
      msg = eval('mod.'+self.msg_name)


      if sys.version_info >= (3, 0):  # Our node always run on python 3 so this will not have the if
          self.globals["__builtins__"] = eval('{"min":min, "max":max, "str":str, "print":print}')   #just because python 2.7 misses the parse of print: print -.-'
      else:
          self.globals["__builtins__"] = {"min":min, "max":max, "str":str}

      #print('\n ---   Evaluating the code   ---')
      #print(self.code, '\n')

      self.vars = {'msg': msg}  #WHAT ABOUT THIS ONE????

      #print('     ---   Result   ---  ')

  #The Real stuff
  def check(self):

      try:
          exec(self.code, self.globals, self.vars)
          #print('Everything is fine!\n')
      except SyntaxError as err:
          error_class = err.__class__.__name__
          detail = err.args[0]
          line_number = err.lineno
      except VarException as err:
          error_class = err.__class__.__name__
          detail = err.args[0]
          line_number = err.lineno
      except Exception as err:
          error_class = err.__class__.__name__
          detail = err.args[0]
          cl, exc, tb = sys.exc_info()
          line_number = traceback.extract_tb(tb)[-1][1]
      else:
          return (True, "No error")


      return (False, "%s at line %d: %s" % (error_class, line_number, detail))

#use a message with a error: Bool and a description: str, and maybe some error code?


data = {
       "imports": [
               {"label":"np","value":"numpy"},
               { "value": "math", "label": "math" }
               ],
       "code": "print('hello world my friend')\na = gd.params['sda']\nVar.set('CALLBACK', 'var', 2)\nb=msg.data\nprint(b)",
       "function_name": "something_callback",
       "message": "std_msgs.msg/Bool" 
       }
(success, error) = UserCodeEval(data).check()
print(success, error)
