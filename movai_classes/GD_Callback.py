#!/usr/bin/env python
# -*- coding: utf-8 -*-

import redis
import ast
from User_Code import UserCode
from protocols.Redis import GD_Redis, Redis

class Callback(object):
    """
    There are two ways of using this object:
        -if one provides only a name 'callback_name' it will initialize a new callback.
        -if one provide 'callback_name;callback_version' it will search for the callback with a certain version.
    Version might be 'dev'
    """
    def __init__(self, cb_name, node_name='', port_name='', update=False):
        self.node_name = node_name
        self.port_name = port_name
        self.Py3Lib = [] # list of dicts
        self.oPortDepend = [] # list of dicts
        s = cb_name.split(';')
        if len(s) == 1:
            if len(GD_Redis.db_slave.keys('Callback:' + s[0] + ';*')) > 0:
                print('ERROR: Callback already exists, please provide a version')
                return

            self.Name = cb_name
            print('Callback does not exist!!')
            self.Message = None # string
            self.Code = None # string
            self.Version = 'dev'
            return
        else:
            if s[1] == '':
                print('Error: Please provide a version')
            else:
                self.Name = s[0]
                self.Version = s[1]
       
        self.match_string = 'Callback:' + self.Name + ';' + self.Version + ','

        self.update()
        if update:
            self.subscribe_to_change()

        
    def update(self):
        keys = GD_Redis.db_slave.keys(self.match_string + '*')

        if len(keys) > 0:
            for key in keys:
                key = key#.decode("utf-8")
                comma_split = key.split(',')
                if len(comma_split) > 1:
                    colon_split = comma_split[1].split(':')
                    path = 1
                    if colon_split[0] == 'Message':
                        self.Message = colon_split[1]
                    if colon_split[0] == 'Code':
                        self.Code = GD_Redis.db_slave.get(key)
                        self.Compiled_code = compile(GD_Redis.db_slave.get(key), 'fake', 'exec')
                    if colon_split[0] == 'Py3Lib':
                        lib = {'module':'','class':'','name':''}
                        lib['module'] = colon_split[1]
                        if len(comma_split) > 2:
                            for split in comma_split:
                                s = split.split(':')
                                if s[0] == 'Package':
                                    lib['class'] = s[1]
                                if s[0] == 'As':
                                    lib['name'] = s[1]
                        else :
                            lib['name'] = colon_split[1]
                        self.Py3Lib.append(lib)
                    if colon_split[0] == 'oPortDepend':
                        oportdepend = ast.literal_eval(GD_Redis.db_slave.get(key))#.decode("utf-8"))
                        self.oPortDepend.append(oportdepend)

            self.user = UserCode(self.node_name, self.port_name, self.Py3Lib)

        else:
            print('Error: Callback or Version given does not exist. Please, create first.')

    def save(self, version = None):
        self.delete(version = 'dev')
        basic_key = 'Callback:' + self.Name
        if version:
            basic_key = basic_key +  ';' + version
            self.Version = version
        else:
            basic_key = basic_key +  ';' + 'dev' 
            self.Version = 'dev'

        key = basic_key + ',Message:' + self.Message 
        GD_Redis.db_global.set(key, '')  
        print (key)
        key = basic_key + ',Code:'
        GD_Redis.db_global.set(key, self.Code)  
        print (key)

        for oportdepend in self.oPortDepend:
            key = basic_key + ',oPortDepend:' + oportdepend['Name']
            GD_Redis.db_global.set(key, str(oportdepend))
            print (key)

        for py3lib in self.Py3Lib:
            if py3lib['class'] == '':
                if py3lib['name'] == py3lib['module']:
                    key = basic_key + ',Py3Lib:' + py3lib['module']
                else:
                       key = basic_key + ',Py3Lib:' + py3lib['module']  + ',As:' + py3lib['name']
            else:
                key = basic_key + ',Py3Lib:' + py3lib['module']  + ',Package:' + py3lib['class'] + ',As:' + py3lib['name']

            GD_Redis.db_global.set(key, str(py3lib))
            print (key)
        
        return 0

    def addoPortDepend(self,name,protocol,message):
        exists = False
        for oPortDepend in self.oPortDepend:
            if oPortDepend['Name'] == name:
                oPortDepend['Protocol'] = protocol
                oPortDepend['Message'] = message
                exists = True

        if not exists:
            oportdepend={}
            oportdepend['Name'] = name
            oportdepend['Protocol'] = protocol
            oportdepend['Message'] = message
            self.oPortDepend.append(oportdepend)

    def addPy3Lib(self,imp):
        s = imp.split(' ')
        module = cla = name = ''

        if len(s) == 2:
            name = module = s[1]
        elif len(s) == 4:
            if 'as' in s:
                module = s[1]
                name = s[3]
            else:
                name = module = s[1]
                cla = s[3]
        elif len(s) == 6:
            name = s[5]
            module = s[1]
            cla = s[3]
        else:
            print('ERROR: something went wrong with your import string')
            return

        for lib in self.Py3Lib:
            if lib['module'] == module and lib['class'] == cla and lib['name'] == name:
                print('ERROR: That Py3lib was already added in this callback')
                return

        Py3lib = {}
        Py3lib['name'] = name
        Py3lib['module'] = module
        Py3lib['class'] = cla
        self.Py3Lib.append(Py3lib)

    def delete(self, version = None):

        """ delete specific version of a callback from redis. If no version is given, it will erase all the nodes' entries"""
        if not version:
            match_string = 'Callback:' + self.Name + ';*'
        else:
            match_string = 'Callback:' + self.Name + ';' + version + '*'

        keys = GD_Redis.db_global.keys(match_string)
        for key in keys:
            GD_Redis.db_global.delete(key)
 
    def getAvailableVersions(self):
        match_string = 'Callback:' + self.Name + ';*'
        keys = GD_Redis.db_slave.keys(match_string)
        versions = []
        for key in keys:
            key = key.decode("utf-8")
            try:
                s = key.split(',')[0].split(';')[1]
            except:
                continue
            if s in versions:
                continue
            versions.append(s)
        return versions

    def getLastestVersion(self):
        versions = self.getAvailableVersions()
        if 'dev' in versions: 
            versions.remove('dev')
        if len(versions) == 0:
            print ('No available versions yet. Please save your dev') 
            return ''
        latest = ''
        for version in versions:
            if latest == '':
                latest=version
                continue
            found = False
            i = 0
            while(not found):
                try:
                    version.split('.')[i]
                except:
                    found = True
                    continue
                try:
                    latest.split('.')[i]
                except:
                    latest = version
                    found = True
                    continue
                
                if int(version.split('.')[i]) > int(latest.split('.')[i]):
                    latest = version
                    found = True
                elif int(version.split('.')[i]) < int(latest.split('.')[i]):
                    found = True

                i = i +1
            
        return latest

    def execute(self, msg):
        return self.user.execute(self.Compiled_code, msg)

    def subscribe_to_change(self):
        self.key = self.match_string +'Code:'
        Redis().sub_VarFleet(self.key, self.change_update)

    def change_update(self, result):
        if result['data']  == 'set':#.decode("utf-8")
            print ('Callback "%s" was updated'%self.Name)
            self.update()
    