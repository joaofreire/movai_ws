#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import redis
import time
import ast
from protocols.Redis import GD_Redis

class Node(object):
    def __init__(self, name, version=None, flow=None):
        self.Name = name
        #self.r = Redis('127.0.0.1')
        self.Inst_CommandLine = []
        self.Inst_Parameters = []
        self.Inst_EnvVars = []
        self.Inst_NoFlow = []
        self.iPorts = []
        self.oPorts = [] 
        #search if node is already in the DB
        #scan(self, cursor=0, match=None, count=None)
        #most recent version is in the value of the key "Node:node_name"
        
        match_string = 'Node:' + name
        pre_scan = GD_Redis.db_slave.keys(match_string + ';*')

        
        #search if the node already exists 

        if len(pre_scan) > 0:
            print('Node already exists!!')
            if version:
                if version in self.getAvailableVersions():
                    self.Version = version
                else:
                    print('ERROR: Version not available.')
                    return -1
            else:
                self.Version = self.getLastestVersion()
                if self.Version == '':
                    self.Version = 'dev'

            match_string = match_string + ';' + str(self.Version)

            #get the path that is in the "Node:node_name;node_version" key
            self.Path = GD_Redis.db_slave.get(match_string)#.decode("utf-8")

            keys = GD_Redis.db_slave.keys(match_string + '*')
            copy_keys = keys
            for key in keys:                                   
                #key = key.decode("utf-8")   # CHANGE due to TypeError: a bytes-like object is required, not 'str'
                comma_split = key.split(',')
                #print comma_split
                if len(comma_split) > 1:
                    colon_split = comma_split[1].split(':')
                    path = 1
                    #print colon_split[0]
                    if colon_split[0] == 'Nodelet':
                        self.Nodelet = colon_split[1]
                    elif colon_split[0] == 'Persistent':
                        self.Persistent = GD_Redis.db_slave.get(key) in ['True']
                    elif colon_split[0] == 'Description':
                        self.Description = GD_Redis.db_slave.get(key)
                    elif colon_split[0] == 'Inst_CommandLine':
                        #Node:slam1;2.3,Inst_CommandLine -> {key:-param=,val:4, desc:ND_CMD_param};{key:-g , val:rules.txt, desc:ND_CMD_g}
                        dict_list = GD_Redis.db_slave.get(key).split(';')#(.decode"utf-8")
                         #{key:-param=,val:4, desc:ND_CMD_param}
                        for d in dict_list:
                            inst = Node_Inst()
                            self.Inst_CommandLine.append(inst.fromString(d))

                    elif colon_split[0] == 'Inst_Parameters':
                        #Node:slam1;2.3,Inst_Parameters -> {key:dblayer, val:4, desc:ND_PARAM_dblayer};{key:prefix ,val:0.05, desc:ND_PARAM_prefix}
                        dict_list = GD_Redis.db_slave.get(key).split(';')#.decode("utf-8")
                        for d in dict_list:
                            inst = Node_Inst()
                            self.Inst_Parameters.append(inst.fromString(d))

                    elif colon_split[0] == 'Inst_EnvVars':
                        #Node:slam1;2.3,Inst_EnvVars -> {key:ROS_ROOT, val:/home/mov.ai/ros,desc:ND_ENV_ROS_ROOT};{key:PYTHONPATH,val:/home/mov.ai/python,desc:ND_ENV_PYTHONPATH}
                        dict_list = GD_Redis.db_slave.get(key).split(';')#.decode("utf-8")
                        for d in dict_list:
                            inst = Node_Inst()
                            self.Inst_EnvVars.append(inst.fromString(d))

                    elif colon_split[0] == 'Inst_NoFlow':
                        #Node:slam1;2.3,Inst_NoFlow -> {key:x,val:200, desc: ND_NoFlow_x}, {key:y, val: 200, desc: ND_NoFlow_y}, {key:icon, val:hammer, desc:ND_NoFlow_icon}
                        dict_list = GD_Redis.db_slave.get(key).split(';')#.decode("utf-8")
                        for d in dict_list:
                            inst = Node_Inst()
                            self.Inst_NoFlow.append(inst.fromString(d))

                    elif colon_split[0] == 'iPort':
                        #Node:image_proc,iPort:raw_image,Protocol:ROS1/Subscribe
                        #Node:image_proc,iPort:raw_image,Message:sensor_msgs/Image
                        #Node:image_proc,iPort:raw_image,Path -> /path/to/file
                        #Node:laser_squash;3.2.1,iPort:raw_image,Callback:qr_detect;4
                        #Node:laser_squash;3.2.1,iPort:raw_image,CallbackUpdate -> true
                        #Node:laser_squash;3.2.1,iPort:raw_image,Immutable -> true
                        iport = {}  
                        iport_name = colon_split[1]
                        got_iport = False
                        for iPort in self.iPorts:

                            if iPort['Name'] == iport_name:
                                got_iport = True
                                arg = comma_split[2]
                                a = arg.split(':')
                                if a[1] == '':
                                    iPort[a[0]] = GD_Redis.db_slave.get(key)#.decode("utf-8")
                                else:
                                    iPort[a[0]] = a[1]
                                break
                        if not got_iport:
                            iport['Name'] = iport_name
                            arg = comma_split[2]
                            a = arg.split(':')
                            if a[1] == '':
                                iport[a[0]] = GD_Redis.db_slave.get(key)#.decode("utf-8")
                            else:
                                iport[a[0]] = a[1]
                            self.iPorts.append(iport)
                    elif colon_split[0] == 'oPort':
                        #Node:image_proc,oPort:raw_image,Protocol:ROS1/Publish
                        #Node:image_proc,oPort:raw_image,Message:sensor_msgs/Image
                        #Node:image_proc,oPort:raw_image,Immutable -> true
                        oport = {}  
                        oport_name = colon_split[1]
                        got_oport = False
                        for oPort in self.oPorts:

                            if oPort['Name'] == oport_name:
                                got_oport = True
                                arg = comma_split[2]
                                a = arg.split(':')
                                if a[1] == '':
                                    oPort[a[0]] = GD_Redis.db_slave.get(key)#.decode("utf-8")
                                else:
                                    oPort[a[0]] = a[1]
                                break
                        if not got_oport:
                            oport['Name'] = oport_name
                            arg = comma_split[2]
                            a = arg.split(':')
                            if a[1] == '':
                                oport[a[0]] = GD_Redis.db_slave.get(key)#.decode("utf-8")
                            else:
                                oport[a[0]] = a[1]
                            self.oPorts.append(oport)
        else:
            print('Node does not exist!!')
            self.Version = 1.0
            self.Nodelet = None
            self.Path = None
            self.Persistent = None
            self.Description = None

    def addiPort(self,name,protocol,message,callback,callbackUpdate,remap,immutable):
        exists = False
        for iPort in self.iPorts:
            if iPort['Name'] == name:
                iPort['Protocol'] = protocol
                iPort['Message'] = message
                iPort['Callback'] = callback
                iPort['CallbackUpdate'] = callbackUpdate
                iPort['Immutable'] = immutable
                iPort['Remap'] = remap
                exists = True
        if not exists:
            iport={}
            iport['Name'] = name
            iport['Protocol'] = protocol
            iport['Message'] = message
            iport['Callback'] = callback
            iport['CallbackUpdate'] = callbackUpdate
            iport['Immutable'] = immutable
            iport['Remap'] = remap
            self.iPorts.append(iport)

    def addoPort(self,name,protocol,message,remap,immutable):
        exists = False
        for oPort in self.oPorts:
            if oPort['Name'] == name:
                oPort['Protocol'] = protocol
                oPort['Message'] = message
                oPort['Immutable'] = immutable
                oPort['Remap'] = remap
                exists = True
        if not exists:
            oport={}
            oport['Name'] = name
            oport['Protocol'] = protocol
            oport['Message'] = message
            oport['Immutable'] = immutable
            oport['Remap'] = remap
            self.oPorts.append(oport)

    def addInstCommandLine(self,key,val,desc):
        got = False
        for inst in self.Inst_CommandLine:
            if inst['key'] == key:
                got = True
                inst['val'] = val
                inst['desc'] = desc
        if not got:
            instance = {}
            instance['key'] = key
            instance['val'] = val
            instance['desc'] = desc
            self.Inst_CommandLine.append(instance)

    def addInstParameter(self,key,val,desc):
        got = False
        for inst in self.Inst_Parameters:
            if inst['key'] == key:
                got = True
                inst['val'] = val
                inst['desc'] = desc
        if not got:
            instance = {}
            instance['key'] = key
            instance['val'] = val
            instance['desc'] = desc
            self.Inst_Parameters.append(instance)

    def addInstEnvVar(self,key,val,desc):
        got = False
        for inst in self.Inst_EnvVars:
            if inst['key'] == key:
                got = True
                inst['val'] = val
                inst['desc'] = desc
        if not got:
            instance = {}
            instance['key'] = key
            instance['val'] = val
            instance['desc'] = desc
            self.Inst_EnvVars.append(instance)

    def addInstNoFlow(self,key,val,desc):
        got = False
        for inst in self.Inst_NoFlow:
            if inst['key'] == key:
                got = True
                inst['val'] = val
                inst['desc'] = desc
        if not got:
            instance = {}
            instance['key'] = key
            instance['val'] = val
            instance['desc'] = desc
            self.Inst_NoFlow.append(instance)

    def rmiPort(self,name):
        got = False
        for iport in self.iPorts:
            if iport['Name'] == name:
                self.iPorts.remove(iport)
                got = True
                break
        if not got:
            print ("iPort doesn't exist")

    def rmoPort(self,name):
        got = False
        for oport in self.oPorts:
            if oport['Name'] == name:
                self.oPorts.remove(oport)
                got = True
                break
        if not got:
            print ("oPort doesn't exist")

    def rmInstCommandLine(self,key):
        got = False
        for instance in self.Inst_CommandLine:
            if instance['key'] == key:
                self.Inst_CommandLine.remove(instance)
                got = True
                break
        if not got:
            print ("InstCommandLine doesn't exist")

    def rmInstParamater(self,key):
        got = False
        for instance in self.Inst_Parameters:
            if instance['key'] == key:
                self.Inst_Parameters.remove(instance)
                got = True
                break
        if not got:
            print ("InstParameter doesn't exist")

    def rmInstEnvVar(self,key):
        got = False
        for instance in self.Inst_EnvVars:
            if instance['key'] == key:
                self.Inst_EnvVars.remove(instance)
                got = True
                break
        if not got:
            print ("InstEnvVar doesn't exist")

    def rmInstNoFlow(self,key):
        got = False
        for instance in self.Inst_NoFlow:
            if instance['key'] == key:
                self.Inst_NoFlow.remove(instance)
                got = True
                break
        if not got:
            print ("InstParameter doesn't exist")

    def save(self, version = None):
        self.delete(version = 'dev')
        basic_key = 'Node:' + self.Name
        #Node:node_name
        #GD_Redis.db_global.set(basic_key, self.Version)
        #print basic_key
        if version:
            basic_key = basic_key +  ';' + version
            self.version = version
        else:
            basic_key = basic_key +  ';' + 'dev' 
            self.version = 'dev'
        GD_Redis.db_global.set(basic_key, self.Path)
        print (basic_key)
        #Node:node_name;version,Nodelet:nodelet_name
        if self.Nodelet:
            key = basic_key + ',Nodelet:' + self.Nodelet 
            GD_Redis.db_global.set(key, '')  
            print (key)
        #Node:node_name;version,Persistent: -> true/false
        key = basic_key + ',Persistent:'
        GD_Redis.db_global.set(key, str(self.Persistent))
        print (key)
        #Node:node_name;version,Description: -> this node blablalbllabl
        key = basic_key + ',Description:'
        GD_Redis.db_global.set(key, self.Description)
        print (key)
        #Node:slam1;2.3,Inst_CommandLine ->{key:-param=,val:4, desc:ND_CMD_param};{key:-g , val:rules.txt, desc:ND_CMD_g}
        if len(self.Inst_CommandLine) > 0:
            value = ''
            for instance in self.Inst_CommandLine:
                value = value + str(instance) + ';'
            key = basic_key + ',Inst_CommandLine:'
            GD_Redis.db_global.set(key, value[:-1])
            print (key)
        #Node:slam1;2.3,Inst_Parameters -> {key:dblayer, val:4, desc:ND_PARAM_dblayer};{key:prefix ,val:0.05, desc:ND_PARAM_prefix}
        if len(self.Inst_Parameters) > 0:
            value = ''
            for instance in self.Inst_Parameters:
                value = value + str(instance) + ';'
            key = basic_key + ',Inst_Parameters:'
            GD_Redis.db_global.set(key, value[:-1])
            print (key)
        #Node:slam1;2.3,Inst_EnvVars -> {key:ROS_ROOT, val:/home/mov.ai/ros, desc:ND_ENV_ROS_ROOT};{key:PYTHONPATH,val:/home/mov.ai/python,desc:ND_ENV_PYTHONPATH}
        if len(self.Inst_EnvVars) > 0:
            value = ''
            for instance in self.Inst_EnvVars:
                value = value + str(instance) + ';'
            key = basic_key + ',Inst_EnvVars:'
            GD_Redis.db_global.set(key, value[:-1])
            print (key)
        #Node:slam1;2.3,Inst_NoFlow -> {key:x,val:200, desc: ND_NoFlow_x}, {key:y, val: 200, desc: ND_NoFlow_y}, {key:icon, val:hammer, desc:ND_NoFlow_icon}
        if len(self.Inst_NoFlow) > 0:    
            value = ''
            for instance in self.Inst_NoFlow:
                value = value + str(instance) + ';'
            key = basic_key + ',Inst_NoFlow:'
            GD_Redis.db_global.set(key, value[:-1])
            print (key)
        #Node:image_proc,iPort:raw_image,Protocol:ROS1/Subscribe
        #Node:image_proc,iPort:raw_image,Message:sensor_msgs/Image
        #Node:image_proc,iPort:raw_image,Path -> /path/to/file
        #Node:laser_squash;3.2.1,iPort:raw_image,Callback:qr_detect;4
        #Node:laser_squash;3.2.1,iPort:raw_image,CallbackUpdate -> true
        #Node:laser_squash;3.2.1,iPort:raw_image,Immutable -> true
        if len(self.iPorts) > 0:
            for iport in self.iPorts:
                iport_basic_key = basic_key + ',iPort:' + iport['Name']
                key = iport_basic_key + ',Protocol:' + iport['Protocol']
                GD_Redis.db_global.set(key, '')
                print (key)
                key = iport_basic_key + ',Message:' + iport['Message']
                GD_Redis.db_global.set(key, '')
                print (key)
                #key = iport_basic_key + ',Path:'
                #GD_Redis.db_global.set(key, iport['path'])
                key = iport_basic_key + ',Callback:' + iport['Callback']
                GD_Redis.db_global.set(key, '')
                print (key)
                key = iport_basic_key + ',CallbackUpdate:'
                GD_Redis.db_global.set(key, str(iport['CallbackUpdate']))
                print (key)
                key = iport_basic_key + ',Immutable:'
                GD_Redis.db_global.set(key, str(iport['Immutable']))            
                print (key)
                key = iport_basic_key + ',Remap:'
                GD_Redis.db_global.set(key, str(iport['Remap']))            
                print (key)
        #Node:image_proc,oPort:raw_image,Protocol:ROS1/Publish
        #Node:image_proc,oPort:raw_image,Message:sensor_msgs/Image
        #Node:image_proc,oPort:raw_image,Immutable -> true
        if len(self.oPorts) > 0:
            for oport in self.oPorts:
                oport_basic_key = basic_key + ',oPort:' + oport['Name']
                key = oport_basic_key + ',Protocol:' + oport['Protocol']
                GD_Redis.db_global.set(key, '')
                print (key)
                key = oport_basic_key + ',Message:' + oport['Message']
                GD_Redis.db_global.set(key, '')
                print (key)
                key = oport_basic_key + ',Immutable:'
                GD_Redis.db_global.set(key, str(oport['Immutable'])) 
                print (key)
                key = oport_basic_key + ',Remap:'
                GD_Redis.db_global.set(key, str(oport['Remap'])) 
                print (key)

    def getAvailableVersions(self):
        match_string = 'Node:' + self.Name + ';*'
        keys = GD_Redis.db_slave.keys(match_string)
        versions = []
        for key in keys:
            key = key#.decode("utf-8")
            try:
                s = key.split(',')[0].split(';')[1]
            except:
                continue
            if s in versions:
                continue
            versions.append(s)
        return versions

    def getLastestVersion(self):
        versions = self.getAvailableVersions()
        #print(versions)
        if 'dev' in versions: 
            versions.remove('dev')
        if len(versions) == 0:
            print ('No available versions yet. Please save your dev') 
            return ''
        latest = ''
        for version in versions:
            if latest == '':
                latest=version
                continue
            found = False
            i = 0
            while(not found):
                try:
                    version.split('.')[i]
                except:
                    found = True
                    continue
                try:
                    latest.split('.')[i]
                except:
                    latest = version
                    found = True
                    continue
                # print i
                # print version.split('.')[i]
                # print latest.split('.')[i]
                
                if int(version.split('.')[i]) > int(latest.split('.')[i]):
                    latest = version
                    found = True
                elif int(version.split('.')[i]) < int(latest.split('.')[i]):
                    found = True

                i = i +1
            
        return latest

    def updateVersion(self,version):
        self.delete(version = version)
        self.save(version)
        self.delete(version = 'dev')
        self.Version = version
        pass

    def toJSON(self):
        pass
    def fromJSON(self):
        pass
    def delete(self, version = None):

        """ delete specific version of a node form redis. If no version is given, it will erase all the nodes' entries"""
        if not version:
            match_string = 'Node:' + self.Name + ';*'
        else:
            match_string = 'Node:' + self.Name + ';' + version + '*'

        keys = GD_Redis.db_global.keys(match_string)
        for key in keys:
            GD_Redis.db_global.delete(key)

class Node_Inst(object):

    def __init__(self, key=None, val=None, desc=None):
        #Command line variable
        self.key = key
        self.val = val
        self.desc = desc

    def toString(self):
        d = {}
        d['key'] = key
        d['val'] = val
        d['desc'] = desc

        return str(d)

    def fromString(self,s):
        d = ast.literal_eval(s)
        self.key = d['key']
        self.val = d['val']
        self.desc = d['desc']
        return d

class iPort(object):
    def __init__(self, name, protocol=None, message=None, callback=None, callbackUpdate=None, immutable=None):
        self.name = name
        self.protocol = protocol
        self.message = message
        self.callback = callback
        self.callbackUpdate = callbackUpdate
        self.immutable = immutable

    def toString(self):
        d[self.name] = name
        d[self.protocol] = protocol
        d[self.message] = message
        d[self.callback] = callback
        d[self.callbackUpdate] = callbackUpdate
        d[self.immutable] = immutable
        
        return str(d)

    def fromString(self,s,d=None):
        if not d:
            d = ast.literal_eval(s)
        self.name = d[self.name]
        self.protocol = d[self.protocol]
        self.message = d[self.message]
        self.callback = d[self.callback]
        self.callbackUpdate = d[self.callbackUpdate]
        self=immutable = d[self.immutable]

class oPort(object):
    def __init__(self, name, protocol=None, immutable=None):
        self.name = name
        self.protocol = protocol
        self.immutable = immutable

    def toString(self):
        d[self.name] = name
        d[self.protocol] = protocol
        d[self.message] = message
        d[self.immutable] = immutable
        
        return str(d)

    def fromString(self,s, d = None):
        if not d:
            d = ast.literal_eval(s)
        self.name = d[self.name]
        self.protocol = d[self.protocol]
        self.message = d[self.message]
        self=immutable = d[self.immutable]