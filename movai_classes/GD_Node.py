#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, time
from GD import GD_User
from GD_Luis import Node as no
from GD_Protocols import Iport, Oport, Transports

from protocols import MovAI
from protocols.Redis import Redis, GD_Redis
#from protocols.ROS2 import *

#get the instance name as argv
name = 'lidar2'
if len(sys.argv)>1:
    name = sys.argv[1]

print('\nStarting GD_Node instance with name:', name)
Redis(name, 'agv1') #need to pass the robot name

#la = ROS2_INIT('node_ros2')

#ROS2_Subscriber('s', 'c')


node = no(name)

#Check all transport types and initialize them in threads?
transports = {'ROS1':False, 'ROS2':False, 'Flask':False, 'Redis':True, 'SocketIO':False}

#Lets check which transport types are we using in this node
for port in node.iPorts + node.oPorts:
    for key in transports:
        if key in port['Protocol']:
            transports[key] = True

#And initialize each of the transports
for key in transports:
    if transports[key]:
        Transports(node.Name, key)

time.sleep(0.1)  #for the tranports to initialize... need a better way to check this 

#params are available all over the node as gd.params['name']. The description is only for UI
for p in node.Inst_Parameters:
    GD_User.params[p['key']] = p['val']

#first we start the oports
for o in node.oPorts:
    Oport(name, o['Name'], o['Protocol'], o['Remap'], o['Message'])

#then we run the user init callback
for i in node.iPorts:
    if i['Protocol'] == 'Init':
        MovAI.Init(name, i['Name'], i['Callback'])  #yes, we have our own MovAI protocol #badass

#and finnaly we start the iports
for i in node.iPorts: 
    Iport(name, i)  #pass node name and iport info

#Keep the node running
if not transports['Flask']:
    #pass
    MovAI.Run()

#rclpy.spin(ROS2.node)

#TO DO:
#on_shutdown correr o on_exit? ...

#WHERE IS THE ON EXIT??
#Maybe a callback to the database that triggers when the exit call is made?
#Maybe a triiger in the node side that runs after the gd.send['transition_port']?
#We should guarantee that the other stuff is not running (subscribers and shit) when the on_exit runs... this can only be achieved with iport stop()
