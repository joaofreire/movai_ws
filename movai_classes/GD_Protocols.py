#!/usr/bin/env python

from protocols import *
from GD import GD_User as gd

class Transports(object):
	def __init__(self, _node_name, _type):
		if _type == 'ROS1':
			ROS1.ROS1(_node_name)
		if _type == 'Redis':
			Redis.Redis(_node_name, "10.10.0.60", 6379)
		if _type == 'Flask':
			SocketIO.Socketio('localhost', 5000).run()  # Yes, naming is not ok
		if _type == 'ROS2':
			print('WOW, trying to use ROS2... good luck with that')
		if _type == 'SocketIO':
			pass  #same as flask
	
class Iport(object):
	def __init__(self, _node_name, _iport):

		_name = _iport['Name']
		_type = _iport['Protocol']
		print('Iport:', _name, _type)

		if _type == "ROS1/Subscriber":   #"ROS1/Subscribe"
			gd.iport[_name] = ROS1.ROS1_Subscriber(_node_name, _iport)
		if _type == "ROS1/Service":
			gd.oport['reply@' + _name] = ROS1.ROS1_ServiceServerReply()
			gd.iport[_name] = ROS1.ROS1_ServiceServer(_node_name, _iport)
		if _type == "ROS1/Action":
			gd.iport[_name] = ROS1.ROS1_ActionServer(_node_name, _iport)
		if _type == "ROS1/Timer":
			gd.iport[_name] = ROS1.ROS1_Timer(_node_name, _iport)
		if _type == "Redis/Subscriber":
			gd.iport[_name] = RedisSub.Redis_Subscriber(_node_name, _iport)
		if _type == "FlaskSocketIO/Subscriber":
			gd.iport[_name] = SocketIO.SIO_Sub(_node_name, _iport, SocketIO.SIO.sio)

class Oport(object):
	def __init__(self, _node_name, _name, _type, _topic, _message):

		print('Oport:', _name, _type)
		
		if _type == "ROS1/Publisher":   #"ROS1/Publish"
			gd.oport[_name] = ROS1.ROS1_Publisher(_topic, _message)
		if _type == "ROS1/Service":
			gd.oport[_name] = ROS1.ROS1_ServiceClient(_topic, _message)
		if _type == "ROS1/Action":
			gd.oport[_name] = ROS1.ROS1_ActionClient(_topic, _message)
		if _type == "MovAI/Transition":
			gd.oport[_name] = MovAI.Transition(_node_name, _name)
		if _type == "SocketIO/Publisher":
			gd.oport[_name] = SocketIO.SIO_Pub(_topic, _message)


class Params(object):
	def __init__(self, _name, _value):
		gd.params[_name] = _value
