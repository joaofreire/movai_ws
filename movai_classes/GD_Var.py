#!/usr/bin/env python
from protocols.Redis import GD_Redis

class Var(object):

    def __init__(self, _node_name, _port_name):

        self.node_name = _node_name
        self.port_name = _port_name

    def getScopeKey(self, scope, key):
        _key = ''
        local = True
        if scope == 'CALLBACK':
            _key = 'Var:%s,Node:%s,iPort:%s'%(key, self.node_name,self.port_name)
        elif scope == 'NODE':
            _key = 'Var:%s,Node:%s'%(key, self.node_name)
        elif scope == 'ROBOT':
            _key = 'Var:%s'%(key)
        elif scope == 'FLEET':
            _key = 'Var:%s'%(key)
            local = False
        
        return _key, local

    def getLock(self, scope, key):
        if scope == 'ROBOT':
            unique_ID = self.node_name
        else:
            unique_ID = self.node_name + "_" + self.port_name
        lock_key = "Lock_"+ key
        lockid = GD_Redis.db_local.get(lock_key)
        if lockid == unique_ID or lockid == None:
            return True, lock_key, unique_ID
        else:
            return False, '',''

    
    def set(self,scope, key, var, Lock = False, timeout = 20):
        
        _key, local = self.getScopeKey(scope, key)
        
        can_lock, lock_key, lockid = self.getLock(scope, _key)
            
        if can_lock:
            if Lock:
                sucess_lock = GD_Redis.db_local.set(lock_key, lockid, ex=timeout, nx = Lock)
                if sucess_lock:
                    can_write = True
                else:
                    can_write = False
            else:
                GD_Redis.db_local.delete(lock_key)
                can_write = True
        else:
            print ("Already locked from", lockid)

        if can_write:
            if local:
                GD_Redis.db_local.set(_key,var)
            else:
                GD_Redis.db_global.set(_key,var)   
        
        return can_write


    def get(self, scope, key, Lock=False, timeout=20):
        _key, local = self.getScopeKey(scope, key)
        var = None

        if local:
            var = GD_Redis.db_local.get(_key)
        else:
            var = GD_Redis.db_global.get(_key)

        if Lock:
            can_lock, lock_key, lockid = self.getLock(scope, _key)
            if can_lock:
                sucess_lock = GD_Redis.db_local.set(lock_key, lockid, ex=timeout, nx = Lock)

        return var 

    def delete(self, scope, key):
        _key, local = self.getScopeKey(scope, key)
        
        can_lock, lock_key, lockid = self.getLock(scope, _key)
        if can_lock:
            if local:
                GD_Redis.db_local.delete(_key)
            else:
                GD_Redis.db_global.delete(_key)
            return True
        else:
            return False


