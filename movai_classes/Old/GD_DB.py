#!/usr/bin/env python

import redis, time
from threading import Thread

class GD_DB(object):
    def __init__(self, _thread=None):
        self.db = redis.StrictRedis(host='10.10.0.60', port=6379, db=0)
        self.pubsub = self.db.pubsub()
        self.thread = _thread


    def sub_VarFleet(self, key,function):
        self.pubsub.psubscribe(**{'__keyspace@0__:%s'%(key): function})
        self.__start()
    
    def __start(self):
        if self.thread is None:       
            self.thread = RedisThread(self)
            self.thread.daemon = True
            self.thread.start()


class RedisThread(Thread):

    def __init__(self, GD):
        self.GD = GD
        super(RedisThread, self).__init__()

    def run(self):
        #print('Starting message loop')  
        while True:  
            message = self.GD.pubsub.get_message()
            if message:
                pass
                #print(message)
            else:
                time.sleep(0.01)

