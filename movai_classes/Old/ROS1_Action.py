#!/usr/bin/env python

import importlib
import rospy
from callback import Callback
from GD import GD_Base
import actionlib

class ROS1_ActionServer(object):
    def __init__(self, _topic, _message, _cb):
        self.topic = _topic
        self.message = _message
        vars()['msg_mod'] = importlib.import_module(GD_Base().get_msg_package(self.message)+'.msg._'+self.message)
        self.cb = Callback(_cb.name, _cb.libs, _cb.msgs)
        self._as = actionlib.SimpleActionServer(self.topic, eval('msg_mod'+'.'+self.message), execute_cb=self.callback, auto_start = False)
        self._as.start()

    def callback(self, msg):
        self.cb.execute(msg)

    def send_result(self, msg): #sends the result....
        self._as.set_succeeded(msg)

    def send_feedback(self, msg): #sends the feedback
        self._as.publish_feedback(msg)


class ROS1_ActionClient(object):
    def __init__(self, _topic, _message):
        self.topic = _topic
        self.message = _message
        vars()['msg_mod'] = importlib.import_module(GD_Base().get_msg_package(self.message)+'.msg._'+self.message)
        self._ac = actionlib.SimpleActionClient(self.topic, eval('msg_mod'+'.'+self.message))

    def send(self, msg):
        self._ac.send_goal(msg)
