#!/usr/bin/env python

import importlib
import rospy
from GD import GD_Base 

class ROS1_Publisher(object):
	def __init__(self,_topic, _message):

		# every import will have the format: TypedDict('from': str, 'import': str, 'as': str)

		vars()['msg_mod'] = importlib.import_module(GD_Base().get_msg_package(_message)+'.msg._'+_message)
		#vars()[_message['module']] = importlib.import_module(_message['package'])
		self.pub = rospy.Publisher(_topic, eval('msg_mod'+'.'+_message), queue_size=1)
				
	def send(self, msg):
		self.pub.publish(msg)

	def get_message(self):
		return 'Mensagem...'

'''
class ROS1_Publish:
    name = "" # port name
    pub = None
    msg = None

    def __init__(self, name: str, message_name: str)

        
        # make sure ROS1 has been initialized
        #if SG.ROS1 == None:
        #    SG.ROS1 = ROS1()
         
   
        msg = GD.Message(message_name)
        #msg.compiled # needs to be imported to become a class
        #msg.source # used by UI editor 
        #msg.message_class # = eval(msg.compiled).String        
        
        # start the publisher, use queue_size of 1. We control queue
        # the remaps are from the command line as sys.argv[] ? or passed
        # creates an instance of GD Message which includes a ROS
        # message because we use ROS framework for messages
        self.pub = SG.ROS1.rospy.Publisher(name, Remap ?,
            GD.Message(message_name).msg, queue_size=1)

        self.msg = 
            
        
    def send(msg: GD.Message):
        # publish via ROS1/Publish protocol
        pub.publish(msg)

    def getmessage():
'''