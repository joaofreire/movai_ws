#!/usr/bin/env python

import importlib
import rospy
from callback import Callback
from GD import GD_Base 

class ROS1_ServiceServer(object):
    def __init__(self, _topic, _service, _cb): #callback info
        self.topic = _topic
        self.service = _service
        globals()['srv_mod'] = importlib.import_module(GD_Base().get_srv_package(self.service)+'.srv._'+self.service)
        self.cb = Callback(_cb.name, _cb.libs, _cb.msgs)
        self.sub = rospy.Service(self.topic, eval('srv_mod'+'.'+self.service), self.callback)
        
    def callback(self, msg):
        return self.cb.execute(msg)

class ROS1_ServiceProxy(object):
    def __init__(self, _topic, _service): #callback info
        self.topic = _topic
        self.service = _service
        globals()['srv_mod'] = importlib.import_module(GD_Base().get_srv_package(self.service)+'.srv._'+self.service)
        self.pub = rospy.ServiceProxy(self.topic, eval('srv_mod'+'.'+self.service))
        
        
    def send(self, srv):
	    self.pub(srv)