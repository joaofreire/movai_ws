#!/usr/bin/env python

import importlib
import rospy
from callback import Callback
from GD import GD_Base 

class ROS1_Subscriber(object):

	def __init__(self, _topic, _message, _cb): 
		self.topic = _topic
		self.message = _message
		globals()['msg_mod'] = importlib.import_module(GD_Base().get_msg_package(self.message)+'.msg._'+self.message)
		self.cb = Callback(_cb.name, _cb.libs, _cb.msgs)
		self.sub = rospy.Subscriber(self.topic, eval('msg_mod'+'.'+self.message), self.callback)
		
	def callback(self, msg):
		self.cb.execute(msg)

	def unregister(self):
		self.sub.unregister()
		self.sub = None

	def register(self):
		if self.sub is None:
			self.sub = rospy.Subscriber(self.topic, eval('msg_mod'+'.'+self.message), self.callback)
