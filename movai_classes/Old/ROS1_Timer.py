#!/usr/bin/env python

import importlib
import rospy
from callback import Callback
from GD import GD_Base 

class ROS1_Timer(object):
	def __init__(self,_name, _duration, _cb):
		#handle imports
		self.name = _name
		self.duration = float(_duration)
		self.timer = rospy.Timer(rospy.Duration(self.duration), self.callback)
		self.cb = Callback(_cb.name, _cb.libs, _cb.msgs)
		
	def callback(self, msg):
		self.cb.execute(msg)

	def unregister(self):
		self.timer.shutdown()
		
	def register(self):
		if self.timer._shutdown == True:
			self.timer = rospy.Timer(rospy.Duration(self.duration), self.callback)
