#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import importlib 
#from db import DB_Subscriber  
from GD import GD_Base, GD
from GD_DB import GD_DB  
from user_code import UserCode
from import_json import Import  #Only temporary

class Import():  #PLS GET RID OF THIS
    def __init__(self, _type, _name):
        self.type = _type
        self.name = _name

class Callback(object):
    def __init__(self, _name): 
        
        self.imports = list()
        self.cb_name = _name
        #get this from Franscisco class
        self.libraries = [{'module': 'numpy', 'class': '', 'name': 'np'}, {'module': 'std_msgs.msg', 'class': 'Bool', 'name': 'Bool'}, 
                        {'module': 'std_srvs.srv', 'class': 'SetBoolResponse', 'name': 'SetBoolResponse'}]

        for lib in self.libraries:
            mod = importlib.import_module(lib['module'])
            if lib['class'] == '':
                self.imports.append(Import(mod, lib['name']))
            else:
                self.imports.append(Import(eval('mod.'+lib['class']), lib['name']))

        self.DB_Subscribe(_name)
        self.user = UserCode(self.imports)

    def execute(self, msg):
        return self.user.execute(self.code, msg)
    
    def DB_Subscribe(self, _name):
        self.GD = GD()
        print (_name)
        self.key = 'project/CallbackPool/cb/' + _name
        #self.code = self.GD.getCb(self.key)
        self.code = compile(self.GD.getCb(self.key), 'fake', 'exec')  #we used compiled version for improvements when running multiple times the same callback
        GD_DB().sub_VarFleet(self.key, self.DB_Subscribe_update)

    def DB_Subscribe_update(self, result):
        print ('Callback "%s" was updated 22222222222222'%self.cb_name)
        #self.code = self.GD.getCb(self.key)
        self.code = compile(self.GD.getCb(self.key), 'fake', 'exec')


        ''' old stuff, still gold
        self.imports = _imports
        for i in _messages:
            vars()[i + '_mod'] = importlib.import_module(GD_Base().get_msg_package(i)+'.msg._'+i)
            new = Import(eval(i+'_mod'+'.'+i), i)
            self.imports.append(new)

        for j in self.libraries:
            j.type = importlib.import_module(j.type)
            self.imports.append(j)

        for j in self.libraries:
            globals()[j.name] = j.type
        '''