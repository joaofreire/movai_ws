#!/usr/bin/env python

import os
import json
import rosmsg, rospkg

class Import():
    def __init__(self, _type, _name):
        self.type = _type
        self.name = _name

class Callback():
    def __init__(self, _name):
        self.name = _name
        self.libs = [Import('numpy', 'np'),Import('math', 'math') ]
        self.msgs = ['Bool', 'UInt8', 'GripperResult']   #Should we receive the pakcage? std_msgs?
        self.srvs = ['SetBool']

class Port():
    def __init__(self, _type, _name, _message, _topic, _cb_name=None):
        self.type = _type
        self.name = _name
        self.topic = _topic
        self.message = _message
        if _cb_name is not None:
            self.callback = Callback(_cb_name)

class Node():
    def __init__(self):
        self.name = ''
        self.type = ''
        self.description = ''
        self.params = list()
        self.imports = list()
        self.inputs = list()
        self.outputs = list()
        self.outcomes = list()

'''
Class that generates a custom node for the Mov.ai software  
'''
class GetStuff():

    def __init__(self):
        self.nodes_list = list()

    def parse_node_json(self, _filename):
        with open(_filename) as json_data:
            data = json.load(json_data,) 
        for s in data:
            node = Node()
            node.name = str(s)
            node.type = data[s]["nodeType"]
            node.description = data[s]["description"]
            for p in data[s]["params"]:
                node.params.append(str(p))
            #node.params = str(data[s]["params"])
            if data[s]["inports"]:
                for in_port in data[s]["inports"]:
                    # Port class receives (_type, _name, _message, _topic)
                    port = Port(in_port["port_type"],in_port["name"],in_port["type"],in_port["name"], in_port["cb"])
                    node.inputs.append(port)
                    # Import class receives (_type, _name))
                    node.imports.append(Import(in_port["port_type"], in_port["type"]))

            if data[s]["outports"]:
                for out_port in data[s]["outports"]:
                    if out_port["port_type"] == 'Transition':
                        node.outcomes.append(str(out_port["name"]))
                    #else:
                    port = Port(out_port["port_type"],out_port["name"],out_port["type"],out_port["name"])
                    node.outputs.append(port)
                    # Import class receives (_type, _name))
                    node.imports.append(Import(out_port["port_type"], out_port["type"]))
            self.nodes_list.append(node)

    def generate_node(self, node):
        self.callbacks = list()
        #node = self.nodes_list[0]
        path = 'gen/%s.py'%(node.name)

        file = open(path, "w")
        file.write(self.head())
        file.write(self.generate_imports(node))
        if node.type == "State":
            file.write(self.flexbe_imports())    
        file.write(self.information())
        if node.type == "State":
            file.write(self.flexbe_class(node.name,node.outcomes, node.params, node.description))    
        else:
            file.write(self.init_class(node.name))
        
        file.write(self.params(node.params))
        file.write(self.generate_ports(node))
        file.write(self.init_cb(node.name))
        
        if node.type == "State":
            file.write(self.flexbe_cb(node.name))

        for cb in self.callbacks:
            file.write(cb)

        if node.type != "State":
            file.write(self.node_main(node.name))

        file.close()

        #os.chmod(path, 0777)   #CHECK THIS

    def generate_nodes(self):
        for node in self.nodes_list:
            self.generate_node(node)



'''
STUFF TO REMEMBER

unsubscribe: sub1.unregister()




'''

