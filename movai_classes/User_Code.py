#!/usr/bin/env python
"""User Code Module
"""
from GD import GD_User as gd
from GD_Var import Var as Var_Class
import importlib   
import sys    
        
class UserCode(object):

    """This is the user class
    """

    def __init__(self, _node_name, _port_name, _libraries):

        Var = Var_Class(_node_name, _port_name)
        self.globals = {"gd":gd, "Var":Var}

        for lib in _libraries:
            mod = importlib.import_module(lib['module'])
            if lib['class'] == '':
                self.globals[lib['name']] = mod
            else:
                self.globals[lib['name']] = eval('mod.'+lib['class'])

        if sys.version_info >= (3, 0):  # Our node always run on python 3 so this will not have the if
            self.globals["__builtins__"] = eval('{"min":min, "max":max, "str":str, "print":print}')   #just because python 2.7 misses the parse of print: print -.-'
        else:
            self.globals["__builtins__"] = {"min":min, "max":max, "str":str}


    def execute(self, __code, msg=None):
        
        Vars = {'msg':msg}  
        
        exec(__code, self.globals, Vars)  # Without service stuff

        #Services need this because we cannot return inside exec
        #try:
        #    exec(__code, self.globals, Vars)
        #    Result = Vars.get('reply')
        #except Exception as E:
        #    Result = E
        #return Result
