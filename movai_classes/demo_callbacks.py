#!/usr/bin/env python
from GD_Callback import Callback
from protocols.Redis import Redis

Redis('bla','bla')

yo = Callback("init_node1;dev")
yo.Code = "print('INIT NODE 1')"
yo.Message = ""
yo.Version = '0.0.0'
yo.save()

yo = Callback("init_node2;dev")
yo.addPy3Lib('from geometry_msgs.msg import Twist as Twist')
yo.addPy3Lib('from tb_agv_srvs.srv import SetEffectLEDs as SetEffectLEDs')
yo.addoPortDepend('cmd_vel','ROS1/Publisher','geometry_msgs/Twist')
yo.Code = "print('INIT NODE 2')\ngd.oport['lights'].send('bo500')"
yo.Message = ""
yo.Version = '0.0.0'
yo.save()

yo = Callback("timer_rotate_cb;dev")
yo.addPy3Lib('from geometry_msgs.msg import Twist as Twist')
yo.addoPortDepend('cmd_vel','ROS1/Publisher','geometry_msgs/Twist')
yo.Code = "print('Rotating')\nmsg = Twist()\nmsg.angular.z = 0.2\ngd.oport['move_wheels'].send(msg)"
yo.Message = ""
yo.Version = '0.0.0'
yo.save()

yo = Callback("timer_finish_cb;dev")
yo.addPy3Lib('from geometry_msgs.msg import Twist as Twist')
yo.addPy3Lib('from tb_agv_srvs.srv import SetEffectLEDs as SetEffectLEDs')
yo.addoPortDepend('cmd_vel','ROS1/Publisher','geometry_msgs/Twist')
yo.Code = "print('Stopping rotation')\nmsg = Twist()\ngd.oport['move_wheels'].send(msg)\ngd.oport['lights'].send('fg0005')\ngd.oport['done'].send()"
yo.Message = ""
yo.Version = '0.0.0'
yo.save()

