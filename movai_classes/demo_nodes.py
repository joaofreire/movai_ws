from GD_Luis import Node
from protocols.Redis import Redis

Redis('bla','bla')

#NODE 1

node = Node('joystick_safe_drive')
node.delete()
node.Version
node.Nodelet = ''
node.Path = '/path/to/file'
node.Persistent = True
node.Description = 'This node lets the user control the AGV throught the joystick with a safe mechanism, stopping before hitting obstacles'

# One Init, 2 Ros Subscriber and 1 Transition
node.addiPort('init','Init','init','init_node1;dev',False,'',False)
node.addiPort('laser','ROS1/Subscriber','sensor_msgs/LaserScan','laser_front_cb;dev',False,'/scan_front',False)
node.addiPort('joystick','ROS1/Subscriber','sensor_msgs/Joy','joystick_cb;dev',False,'/joy',False)
node.addiPort('transit','MovAI/Transition','','',False,'',False) #the iport transition is just for UI maybe

# One Ros Publisher and one transition
node.addoPort('move_wheels','ROS1/Publisher','geometry_msgs/Twist','/wheels_controller/cmd_vel',False)
node.addoPort('stop','MovAI/Transition','nada','nada',False)

#node.addoPort('call_srv','ROS1/Service','std_srvs/SetBool','/service',False)
#node.addoPort('oport3','ROS','std_msgs/Bool',False)
#node.addInstCommandLine('cmdline','cmdline','cmdline')
#node.addInstEnvVar('envvar','envvar','envvar')
#node.addInstParameter('move',True,'this is a param')
#node.addInstParameter('timeout',10,'another param')
#node.addInstNoFlow('noflow','noflow','noflow')

node.save()
#node.updateVersion('1.1')


#NODE 2
node = Node('turn_around')
node.delete()
node.Version
node.Nodelet = ''
node.Path = '/path/to/file'
node.Persistent = True
node.Description = 'This node turns the agv 180 degrees while blinking orange'

# One Init, 1 Ros Timer and 1 Transition
node.addiPort('init','Init','init','init_node2;dev',False,'',False)
node.addiPort('timer','ROS1/Timer','7.85','timer_finish_cb;dev',False,'',False)
node.addiPort('timer2','ROS1/Timer','0.033','timer_rotate_cb;dev',False,'',False)
node.addiPort('transit','MovAI/Transition','','',False,'',False) #the iport transition is just for UI maybe

# One Ros Publisher, one Ros ServiceClient and one transition
node.addoPort('move_wheels','ROS1/Publisher','geometry_msgs/Twist','/wheels_controller/cmd_vel',False)
node.addoPort('lights','ROS1/Service','tb_agv_srvs/SetEffectLEDs','/central_controller/effects',False)
node.addoPort('done','MovAI/Transition','','',False)

#node.addoPort('call_srv','ROS1/Service','std_srvs/SetBool','/service',False)
#node.addoPort('oport3','ROS','std_msgs/Bool',False)
#node.addInstCommandLine('cmdline','cmdline','cmdline')
#node.addInstEnvVar('envvar','envvar','envvar')
#node.addInstParameter('move',True,'this is a param')
#node.addInstParameter('timeout',10,'another param')
#node.addInstNoFlow('noflow','noflow','noflow')

node.save()
