#!/usr/bin/env python
import subprocess
#from GD_Flow import GD_Flow

a = subprocess.call(['python', 'GD_Node.py', 'lidar2'])


#the node monitor will be something like this
#every instance name is unique

#make a list of: From  instance,port  ->  To: instance 


class Flow_Monitor(object):
    def __init__(self, _flow_name):
        self.dependencies = {}
        self.flow = GD_Flow(_flow_name)
        #need to have stuff organized

        #make a list of every possible state transition for easy access
        self.transition_connection = [] #from flow

        #make a list of every dependency of each state, one time lookup
        self.dependencies['prev_node'] = ['lala1', 'lala2']
        self.dependencies['next_node'] = ['lala2', 'lala3']

        print('wow')
        a = [x for x in dependencies['next_node'] if x not in dependencies['prev_node']]
        b = [x for x in dependencies['prev_node'] if x not in dependencies['next_node']]

    def run_node(self, _instance_name):
        pass

    def kill_node(self, _instance_name):
        pass

    def transition(self, _prev_instance_name, _prev_port_name):
        #get the instance we are transitioning to
        next_instance_name = 'next_node'
        #calculate changes in dependencies
        not_needed_nodes = [x for x in dependencies[_prev_instance_name] if x not in dependencies[next_instance_name]]

        new_needed_nodes = [x for x in dependencies[next_instance_name] if x not in dependencies[_prev_instance_name]]  

        #kill the current instance  
        self.kill_node(_prev_instance_name)
        #and the unused dependencies
        for node in not_needed_nodes:
            self.kill_node(node)

        #start the new dependencies
        for node in new_needed_nodes:
            self.run_node(node)
        #and finnaly the new instance
        self.run_node(next_instance_name) 

