#!/usr/bin/env python

from flask import Flask

class Flask(object):

	def __init__(self, _port):
		self.app = Flask(__name__)
		self.port = _port

	def run(self):
		app.run(port=self.port, debug=True)
    	#app.run(host=os.environ['ROS_IP'], port=5001)  to make stuff with ros