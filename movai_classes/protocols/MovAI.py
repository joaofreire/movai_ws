#!/usr/bin/env python
from __future__ import print_function   #just cuz we still support running in python 2.7
import time, sys
from GD_Callback import Callback
import os, subprocess

class Init(object):
    def __init__(self, _node_name, _iport_name, _cb_name):
        cb = Callback(_cb_name, _node_name, _iport_name, False)
        cb.execute('')

#THE TEMPORARY MECHANISM TO PREVENT THE NODE OF EXITING ... LIMOR WILL KILL ME PROBABLY
class Run(object):
    def __init__(self):
        try:
            self.main_loop()
        except KeyboardInterrupt:
            print('\nExiting by user request.\n', file=sys.stderr)
        sys.exit(0)

    def main_loop(self):
        while 1:
            time.sleep(0.1)

class Transition(object): #oport
    def __init__(self, _node_name, _oport_name):
        self.node_name = _node_name
        self.port_name = _oport_name
        #Initialize the transition class of health monitor

    def send(self):

        if self.node_name =='joystick_safe_drive':
            name = 'turn_around'
        else: 
            name = 'joystick_safe_drive'
        print("TRANSITION TO: ", name)
        #subprocess.check_output("python3 ~/movai_ws/movai_classes/GD_Node.py "+name, shell=True)
        p = subprocess.Popen("python3 ~/movai_ws/movai_classes/GD_Node.py "+name, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        os.system("rosnode kill "+ self.node_name)

        #Flow_Monitor.transition(self.node_name, self.port_name) 

