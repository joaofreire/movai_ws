#!/usr/bin/env python

import importlib
import rospy
from GD_Callback import Callback
import threading
from GD import GD_User

#Specific imports should be made in the class??
from dynamic_reconfigure.client import Client
import actionlib

class ROS1(object):
    def __init__(self, _node_name):
        #rospy.init_node(node_name, argv=sys.argv, anonymous=False, log_level=None, disable_rostime=False, disable_rosout=True, disable_signals=False, xmlrpc_port=0, tcpros_port=0)
        threading.Thread(target=lambda: rospy.init_node(_node_name, disable_signals=True, disable_rosout=True)).start()
    def run(self):
        rospy.spin() 


##############################            IPORTS                   ########################################################


class ROS1_Subscriber(object):

    def __init__(self, _node_name, _iport):
        self.node_name = _node_name 
        self.port_name = _iport['Name']
        self.topic = _iport['Remap']

        module, self.msg_name = _iport['Message'].split('/')
        globals()['msg_mod'] = importlib.import_module(module +'.msg._'+ self.msg_name)

        self.cb = Callback(_iport['Callback'], self.node_name, self.port_name, True)
        self.sub = rospy.Subscriber(self.topic, eval('msg_mod'+'.'+self.msg_name), self.callback)
        
    def callback(self, msg):
        self.cb.execute(msg)

    def unregister(self):
        self.sub.unregister()
        self.sub = None

    def register(self):
        if self.sub is None:
            self.sub = rospy.Subscriber(self.topic, eval('msg_mod'+'.'+self.msg_name), self.callback)


####################################################


class ROS1_ServiceServer(object):
    def __init__(self, _node_name, _iport):
        self.node_name = _node_name
        self.port_name = _iport['Name']
        self.topic = _iport['Remap']

        module, self.srv_name = _iport['Message'].split('/')
        globals()['srv_mod'] = importlib.import_module(module +'.srv._'+ self.srv_name)

        #initialize the oport needed for the reply
        self.reply = GD_User.oport['reply@' + self.port_name]

        self.cb = Callback(_iport['Callback'], self.node_name, self.port_name, True)
        self.sub = rospy.Service(self.topic, eval('srv_mod'+'.'+self.srv_name), self.callback)
        
    def callback(self, msg):
        self.cb.execute(msg)
        # need some kind of verification??
        return self.reply.msg


class ROS1_ServiceServerReply(object):
    #stupid oport class just for the user to reply in a service server with oport['name'].send(msg)
    def __init__(self):
        self.msg = None

    def send(self, msg):
        self.msg = msg


####################################################

class ROS1_Timer(object):
    def __init__(self,_node_name, _iport):
        self.node_name = _node_name
        self.port_name = _iport['Name']
        self.duration = float(_iport['Message'])

        self.cb = Callback(_iport['Callback'], self.node_name, self.port_name, True)
        self.timer = rospy.Timer(rospy.Duration(self.duration), self.callback)
        
    def callback(self, msg):
        self.cb.execute(msg)

    def unregister(self):
        self.timer.shutdown()
        
    def register(self):
        if self.timer._shutdown == True:
            self.timer = rospy.Timer(rospy.Duration(self.duration), self.callback)


####################################################


class ROS1_ActionServer(object):   #MAYBE WE WILL NOT PROVIDE THIS
    def __init__(self, _node_name, _iport):
        self.node_name = _node_name
        self.port_name = _iport['Name']
        self.topic = _iport['Remap']

        module, msg_name = _iport['Message'].split('/')
        vars()['msg_mod'] = importlib.import_module(module + '.msg._' + msg_name)

        self.cb = Callback(_iport['Callback'], self.node_name, self.port_name, True)
        self._as = actionlib.SimpleActionServer(self.topic, eval('msg_mod' + '.' + msg_name), execute_cb=self.callback, auto_start = False)
        self._as.start()

    def callback(self, msg):
        self.cb.execute(msg)

    def send_result(self, msg): #sends the result....
        self._as.set_succeeded(msg)

    def send_feedback(self, msg): #sends the feedback
        self._as.publish_feedback(msg)


####################################################


class ROS1_TF(object): #iport that has a callback for a TF change
    def __init__(self, _topic1, _topic2, _message, _cb_name): 
        #Create a timer??
        self.cb = Callback(_cb_name)
 
    def callback(self, msg):
        (trans,rot) = listener.lookupTransform(_topic1, _topic2, rospy.Time(0))
        self.cb.execute(msg)



##############################            OPORTS                   ########################################################



class ROS1_Publisher(object):
    def __init__(self,_topic, _message):

        module, msg_name = _message.split('/')
        vars()['msg_mod'] = importlib.import_module(module +'.msg._'+ msg_name)
        self.pub = rospy.Publisher(_topic, eval('msg_mod'+'.'+ msg_name), queue_size=1)
        
    def send(self, msg):
        self.pub.publish(msg)

    def get_message(self):
        return 'Mensagem...'


####################################################

class ROS1_ServiceClient(object):
    def __init__(self, _topic, _service): 

        module, srv_name = _service.split('/')
        vars()['srv_mod'] = importlib.import_module(module +'.srv._'+ srv_name)
        self.pub = rospy.ServiceProxy(_topic, eval('srv_mod'+'.'+srv_name))
        
    def send(self, srv):
        response = self.pub(srv)
        return response

####################################################


class ROS1_ActionClient(object):
    def __init__(self, _topic, _message):

        module, msg_name = _message.split('/')
        vars()['msg_mod'] = importlib.import_module(module + '.msg._' + msg_name)
        self._ac = actionlib.SimpleActionClient(_topic, eval('msg_mod' + '.' + msg_name))

    def send(self, msg):
        self._ac.send_goal(msg)


####################################################

class ROS1_DynReconfigure(object): #client, oport
    def __init__(self, _topic):
        self.dyn = Client(_topic, timeout=None, config_callback=None)

    def send(self, msg): #({str: value}) - dictionary of key value pairs for the parameters that are changing
        self.dyn.update_configuration(msg)
