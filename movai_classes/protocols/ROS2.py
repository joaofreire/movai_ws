#!/usr/bin/env python

#imports???
import rclpy
from rclpy.node import Node
from lifecycle_msgs.msg import Transition
from std_msgs.msg import String


class ROS2(object):
    node = ''


class ROS2_INIT(object):
    def __init__(self, _node_name):
        rclpy.init(args=None)
        ROS2.node = rclpy.create_node(_node_name)


class ROS2_Subscriber(object):

    def __init__(self, _node_name, _iport):

        self.sub = ROS2.node.create_subscription(String, 'topico', self.callback)
        self.sub # prevent unused variable warning

    def callback(self, msg):
        print('Was ... This... a message in ROS2?? Not bad')
        print(msg)
        #self.cb.execute(msg)


class ROS2_Publisher(object):
    def __init__(self, _topic='topico', _message='String'):

        #module, msg_name = _message.split('/')
        #vars()['msg_mod'] = importlib.import_module(module +'.msg._'+ msg_name)
        #self.pub = rospy.Publisher(_topic, eval('msg_mod'+'.'+ msg_name), queue_size=1)
        self.pub = ROS2.node.create_publisher(eval(_message), _topic)
        
    def send(self, msg):
        self.pub.publish(msg)