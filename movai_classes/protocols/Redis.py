#!/usr/bin/env python

import redis, time
from threading import Thread
import pickle

class GD_Redis(object):

    db_global= None
    db_slave = None
    db_local = None

class Redis(object):
    __instance = None

    
    def __init__(self, _nodename=None, robot_name=None,_hostname='10.10.0.60', _port='6379', _passwd=None): #add robot name
        # only import the redis and initiate it once per node run
        # (ie: can't have 2 ports on one node)  - I think we can, and maybe we need... still needs testing
        #time_start = time.time()
        pool = redis.ConnectionPool(host=_hostname, port=_port, db=0)

        
        if GD_Redis.db_global == None:
            GD_Redis.db_global = redis.StrictRedis(connection_pool=pool, decode_responses= True)
            GD_Redis.db_slave = redis.StrictRedis(host='localhost', port=6380,db=0, decode_responses= True)
            GD_Redis.db_local = redis.StrictRedis(host='localhost', port=4000,db=0, decode_responses= True)
            GD_Redis.db_global.client_setname(robot_name+'_'+_nodename)
            GD_Redis.db_slave.client_setname(_nodename)
            GD_Redis.db_local.client_setname(_nodename)

            #self.searchDb('bla')
        #    print (GD_Redis.db_global.client_getname())
        #    print ('Took me ',time.time() - time_start,' to connect')


        self.robot_id = "AGV1"
        self.slave_pubsub = GD_Redis.db_slave.pubsub()
        self.local_pubsub = GD_Redis.db_local.pubsub()

 
        self.thread = None


    def searchDb(self, key):
        
        time_start = time.time()
        for key in GD_Redis.db_slave.scan_iter(match='*lidar2*', count=1000):            
            pass
        print (time.time() - time_start)

        time_start = time.time()
        
        for key in GD_Redis.db_slave.keys('*lidar2*'):
            pass
        print (time.time() - time_start)
        
# #       for name in GD_Redis.db_slave.scan_iter("*lidar2*iport"):
          # print(name.decode('utf-8')) #for a single child key


    def db_setVar(self, name, value):
        GD_Redis.db_global.set(name, pickle.dumps(value))

    def db_getVar(self, name):
        var = pickle.loads(GD_Redis.db_local.get(name))
        
        if var:
            return var
        else:
            return None

    def db_getCb(self, name):
        var = GD_Redis.db_slave.get(name)
        if var:
            return var
        else:
            return None

    def sub_VarFleet(self, key,function):

        self.slave_pubsub.psubscribe(**{'__keyspace@0__:%s'%(key): function})
        self.__start()
        
    def sub_VarRobot(self, key,function):
        key = "%s/%s"%(self.robot_id, key)
        self.slave_pubsub.psubscribe(**{'__keyspace@0__:%s'%(key): function})
        self.__start()

    def __start(self):
        if self.thread is None:       
            self.thread = RedisThread(self)
            self.thread.daemon = True
            self.thread.start()

class RedisThread(Thread):

    def __init__(self, GD):
        self.GD = GD
        super(RedisThread, self).__init__()

    def run(self):
        while True:  
            message = self.GD.slave_pubsub.get_message()
            if message:
                pass
            else:
                time.sleep(0.01)
