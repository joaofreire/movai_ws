#!/usr/bin/env python

from threading import Thread
from GD_Callback import Callback
from protocols.Redis import GD_Redis, Redis
from GD_Var import Var
from GD_Message import Message
import time
import importlib

class Redis_Subscriber(object):

    def __init__(self, _node_name, _iport, _thread=None):
        self.node_name = _node_name 
        self.port_name = _iport['Name']
        self.var_name = _iport['Remap']

        self.slave_pubsub = GD_Redis.db_slave.pubsub()

        self.thread = _thread
        self.cb = Callback(_iport['Callback'], self.node_name, self.port_name, True)
        self.value = Var(self.node_name, self.port_name)    
        self.slave_pubsub.psubscribe(**{'__keyspace@0__:%s'%self.var_name: self.callback})
        
        #Make something to one line this process
        msg = Message('msg')
        msg.get('redis_data')
        exec(msg.compiled)
        self.m = eval('redis_data()')
        self.__start()

    def __start(self):
        if self.thread is None:       
            self.thread = RedisThread(self)
            self.thread.daemon = True
            self.thread.start()

    def callback(self, msg):    #NEED TO GET THE SCOPE AND THE VAR NAME
        self.m.type = msg['data']#.decode('utf-8')
        self.m.data = self.value.get('FLEET', 'var_fleet')
        self.cb.execute(self.m)


class RedisThread(Thread):

    def __init__(self, GD):
        self.GD = GD           
        super(RedisThread, self).__init__()

    def run(self):
        while True:  
            message = self.GD.slave_pubsub.get_message()
            if message:
                pass
            else:
                time.sleep(0.01)
