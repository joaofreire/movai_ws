import socketio
import threading
from flask import Flask
from flask_socketio import SocketIO, emit, send
from GD_Callback import Callback

class SIO(object):
    app = ''
    sio = ''

class Socketio(object):
    def __init__(self, _hostname, _port):
        self.hostname = _hostname
        self.port = _port
        SIO.app = Flask(__name__)
        SIO.app.config['SECRET_KEY'] = 'secret!'
        SIO.sio = SocketIO(SIO.app)

    def run(self):
        #SIO.sio.run(SIO.app, host=self.hostname, port=self.port)
        threading.Thread(target=lambda: SIO.sio.run(SIO.app, host=self.hostname, port=self.port)).start()


class SIO_Sub(object):
    def __init__(self, _node_name, _iport, _sio=''):
        self.node_name = _node_name 
        self.port_name = _iport['Name']
        self.topic = _iport['Remap']
        _sio.on_event('connect', self.connect, namespace="/") #test only
        _sio.on_event('disconnect', self.disconnect, namespace="/") #test only

        self.cb = Callback(_iport['Callback'], self.node_name, self.port_name, True)
        _sio.on_event(self.topic, self.callback, namespace="/")

    def callback(self, msg):
        self.cb.execute(msg)

    def connect(self):
        print('Cient Connected')

    def disconnect(self):
        print('Client Disconnected')


class SIO_Pub(object):
    def __init__(self, _topic, _event):
        self.event = _event
        self.topic = _topic
        self.sio = socketio.Client()
        self.sio.on('connect', self.on_connect, namespace="/")  # for test only, client dont have callbacks
        self.sio.connect(self.topic) # http://localhost:5000

    def on_connect(self): #Test only
        print('Connected to Server')

    def send(self, msg):
        self.sio.emit(self.event, msg)

    def connect(self):
        self.sio.connect(self.topic)

    def disconnect(self):
        self.sio.disconnect()