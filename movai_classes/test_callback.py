#!/usr/bin/env python
from GD_Callback import Callback
from protocols.Redis import Redis

Redis('bla','bla')

yo = Callback("test_callback_up_2;dev")
yo.addPy3Lib('import numpy')
yo.addPy3Lib('from std_msgs.msg import Bool as Bool')
yo.addoPortDepend('laser','ROS','scan')
#yo.Code = "print ('Brand new Callback v5')\nVar.set('CALLBACK','var_cb',1)\nVar.set('NODE','var_node',2)\nVar.set('ROBOT','var_robot',3)\nVar.set('FLEET','var_fleet',5)"
#yo.Code = "print ('Brand new Callback v6')\nVar.delete('CALLBACK','var_cb')\nprint(Var.get('CALLBACK','var_cb'))\nprint(Var.get('NODE','var_node'))\nprint(Var.get('ROBOT','var_robot'))\nprint(Var.get('FLEET','var_fleet'))"
#yo.Code = "gd.oport['oport2'].send('socketes com fartura')"
yo.Code = "print('Just got a ROS1 msg')\nprint(msg)"
yo.Message = "std_msgs/Bool"
yo.Version = '0.0.0'
yo.save()

'''
yo = Callback("test_service_cb;dev ")
yo.addPy3Lib('from std_srvs.srv import SetBool as SetBool')
yo.addoPortDepend('laser','ROS','scan')
yo.Code = "print ('Service Callback')\ngd.oport['reply@iport2'].send((True, 'nhecs'))"
yo.Message = "std_srvs/SetBool"
yo.Version = '0.0.0'
yo.save()
'''
'''
yo = Callback("test_callback_init;dev ")
yo.Code = "print ('INIT')\nVar.get('yey','')"
yo.Message = ""
yo.Version = '0.0.0'
yo.save()
'''
'''
yo = Callback("test_callback_redis;dev")
yo.Code = "print ('REDIS SUB GETTING STUFF:')\nprint(msg)"
yo.Message = ""
yo.Version = '0.0.0'
yo.save()
'''
'''
yo = Callback('a', 'a', "test_callback_redis_event;dev")
yo.Code = "print ('REDIS EVENT GETTING STUFF:')\nprint(msg)"
yo.Message = ""
yo.Version = '0.0.0'
yo.save()

yo = Callback("test_callback_socketio;dev")
yo.Code = "print('GOT SOMETHING WITH SOCKET.IO')\nprint(msg)"
yo.Message = ""
yo.Version = '0.0.0'
yo.save()
'''

yo = Callback("test_callback_redis;dev")
yo.Code = "print ('REDIS SUB GETTING STUFF:')\nprint(msg)"
yo.Message = ""
yo.Version = '0.0.0'
yo.save()


yo = Callback("test_callback_redis_lock;dev")
yo.Code = "Var.set('ROBOT','var', 5)"
yo.Message = ""
yo.Version = '0.0.0'
yo.save()

yo = Callback("test_callback_redis_no_lock;dev")
yo.Code = "print(Var.get('ROBOT', 'var', True, 10))"
yo.Message = ""
yo.Version = '0.0.0'
yo.save()

