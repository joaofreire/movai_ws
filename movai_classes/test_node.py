from GD_Luis import Node
from protocols.Redis import Redis

Redis('bla','bla')

node = Node('redis_tests')
node.delete()
node.Version
node.Nodelet = 'lidar_nodelet'
node.Path = '/path/to/file'
node.Persistent = True
node.Description = 'this node server for nothing at all'
node.addiPort('init','Init','init','test_callback_init;dev',False,'',False)
node.addiPort('iport1','ROS1/Subscriber','std_msgs/Bool','test_callback_redis_lock;dev',False,'/sensor_up',False)

node.addiPort('iport2','ROS1/Subscriber','std_msgs/Bool','test_callback_redis_no_lock;dev',False,'/sensor_up_up',False)
node.addiPort('iport3','Redis/Subscriber','','test_callback_redis;dev',False,'Var:var_fleet',False)

#node.addiPort('iport2','ROS1/Service','std_srvs/SetBool','test_service_cb;dev',False,'/service',False)
#node.addiPort('iport3','Redis/Subscriber','','test_callback_redis;dev',False,'Var:var_fleet',False)
#node.addiPort('iport4', 'FlaskSocketIO/Subscriber', 'localhost:5000', 'test_callback_socketio;dev', False, 'lel', False)

#node.addiPort('iport4','Redis/Subscriber','','test_callback_redis_event;dev',False,'*',False)


#node.addoPort('oport1','ROS1/Publisher','std_msgs/Bool','/publisher',False)

#node.addoPort('oport2','SocketIO/Publisher','lel','http://localhost:5000',False)
#node.addoPort('call_srv','ROS1/Service','std_srvs/SetBool','/service',False)
#node.addoPort('oport3','ROS','std_msgs/Bool',False)
#node.addInstCommandLine('cmdline','cmdline','cmdline')
#node.addInstEnvVar('envvar','envvar','envvar')
#node.addInstParameter('move',True,'this is a param')
#node.addInstParameter('timeout',10,'another param')
#node.addInstNoFlow('noflow','noflow','noflow')
#node.addInstNoFlow('noflow3','noflow2','noflow1')
node.save()
#print '\n\n\n'
#print len(node.iPorts)
#node.updateVersion('1.1')
#node.save()
#node.updateVersion('1.1.2.3.4')
#node.save()
#node.updateVersion('2.1')
#node.save()
