#!/usr/bin/env bash


#Launch Roscore
source /opt/ros/kinetic/setup.sh
source ~/movai_ws/devel/setup.bash
export ROS_PACKAGE_PATH=/home/gdrone/movai_ws/src:/opt/ros/kinetic/share:$ROS_PACKAGE_PATH
#export ROS_IP=192.168.1.10
#export ROS_MASTER_URI=http://192.168.1.10:11311
roscore
