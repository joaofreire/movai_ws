#!/usr/bin/env python

import os
import sys
import rospy
import inspect

import genpy.generator
import genpy.genpy_main
import genpy.generate_initpy

from genaction import gen_action

class MsgNotFound(Exception):

    def __init__(self, message, base_type=None, package=None, search_path=None):
        super(MsgNotFound, self).__init__(message)
        self.base_type = base_type
        self.package = package
        self.search_path = search_path

class MsgNode(object):
    def __init__(self, name):
        #path_in_catkin = '~/catkin_ws/devel/lib/python2.7/dist-packages/'
        in_dir = '/home/paulino/catkin_ws/src/movai_tiago/movai_msgs/'
        out_dir = '/home/paulino/catkin_ws/src/movai_tiago/movai_msgs/gen/movai_msgs/'

        #Create all the ActionLib Messages
        actions = list()
        files = os.listdir(in_dir + 'action/')
        for elem in files:
            gen_action(in_dir + 'action/' + elem)  #Generate Actionlib Aux Messages

        #READ MSGS FROM FILES (for now)
        msgs = list()
        files = os.listdir(in_dir + 'msg/')
        for elem in files:
            msgs.append(in_dir + 'msg/' + elem)

        #READ SRVS FROM FILES (for now)
        srvs = list()
        files = os.listdir(in_dir + 'srv/')
        for elem in files:
            srvs.append(in_dir + 'srv/' + elem)

        #CREATE OUT DIR (some folder with python path)
        try:
            os.makedirs(out_dir)
        except:
            print ('Folder exists already')
        file = open(out_dir + "__init__.py", "w") 
        file.close() 


        search_path ={
            "std_msgs": ["/opt/ros/kinetic/share/std_msgs/msg"],
            "geometry_msgs": ["/opt/ros/kinetic/share/geometry_msgs/msg"],
            "actionlib_msgs": ["/opt/ros/kinetic/share/actionlib_msgs/msg"],
            "movai_msgs": ["/home/paulino/catkin_ws/src/movai_tiago/movai_msgs/msg"]
        }
 
        #Generate Messages , including actionlib messages
        msg_gen = genpy.generator.MsgGenerator()
        msg_gen.generate_messages('movai_msgs', msgs, out_dir + 'msg', search_path)
        genpy.generate_initpy.write_modules(out_dir + 'msg')

        #Generate Services
        srv_gen = genpy.generator.SrvGenerator()
        srv_gen.generate_messages('movai_msgs', srvs, out_dir + 'srv', search_path)
        genpy.generate_initpy.write_modules(out_dir + 'srv')


        #print inspect.getmembers(actionlib_msgs, predicate=inspect.ismethod) 

if __name__ == '__main__':
    rospy.init_node('msg_gen')
    node = MsgNode(rospy.get_name())
    rospy.spin()