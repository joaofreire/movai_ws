#!/usr/bin/env python
#from movai_db.GD_Singleton_Mongo import *
from Interface_Redis import *

class GD(object):
  sub = {}
  pub = {}
  ac = {}
  cb = {}  
  variables = {}
  srv_client = {}
  transition = {}
  change_state = None
  timer = {}
  db_sub = {}
  GD_db = Interface_Redis()
  __var_node = {}
  _nodename = ''
  
  
  __instance = None
  def __new__(cls, node_name):
        _nodename = node_name

        if GD.__instance is None:
            GD.__instance = object.__new__(cls)
        return GD.__instance
  def __init__(self, node_name):
    #self.policy.run()
    #policy.setGd(gdClass)
    self.nodename = node_name
    pass


    
  def getSub(self):
    return self.sub;

  
  def add_db_sub(self, name, type):
    
    if type == 'robot':
      self.GD_db.sub_VarRobot(name, self.cb['handle_db_%s'%name.replace('.','_')])
    elif type == 'fleet':
      self.GD_db.sub_VarFleet(name, self.cb['handle_db_%s'%name.replace('.','_')])

  def getHash(self, name):
    return self.GD_db.getHash(name)
    
  def getProject(self):

    return self.GD_db.getProject()

  def getCb(self, name):
    return self.GD_db.getCb(name)

  def getVar(self, name):
    return self.GD_db.getVar(name)

  """
  GET variables persistent across all Nodes in a given robot.
  """
  def getVarRobot(self, name):
    return self.GD_db.getVarRobot(name)
    
    
  """
  SET variables persistent across all Nodes in a given robot.
  """
  def setVarRobot(self, name, value):
    self.GD_db.setVarRobot(name, value) #Through DB

  """
  GET variables persistent across all callbacks of all robots.
  """
  def getVarFleet(self, name):
    return self.GD_db.getVarFleet(name)
            
    
  """
  SET variables persistent across all callbacks of all robots.
  """
  def setVarFleet(self, name, value):
    self.GD_db.setVarFleet(name, value) #Through DB
  
  """
  GET variables persistent across consecutive calls to given callback.
  """
  def getVarLocal(self, name):
    return self.__var_local[name] #Through Global variables
    
  """
  SET variables persistent across consecutive calls to given callback.
  """
  def setVarLocal(self, name, value):
    self.__var_local[name] = value

  """
  GET variables persistent across all callbacks in a given Node
  """
  def getVarNode(self, name):
    return self.GD_db.getVarNode(self.nodename, name) #Through Global variables
    
  """
  SET variables persistent across all callbacks in a given Node
  """
  def setVarNode(self, name, value):
    self.GD_db.setVarNode(self.nodename, name, value)
    
    
  def call(self, var, data=None):
    if var in self.pub.keys():
        self.pub[var].publish(data)
    if var in self.srv_client.keys():
      self.srv_client[var](data)
    if var in self.action_client.keys():
      self.action_client[var].send_goal(data, active_cb=self.cb['%s_active'%var], done_cb=self.cb['%s_done'%var], feedback_cb=self.cb['%s_feedback'%var])
    if var in self.transition.keys():
      self.change_state = var
    

