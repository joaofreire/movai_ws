
import redis
from threading import Thread
import time


class GD_Redis(object):
    __instance = None
    
    def __new__(cls):
        if GD_Redis.__instance is None:
            GD_Redis.__instance = object.__new__(cls)
            #GD_Singleton_Mongo.__initMongo("AGV1")
        return GD_Redis.__instance

    def __init__(self):
        #self.db = redis.StrictRedis(host='localhost', port=6379, db=0)
        self.db = redis.StrictRedis(host='127.0.0.1', port=6379, db=0)
        self.robot_id = "AGV1" #This needs to be write in other place
        self.pubsub = self.db.pubsub()
        self.__thread = None

        
    """
        GET variables persistent across all Nodes in a given robot.
    """
    def getVarRobot(self, name):
        name = "%s/%s"%(self.robot_id, name)
        return self.__db_getVar(name) #Through DB
    
    
    """
        SET variables persistent across all Nodes in a given robot.
    """
    def setVarRobot(self, name, value):
        name = "%s/%s"%(self.robot_id, name)
        return self.__db_setVar(name, value) #Through DB

    """
        GET variables persistent across all callbacks of all robots.
    """
    def getVarFleet(self, name):
        name = "fleet/%s"%(name)
        return self.__db_getVar(name) #Through DB
    
    """
        SET variables persistent across all callbacks of all robots.
    """
    def setVarFleet(self, name, value):
        name = "fleet/%s"%(name)
        return self.__db_setVar(name, value) #Through DB
    
    def getVar(self, name):
      return self.__db_getVar(name)
      
    def setVar(self, name, value):
      self.__db_setVar(name, value)
      
    def getHash(self, name):
      return self.__db_getHash(name)
      
    def getProject(self):
        keys = self.db.keys('*')
        project = {}
        project["nodes"] = []
        node_arr = []
        for key in keys:
            if "project/" in key:
                node = {}
                node["inports"] = []
                node["outports"] = []
                node["name"] =  self.__getNodeName(key)
                node["nodeType"] = self.__db_getVar("project/%s/nodeType" %node["name"])
                node_in_arr = self.__getNodeArr(node_arr, node["name"])
                if  node_in_arr is None:
                    inport = self.__getInport(key)
                    if inport is not None:
                        i = {"name": inport, "key": key}
                        node["inports"].append(i)
                    
                    outport = self.__getOutport(key)
                    if outport is not None:
                        i = {"name": outport, "key": key}
                        node["outports"].append(i)
                    node_arr.append(node)
                else:
                    node = node_in_arr
                    inport = self.__getInport(key)
                    if inport is not None:
                        i = {"name": inport, "key": key}
                        node["inports"].append(i)
                    
                    outport = self.__getOutport(key)
                    if outport is not None:
                        i = {"name": outport, "key": key}
                        node["outports"].append(i)
        
        project["nodes"] = node_arr
        return project
                    
                
      
    
    def __getNodeName(self,key):
        arr = key.split("/")
        return arr[1]
    
    def __getInport(self,key):
        if "inports" in key:
            arr = key.split("/")
            return arr[3]
        else:
            return None
    
    def __getOutport(self,key):
        if "outports" in key:
            arr = key.split("/")
            return arr[3]
        else:
            return None
            
    def __getNodeArr(self, arr, nodeName):
        for node in arr:
            if node["name"] == nodeName:
                return node
        
        return None
      
    #Redis DB
    def __db_getHash(self, name):
        return self.db.hgetall(name)
        
    def __db_setVar(self, name, value):
        self.db.set(name, value)

    def __db_getVar(self, name):
        var = self.db.get(name)
        
        if var:
            return var
        else:
            return None
            
    
    #Redis PUB/SUB
    
    def sub_VarRobot(self, key, function):
        key = "%s/%s"%(self.robot_id, key)
        self.pubsub.psubscribe(**{'__keyspace@0__:%s'%(key): function})
        self.__start()
    
    def sub_VarFleet(self, key, function):
        #key = "battery"
        print "Added sub bar fleet"
        key = "fleet/%s"%(key)
        self.pubsub.psubscribe(**{'__keyspace@0__:%s'%(key): function})
        self.__start()
    
    def __start(self):
        if self.__thread is None:        
            self.__thread = RedisThread(self)
            self.__thread.daemon = True
            self.__thread.start()


    
class RedisThread(Thread):

    def __init__(self, GD):
        self.GD = GD
        super(RedisThread, self).__init__()

    def run(self):
        print('Starting message loop')  
        while True:  
            message = self.GD.pubsub.get_message()
            if message:
                print(message)
            else:
                time.sleep(0.01)



