#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from movai_states.GripperGen import GripperGenState
from movai_states.GoToGen import GoToGenState


###############################################
#                                             #
#     THIS CODE WAS AUTO-GENERATED FROM       #
#        MOV.AI SOFTWARE FOR ROBOTICS         #
#     FOR MORE INFORMATION VISIT MOV.AI       #
#                                             #
###############################################


class MovAiTestsSM(Behavior):
	def __init__(self):
		super(MovAiTestsSM, self).__init__()

	def create(self):
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])
		# userdata should be placed here if used
		# _state_machine.userdata.agv_name = self.agvname

		with _state_machine:
			# x:504 y:288
			OperatableStateMachine.add('Grab',
										GripperGenState(type='Grab', timeout=5),
										transitions={'success': 'GoTo B', 'fail': 'failed'},
										autonomy={'GoTo B': Autonomy.Off, 'failed': Autonomy.Off})

			# x:864 y:288
			OperatableStateMachine.add('GoTo B',
										GoToGenState(target_pose=''),
										transitions={'success': 'Grab', 'fail': 'failed'},
										autonomy={'Grab': Autonomy.Off, 'failed': Autonomy.Off})

		return _state_machine