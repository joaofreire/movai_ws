#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from movai_states.GoTo import GoToState
from movai_states.Grab import GrabState


###############################################
#                                             #
#     THIS CODE WAS AUTO-GENERATED FROM       #
#        MOV.AI SOFTWARE FOR ROBOTICS         #
#     FOR MORE INFORMATION VISIT MOV.AI       #
#                                             #
###############################################


class MyFirstProjectSM(Behavior):
	def __init__(self):
		super(MyFirstProjectSM, self).__init__()

	def create(self):
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])
		# userdata should be placed here if used
		# _state_machine.userdata.agv_name = self.agvname

		with _state_machine:
			# x:10 y:20
			OperatableStateMachine.add('Going to A',
										GoToState(log_description='moving...',log_level='0', timeout=40),
										transitions={'success': 'Wait victory', 'failed': 'Go B'},
										autonomy={'success': Autonomy.Off, 'failed': Autonomy.Off})

			# x:504 y:288
			OperatableStateMachine.add('manager123',
										managerState(param3=288, param2=504, param1='manager_param'),
										transitions={'p_out': 'planner', 'p_out': 'batteryMng'},
										autonomy={'planner': Autonomy.Off, 'batteryMng': Autonomy.Off})

		# x:123 y:145
		_sm_container_test = OperatableStateMachine(outcomes=['success', 'failed'])

		with _sm_container_test:
			# x:15 y:25
			OperatableStateMachine.add('Going to B',
										GoToState(log_description='moving...',log_level='0', timeout=40),
										transitions={'success': 'Wait victory', 'failed': 'Go B'},
										autonomy={'success': Autonomy.Off, 'failed': Autonomy.Off})

		return _state_machine