#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from movai_states.test import testState
from movai_states.manager123 import manager123State
from movai_states.planner import plannerState
from movai_states.batteryMng import batteryMngState


###############################################
#                                             #
#     THIS CODE WAS AUTO-GENERATED FROM       #
#        MOV.AI SOFTWARE FOR ROBOTICS         #
#     FOR MORE INFORMATION VISIT MOV.AI       #
#                                             #
###############################################


class MySecondProjectSM(Behavior):
	def __init__(self):
		super(MySecondProjectSM, self).__init__()

	def create(self):
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])
		# userdata should be placed here if used
		# _state_machine.userdata.agv_name = self.agvname

		with _state_machine:
			# x:756 y:72
			OperatableStateMachine.add('test',
										testState(param3=288, param2=504, param1='test_param'),
										transitions={},
										autonomy={})

			# x:504 y:288
			OperatableStateMachine.add('manager123',
										managerState(param3=288, param2=504, param1='manager_param'),
										transitions={'p_out': 'planner', 'p_out': 'batteryMng'},
										autonomy={'planner': Autonomy.Off, 'batteryMng': Autonomy.Off})

			# x:864 y:288
			OperatableStateMachine.add('planner',
										plannerState(param3=288, param2=504, param1='planner_param'),
										transitions={},
										autonomy={})

			# x:900 y:504
			OperatableStateMachine.add('batteryMng',
										batteryMngState(param3=288, param2=504, param1='battery_param'),
										transitions={},
										autonomy={})

		return _state_machine