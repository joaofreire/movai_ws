#!/usr/bin/env python

from std_srvs.srv import *
#from std_srvs.srv import SetBool, SetBoolRequest, SetBoolResponse 
import rospy, inspect

def attach(req):
    if req.data:
        print "[GRIPPER] Attach"
    else:
        print "[GRIPPER] Dettach"
    resp = SetBoolResponse()
    resp.success = True
    return resp

def center(req):
    print "[GRIPPER] Center"
    return True

def gripper():
    rospy.init_node('gripper')
    s = rospy.Service('/central_controller/attach_gripper', SetBool, attach)
    s = rospy.Service('/central_controller/center_gripper', Empty, center)
    print "Gripper Ready"
    print inspect.getsource(center)
    rospy.spin()

if __name__ == "__main__":
    gripper()
