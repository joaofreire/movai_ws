#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, rospy
from my_package import UInt8, String, Bool
from flexbe_core import EventState


###############################################
#                                             #
#     THIS CODE WAS AUTO-GENERATED FROM       #
#        MOV.AI SOFTWARE FOR ROBOTICS         #
#     FOR MORE INFORMATION VISIT MOV.AI       #
#                                             #
###############################################


class GripperState(EventState):
'''
The description here if needed
'''

	def __init__(self):
		super(GripperState, self).__init__(outcomes = ['success', 'failed'])

	def execute(self, userdata):
		exec(self.code_execute)

	def on_enter(self, userdata):
		exec(self.code_enter)

	def on_exit(self, userdata):
		exec(self.code_exit)

