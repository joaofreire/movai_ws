#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from GD import GD
from callbacks import CB
import actionlib 
from std_msgs.msg import Bool 
from tb_manager_msgs.msg import GoToAction, GoToFeedback, GoToGoal, GoToResult 


###############################################
#                                             #
#     THIS CODE WAS AUTO-GENERATED FROM       #
#        MOV.AI SOFTWARE FOR ROBOTICS         #
#     FOR MORE INFORMATION VISIT MOV.AI       #
#                                             #
###############################################


gd = GD('GoToGen')

class GoToGenNode(object):
	def __init__(self, name):

		gd.setVarNode('target_pose', target_pose)
		# Init Imputs
		gd.sub['blocked'] = rospy.Subscriber("GoToGen/blocked", Bool, self.blocked_cb)
		# Init Outputs
		gd.action_client['nav_monitor'] = actionlib.SimpleActionClient("GoToGen/nav_monitor", GoToAction)
		gd.cb = {'nav_monitor_active': self.nav_monitor_active, 'nav_monitor_done': self.nav_monitor_done, 'nav_monitor_feedback': self.nav_monitor_feedback}
		gd.transition['success'] = True
		gd.transition['fail'] = True
		exec(str(gd.getCb('project/GoToGen/cb/init')))


	# Subscriber callback for input 'blocked' 
	def blocked_cb(self, data):
		gd.setVarNode('blocked', data)
		exec(str(gd.getCb('project/GoToGen/cb/blocked')))

	# Client Action callbacks for output 'nav_monitor' 
	def nav_monitor_done(self, status, result):
		exec(str(gd.getCb('project/GoToGen/cb/nav_monitor/done')))
	def nav_monitor_active(self):
		exec(str(gd.getCb('project/GoToGen/cb/nav_monitor/active')))
	def nav_monitor_feedback(self,data):
		exec(str(gd.getCb('project/GoToGen/cb/nav_monitor/feedback')))


if __name__ == '__main__':
	rospy.init_node('GoToGen')
	node = GoToGenNode(rospy.get_name())
	rospy.spin()
