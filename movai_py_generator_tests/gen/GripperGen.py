#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from GD import GD
from callbacks import CB
from std_msgs.msg import Bool 
from std_srvs.srv import Empty 
from std_srvs.srv import SetBool, SetBoolRequest, SetBoolResponse 
from flexbe_core import EventState


###############################################
#                                             #
#     THIS CODE WAS AUTO-GENERATED FROM       #
#        MOV.AI SOFTWARE FOR ROBOTICS         #
#     FOR MORE INFORMATION VISIT MOV.AI       #
#                                             #
###############################################


gd = GD('GripperGen')

class GripperGenState(EventState):
	'''
	Generated Gripper State
	'''

	def __init__(self, move, timeout):
		super(GripperGenState, self).__init__(outcomes = ['success', 'fail'])

		self.cb = CB()

		gd.setVarNode('move', move)
		gd.setVarNode('timeout', timeout)
		# Init Imputs
		gd.sub['up_sensor'] = rospy.Subscriber("GripperGen/up_sensor", Bool, self.up_sensor_cb)
		gd.sub['down_sensor'] = rospy.Subscriber("GripperGen/down_sensor", Bool, self.down_sensor_cb)
		# Init Outputs
		gd.srv_client['attach_gripper'] = rospy.ServiceProxy("GripperGen/attach_gripper", SetBool)
		gd.srv_client['center_gripper'] = rospy.ServiceProxy("GripperGen/center_gripper", Empty)
		gd.transition['success'] = True
		gd.transition['fail'] = True
		exec(str(gd.getCb('project/GripperGen/cb/init')))


	def execute(self, userdata):
		exec(str(gd.getCb('project/GripperGen/cb/execute')))
		if gd.change_state is not None:
			return gd.change_state

	def on_enter(self, userdata):
		exec(str(gd.getCb('project/GripperGen/cb/on_enter')))

	def on_exit(self, userdata):
		exec(str(gd.getCb('project/GripperGen/cb/on_exit')))
		gd.change_state = None


	# Subscriber callback for input 'up_sensor' 
	def up_sensor_cb(self, data):
		gd.setVarNode('up_sensor', data)
		exec(str(gd.getCb('project/GripperGen/cb/up_sensor')))

	# Subscriber callback for input 'down_sensor' 
	def down_sensor_cb(self, data):
		gd.setVarNode('down_sensor', data)
		exec(str(gd.getCb('project/GripperGen/cb/down_sensor')))
