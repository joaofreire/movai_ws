#!/usr/bin/env python
# -*- coding: utf-8 -*-

class CB(object):
	def __init__(self):
		pass

	def GripperGen_code_execute(self):
		s = "print 'execute'"
		return s

	def GripperGen_code_enter(self):
		s = "print 'enter'"
		return s

	def GripperGen_code_exit(self):
		s = "print 'exit'"
		return s

	def input_action_cb(self):
		result = GoToResult()
		feedback = GoToFeedback()
		exec(self.input_action_code)
