#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, rospy
import actionlib 
import tf 
from tf2_msgs.msg import TFMessage 
from std_msgs.msg import String 
from tb_agv_srvs.srv import Attach, AttachRequest, AttachResponse 
from std_srvs.srv import SetBool, SetBoolRequest, SetBoolResponse 
from move_base_msgs.msg import MoveBaseAction, MoveBaseFeedback, MoveBaseGoal, MoveBaseResult 
from tb_manager_msgs.msg import GripperAction, GripperFeedback, GripperGoal, GripperResult 
from flexbe_core import EventState


###############################################
#                                             #
#     THIS CODE WAS AUTO-GENERATED FROM       #
#        MOV.AI SOFTWARE FOR ROBOTICS         #
#     FOR MORE INFORMATION VISIT MOV.AI       #
#                                             #
###############################################


class managerState(EventState):
	'''
	The description here if needed
	'''

	def __init__(self, timeout, blink):
		super(managerState, self).__init__(outcomes = ['success', 'fail'])

		# Init Imputs
		self.sub_battery = rospy.Subscriber("manager/battery", String, self.battery_cb)
		self.srv_say_something = rospy.Service("manager/say_something", SetBool, self.say_something_cb)
		self.action_crazy_action = actionlib.SimpleActionServer("manager/crazy_action", GripperAction, execute_cb = self.crazy_action_cb)
		self.sub_joint = rospy.Subscriber("/tf", TFMessage, self.joint_cb)
		self.timer_tempo = rospy.Timer(rospy.Duration(2), self.tempo_cb)		# Init Outputs
		self.pub_pub_important = rospy.Publisher("manager/pub_important", String, queue_size=10)
		self.close_gripper_srv = rospy.ServiceProxy("manager/close_gripper", Attach)
		self.do_crazy_move_action = actionlib.SimpleActionClient("manager/do_crazy_move", MoveBaseAction)

	def execute(self, userdata):
		exec(self.code_execute)

	def on_enter(self, userdata):
		exec(self.code_enter)

	def on_exit(self, userdata):
		exec(self.code_exit)


	# Subscriber callback for input 'battery' 
	def battery_cb(self, data):
		exec(self.battery_code)

	# Service callback for input 'say_something' 
	def say_something_cb(self, data):
		response = SetBoolResponse()
		exec(self.say_something_code)
		return response

	# Action callback for input 'crazy_action' 
	def crazy_action_cb(self, data):
		result = GripperResult()
		feedback = GripperFeedback()
		exec(self.crazy_action_code)

	# TF callback for input 'joint' 
	def joint_cb(self, data):
		exec(self.joint_code)
		print 'THIS DOES NOT MAKE ANY SENSE'
		print 'PLS CREATE SOME gd.tf_transform()'

	# Timer callback for input 'tempo' 
	def tempo_cb(self, data):
		exec(self.tempo_code)

	# Client Action callbacks for output 'do_crazy_move' 
	def do_crazy_move_done(self, status, result):
		exec(self.do_crazy_move_code)
	def do_crazy_move_active(self):
		exec(self.do_crazy_move_code)
	def do_crazy_move_feedback(self,data):
		exec(self.do_crazy_move_code)
