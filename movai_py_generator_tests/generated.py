#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, rospy
from my_package import UInt8, String, Bool


###############################################
#                                             #
#     THIS CODE WAS AUTO-GENERATED FROM       #
#        MOV.AI SOFTWARE FOR ROBOTICS         #
#     FOR MORE INFORMATION VISIT MOV.AI       #
#                                             #
###############################################


class HelloWorldNode(object):
	def __init__(self):
		self.sub_input_laser = rospy.Subscriber("/base_scan",String,self.input_laser_cb)
		self.pub_output_thing = rospy.Publisher("/blocked",Bool,queue_size=10)
		self.ouput_service_srv = rospy.ServiceProxy("/srv_topic", SetBool)
		self.srv_input_service = rospy.Service("/srv_topic", SetBool, self.input_service_cb)
		self.action_input_action = actionlib.SimpleActionServer("/action_topic", GoTo, execute_cb = self.input_action_cb)
		self.output_action_action = actionlib.SimpleActionClient("/action_topic", GoTo)

	# Generated Subscriber callback for input 'input_laser' 
	def input_laser_cb(self,data):
		exec(self.input_laser_code)

	# Generated Service callback for input 'input_service' 
	def input_service_cb(self,data):
		response = SetBoolResponse()
		exec(self.input_service_code)
		return response

	# Generated Action callback for input 'input_action' 
	def input_action_cb(self,data):
		result = GoToResult()
		feedback = GoToFeedback()
		exec(self.input_action_code)

	# Generated Client Action callbacks for input 'output_action' 
	def output_action_done(self,status,result):
		exec(self.output_action_code)
	def output_action_active(self):
		exec(self.output_action_code)
	def output_action_feedback(self,data):
		exec(self.output_action_code)


if __name__ == '__main__':
	rospy.init_node(HelloWorldNode)
	node = HelloWorldNode(rospy.get_name())
	rospy.spin()
