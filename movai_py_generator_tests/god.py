#!/usr/bin/env python

#script to create generic node
#input msg
##message type
#
##input srv
#srv type
#
##input topic
#topic type
#
#

#args
#[service, input, service_name, serviceType, handle_service_type]
#[service, output, service_name, serviceType]


#publisher
#fakeScan_pub = nh.advertise<sensor_msgs::LaserScan>("/fakeScan", 1);

#subscriber
#laser_sub = nh.subscribe<sensor_msgs::LaserScan>(scanTopic, 1, laserReceived);

#action lib
#agv_ac = actionlib.SimpleActionClient('/%s/order'%agvname, ctt_manager_msgs.msg.AGVOrderAction)

#service
#r = rospy.Service('add_two_ints', AddTwoInts, handle_nome) #output
#add_two_ints = rospy.ServiceProxy('add_two_ints', AddTwoInts)
import sys
import os
from GD import GD

gd = GD()

class Service:
  port_name = ''
  name = ''
  type = ''
  remap = ''

class Topic:
  port_name = ''
  name = ''
  type = ''
  remap = ''
  
class actionLib:
  port_name = ''
  name = ''
  type = ''
  remap = ''
 
class movaiDb:
  port_name = ''
  name = ''
  type = ''
  remap = ''
  
class Timer:
  port_name = ''
  name = ''
  duration = ''

node = None
n_inputs = 0
n_outputs = 0
global out_srvs
global in_srvs
global in_topics
global out_topics
global out_acs
global in_acs
global in_dbs
global timers

def clear():
  global out_srvs
  global in_srvs
  global in_topics
  global out_topics
  global out_acs
  global in_dbs
  global in_acs
  global timers

  n_inputs = 0
  n_outputs = 0
  out_srvs = []
  in_srvs = []
  in_topics = []
  out_topics = []
  out_acs = []
  in_dbs = []
  in_acs = []
  timers = []
  
def make_intro():
  global node
  node.write("#!/usr/bin/env python\n") 
  node.write("import sys\n")
  node.write("import math\n")
  node.write("import rospy\n")
  node.write("import actionlib\n")
  node.write("import geometry_msgs.msg\n")
  node.write("from geometry_msgs.msg import Twist\n")
  node.write("from ctt_agv_msgs.msg import *\n")
  node.write("from ctt_manager_msgs.msg import *\n")
  node.write("from std_msgs.msg import *\n")
  node.write("import ctt_agv_srvs.srv\n")
  node.write("import sensor_msgs.msg\n")
  node.write("from GD import GD\n")
  node.write("from time import sleep\n")
  node.write("import std_srvs.srv\n")
  node.write("\ngd = GD()\n")

def make_db_cb(_node_name, _name, _type):
  node.write("\n#DB %s callbacks of type %s\n"%(_name, _type))
  var = "def handle_db_%s(result):\n"%(_name)
  node.write(var)
  node.write(" data = gd.getVar(result['pattern'].replace('__keyspace@0__:', ''))\n")
  node.write(" command = str(gd.getVar('project/%s/cb/%s'))\n"%(_node_name, _name))
  node.write(" exec(command)\n")
  #node.write(" print result")
  node.write("\n")
  
def make_ac_cb(_node_name,_name, _type):
  print(_name)
  node.write("\n#Actionlib %s callbacks of type %s\n"%(_name, _type))

  var = "def handle_ac_%s_done(self, result):\n"%(name)
  node.write(var)
  node.write("  command = str(gd.getVar('project/%s/cb/%s_done'))\n"%(_node_name, _name))
  node.write("  exec(command)\n")
  node.write("  print result")
  node.write("\n")
  var = "def handle_ac_%s_active():\n"%(_name)
  node.write(var)
  node.write("  command = str(gd.getVar('project/%s/cb/%s_active'))\n"%(_node_name, _name))
  node.write("  exec(command)\n")
  node.write("  print 'active'")
  node.write("\n")  
  var = "def handle_ac_%s_fb(self, msg):\n"%(_name)
  node.write(var)
  node.write("  command = str(gd.getVar('project/%s/cb/%s_fb'))\n"%(_node_name, _name))
  node.write("  exec(command)\n")
  #node.write("  print msg")
  node.write("\n")
  
  
def make_srv_cb(_node_name,_name, _type):
  var = "def handle_srv_%s(data):\n"%(_name)
  node.write("\n#Service %s callback of type %s\n"%(_name, _type))
  node.write(var)
  node.write("  response = %sResponse()\n"%(_type))
  node.write("  command = str(gd.getVar('project/%s/cb/%s'))\n"%(_node_name, _name))
  node.write("  exec(command)\n")
  node.write("  return response")
  #node.write("  print req")
  node.write("\n")

def make_topic_cb(_node_name, _name, _type):
  var = "def handle_topic_%s(data):\n"%(_name)
  node.write("\n#Topic %s callback of type %s\n"%(_name, _type))
  node.write(var)
  #node.write("  print data\n")
  node.write("  command = str(gd.getVar('project/%s/cb/%s'))\n"%(_node_name, _name))
  node.write("  exec(command)\n")
  node.write("\n")

def make_timer_cb(_node_name, _name,true_name, _time):
  var = "def handle_timer_%s(event):\n"%(_name.replace('/','_'))
  node.write("\n#Timer %s \n"%(_name))
  node.write(var)
  node.write("  command = str(gd.getVar('project/%s/cb/%s'))\n"%(_node_name, true_name))
  node.write("  exec(command)\n")
  node.write("\n")

def make_db_client(_name, _type):
  var = " gd.add_db_sub('%s', '%s')\n"%(_name, _type)
  node.write(var)

def make_ac_client(_name, _type, _port_name):
  var = " gd.ac['%s'] = actionlib.SimpleActionClient('/%s', %s)\n"%(_port_name, _name, _type)
  node.write(var)
  
def make_srv(_node_name, _name, _type, _port_name):
  var = " gd.srv['%s'] = rospy.Service(\"%s\", %s, handle_srv_%s)\n"%(_port_name, _name, _type, _port_name)
  node.write(var)

def make_topic(_node_name, _name, _type, _port_name):
  var = " gd.pub['%s'] = rospy.Publisher(\"%s\", %s)\n"%(_port_name,_name,_type)
  node.write(var)
  
def make_timer(_name, _duration, _port_name):
  var = " gd.timer['%s'] = rospy.Timer(rospy.Duration(%s), handle_timer_%s)\n"%(_port_name, _duration, _port_name)
  node.write(var)
  
def call_srv(_name, _type, _port_name):
  var = " gd.srv['%s'] = rospy.ServiceProxy(\"%s\", %s)\n"%(_port_name, _name, _type)
  node.write(var)

def call_topic(_node_name, _name, _type, _port_name):
  var = " gd.sub['%s'] = rospy.Subscriber(\"%s\",%s,handle_topic_%s)\n"%(_port_name, _name, _type, _port_name)
  node.write(var)
  
def make_init_cb(_node_name):
  #global node
  node.write("\n")
  var = "def init():\n"
  node.write("\n#Init\n")
  node.write(var)
  #node.write("  print data\n")
  node.write("  command = str(gd.getVar('project/%s/cb/init'))\n"%(_node_name))
  node.write("  exec(command)\n")
  node.write("\n")

  
def make_init(_node_name):
  global out_srvs
  global in_srvs
  global in_topics
  global out_topics
  global out_acs
  global in_dbs
  global in_acs
  global timers
  
  node.write("def main():\n")

  var = " rospy.init_node('%s')\n"%_node_name
  node.write(var)
  var =  " gd.cb = {"
  n_iter = 0
  for x in in_dbs:
    var+= "'handle_db_%s':handle_db_%s"%(x.port_name, x.port_name)
    n_iter+=1
    if len(in_dbs) > 1 and n_iter < len(in_dbs):
      var+=","
      
  n_iter = 0    
  if len(in_dbs) > 0 and (len(in_srvs) > 0 or len(in_topics) > 0 or len(out_acs) > 0 or len(timers) > 0):
    var+=","
  
  for x in in_srvs:
    var+= "'handle_srv_%s':handle_srv_%s"%(x.port_name, x.port_name)
    n_iter+=1
    if len(in_srvs) > 1 and n_iter < len(in_srvs):
      var+=","
  
  n_iter = 0    
  if len(in_srvs) > 0 and (len(in_topics) > 0 or len(out_acs) > 0 or len(timers) > 0):
    var+=","
  
  for x in in_topics:
    var+= "'handle_topic_%s':handle_topic_%s"%(x.port_name, x.port_name)
    n_iter+=1
    if len(in_topics) > 1 and n_iter < len(in_topics):
      var+=","  
  
  n_iter = 0    
  if len(in_topics) > 0 and (len(out_acs) > 0 or len(timers) > 0):
    var+=","
    
  for x in out_acs:
    var+= "'handle_ac_%s_done':handle_ac_%s_done, 'handle_ac_%s_fb':handle_ac_%s_fb, 'handle_ac_%s_active':handle_ac_%s_active"%(x.port_name, x.port_name,x.port_name, x.port_name,x.port_name, x.port_name)
    n_iter+=1
    if len(out_acs) > 1 and n_iter < len(out_acs):
      var+="," 
  
  n_iter = 0    
  if len(out_acs) > 0 and (len(timers) > 0):
    var+=","
    
  for x in timers:
    var+= "'handle_timer_%s':handle_timer_%s"%(x.port_name, x.port_name)
    n_iter+=1
    if len(timers) > 1 and n_iter < len(timers):
      var+="," 
    
  var += "}\n"
  
  node.write(var)
  
  for x in in_srvs:
    make_srv(_node_name, x.name, x.type, x.port_name)
  
  for x in out_topics:
    print "out"
    print _node_name
    print x.name
    make_topic(_node_name, x.name, x.type, x.port_name)
  
  for x in out_srvs:
    call_srv(x.name, x.type, x.port_name)
  
  for x in in_topics:
    call_topic(_node_name, x.name, x.type, x.port_name)
  
  
  for x in out_acs:
    make_ac_client(x.name, x.type, x.port_name)
  
  for x in in_dbs:
    #~ make_db_client(x.name, x.type, x.port_name)
   
    make_db_client(x.port_name, x.type)
    
  for x in timers:
    make_timer(x.name, x.duration, x.port_name)
  
  node.write(" init()\n") #To add the init()
  node.write(" rospy.spin()\n")
  
def make_main():
  node.write("if __name__ == '__main__':\n")
  node.write("  main()\n")


def main():
  global node
  global out_srvs
  global in_srvs
  global in_topics
  global in_acs
  global out_topics
  global out_acs
  global in_dbs
  global timers
  global directory
  
  ac = actionLib()
  db = movaiDb()
  
  struct = gd.getProject()
  
  print "project structure:" 
  print struct
  #name = sys.argv[1]
  #name+=".py"
  #print name
  #node = open(name,"w")
  #make_intro()
  for x in struct["nodes"]:
    clear()
    print x["name"]
    name = x["name"]
    #name += ".py"
    if x["nodeType"] == 'SW':
      print "name " + name

      path = "%s/%s"%(directory, name)
      node = open(path, "w")
      make_intro()
      for outports in x["outports"]:
        print "---outports---"
        print outports
        outport = gd.getHash(outports["key"])
        print "outport:" 
        print outport
        if outport["port_type"] == "Topic":
          topic = Topic()
          print "outports"
          print outports["name"]
          topic.port_name = outports["name"]
          topic.name = outport["name"]
          topic.type = outport["type"]
          out_topics.append(topic)
        elif outport["port_type"] == "Service":
          srv = Service()
          srv.port_name = outports["name"]
          srv.name = outport["name"]
          srv.type = outport["type"]
          out_srvs.append(srv)
        elif outport["port_type"] == "Actionlib":
          ac.port_name = outports["name"]
          ac.name = outport["name"]
          ac.type = outport["type"]
          make_ac_cb(x["name"] , ac.name.replace('/','_'), ac.type)
          out_acs.append(ac)
        
      for inports in x["inports"]:
        print "---inports---"
        print inports
        inport = gd.getHash(inports["key"])
        print "---inport---"
        print inport
        if inport["port_type"] == "Topic":
          print "This is an input topic"
          topic = Topic()
          topic.port_name = inports["name"]
          topic.name = inport["name"]
          topic.type = inport["type"]
          make_topic_cb(x["name"], topic.port_name , topic.type)
          in_topics.append(topic)
        if inport["port_type"] == "Service":
          srv = Service()
          srv.port_name = inports["name"]
          srv.name = inport["name"]
          srv.type = inport["type"]
          make_srv_cb(x["name"], srv.port_name , srv.type)
          in_srvs.append(srv)
        if inport["port_type"] == "Actionlib":
          ac.name = inport["name"]
          ac.type = inport["type"]
          in_acs.append(ac)
        if inport["port_type"] == "Database":
          db = movaiDb()
          db.port_name = inports["name"]
          db.name = inport["name"]
          db.type = inport["type"]
          make_db_cb(x["name"], db.port_name, db.type)
          in_dbs.append(db)
        if inport["port_type"] == "Timer":
          timer = Timer()
          timer.port_name = inports["name"]
          timer.name = inport["name"]
          timer.duration = inport["type"]
          make_timer_cb(x["name"], timer.port_name, inports["name"], timer.duration)
          timers.append(timer)
      print "-----"
  
      make_init_cb(x["name"])
      make_init(x["name"])
      make_init_cb(x["name"])
      make_main()
      node.close()
      os.chmod(path, 0777)

if __name__ == '__main__':
  directory = sys.argv[1]
  main()  
  #node.close() 
