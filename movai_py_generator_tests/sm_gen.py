#!/usr/bin/env python
# -*- coding: utf-8 -*-

import roslib; roslib.load_manifest('package')
from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from states_folder.GoTo_gen import GoToState


###############################################
#                                             #
#     THIS CODE WAS AUTO-GENERATED FROM       #
#        MOV.AI SOFTWARE FOR ROBOTICS         #
#     FOR MORE INFORMATION VISIT MOV.AI       #
#                                             #
###############################################


class MyFirstProjectSM(Behavior):
	def __init__(self):
		super(MyFirstProjectSM, self).__init__()

	def create(self):
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])
		# userdata should be placed here if used
		# _state_machine.userdata.agv_name = self.agvname

		with _state_machine:
			# x:10 y:20
			OperatableStateMachine.add('Going to A',
										GoToState(log_description='moving...',log_level='0', timeout=40),
										transitions={'success': 'Wait victory', 'failed': 'Go B'},
										autonomy={'success': Autonomy.Off, 'failed': Autonomy.Off})

		# x:123 y:145
		_sm_container_test = OperatableStateMachine(outcomes=['success', 'failed'])

		with _sm_container_test:
			# x:15 y:25
			OperatableStateMachine.add('Going to B',
										GoToState(log_description='moving...',log_level='0', timeout=40),
										transitions={'success': 'Wait victory', 'failed': 'Go B'},
										autonomy={'success': Autonomy.Off, 'failed': Autonomy.Off})

		return _state_machine