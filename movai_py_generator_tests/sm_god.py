#!/usr/bin/env python

import json

'''
Helper class to store the needed values of each state of the state machine 
'''
class SMState():
    def __init__(self):
        self.name = ''
        self.node_name = ''
        self.x = ''
        self.y = ''
        self.parameters = list()  #helper
        self.params = ''
        self.outport = list()   #helper
        self.outcomes = list()
        self.transitions = ''
        self.container = ''   #is this needed???


'''
Class that generates a Smach state machine for the Mov.ai software  
'''
class GenerateSM():

    def __init__(self):
        self.states_list = list()
        self.states_names = list()

    def head(self):
        s = "#!/usr/bin/env python\n"
        s+= "# -*- coding: utf-8 -*-\n\n"
        #s+= "import roslib; roslib.load_manifest('%s')\n"%(_package) #OLD flexbe version
        s+= "from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger\n"
        return s

    def state_import(self, _states, _folder):
        s = ""
        for state in _states:
            s+= "from %s."%(_folder) + state +" import " + state + "State\n"
        return s
    

    def information(self):
        s="\n\n"
        s+= "###############################################\n"
        s+= "#                                             #\n"
        s+= "#     THIS CODE WAS AUTO-GENERATED FROM       #\n"
        s+= "#        MOV.AI SOFTWARE FOR ROBOTICS         #\n"
        s+= "#     FOR MORE INFORMATION VISIT MOV.AI       #\n"
        s+= "#                                             #\n"
        s+= "###############################################\n"
        s+= "\n\n"
        return s 


    def init_class(self, _project_name):
        s= "class %sSM(Behavior):\n"%(_project_name)
        s+= "\tdef __init__(self):\n"
        s+= "\t\tsuper(%sSM, self).__init__()\n\n"%(_project_name)
        return s

    def init_sm(self):   #like a container withour the create(self) part
        s= "\tdef create(self):\n"
        s+= "\t\t_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])\n"
        s+= "\t\t# userdata should be placed here if used\n"
        s+= "\t\t# _state_machine.userdata.agv_name = self.agvname\n\n"
        s+= "\t\twith _state_machine:\n"
        return s
        # x:441 y:597, x:1041 y:496 what is this?


    def sm_container(self, _name, _outcomes, _xpos, _ypos):
        s = "\t"*2  + "# x:%s y:%s\n"%(_xpos, _ypos)
        s+= "\t"*2  + "_sm_%s = OperatableStateMachine(outcomes=%s)\n\n" %(_name, _outcomes)
        s+= "\t"*2  + "with _sm_%s:\n"%(_name)
        return s

    def concurrency_container(self, _name, _outcomes, _xpos, _ypos):
        pass #create when needed

    def state(self, _node_name, _instance_name, _xpos, _ypos, _parameters, _transitions, _outcomes):
        s = "\t"*3  + "# x:%s y:%s\n"%(_xpos, _ypos)
        s+= "\t"*3  + "OperatableStateMachine.add('%s',\n"%(_instance_name)
        s+= "\t"*10 + "%sState(%s),\n"%(_node_name, _parameters)
        s+= "\t"*10 + "transitions={%s},\n"%(_transitions)
        s+= "\t"*10 + "autonomy={"  #needed?
        for out in _outcomes:
            s+="'" + out + "': Autonomy.Off"
            if out != _outcomes[-1]:
                s+= ", "
            else:
                s+= "})\n\n"
        if not _outcomes:  #temporary in case no connection was made but the state is still there
            s+= "})\n\n"
        return s

    def end_sm(self):
        return "\t"*2 + "return _state_machine"

    #Function that parses the states flow json "file" and fills the information needed in the sm of every state
    def parse_sm_json(self, _filename):
        with open(_filename) as json_data:
            data = json.load(json_data,) 
    
        self.states_names = list()  #remove
        self.states_list = list()

        for s in data['processes']:

            state = SMState()
            state.name = str(s)
            state.node_name = str(data['processes'][state.name]['component'])
            self.states_names.append(str(state.node_name))   
            state.x = str(data['processes'][state.name]['metadata']['x'])
            state.y = str(data['processes'][state.name]['metadata']['y'])

            for idx, p in enumerate(data['processes'][state.name]['parameters']):
                state.parameters.append(str(data['processes'][state.name]['parameters'][p]))

                value = data['processes'][state.name]['parameters'][p]
                
                if type(value) is unicode or type(value) is str:
                    state.params += p + "=" + "'%s'"%(value)
                else:
                    state.params += p + "=" + "%s"%(value)
                if idx != len(data['processes'][state.name]['parameters'])-1:
                    state.params += ", "

            self.states_list.append(state)

        # Check all connections and create two lists. 
        # Outport: the port in the current state where transitions inits
        # Outcomes: the state the transitions goes into
        for c in data['connections']:
            for state in self.states_list:
                if c['src']['process'] == state.name:
                    state.outcomes.append(str(c['tgt']['process']))
                    state.outport.append(str(c['src']['port']))

        #Create the "_transitions" string needed for state creation
        for state in self.states_list:
            for idx, out in enumerate(state.outcomes):
                state.transitions += "'%s': '%s'"%(state.outport[idx], out)
                if idx != len(state.outcomes)-1:
                    state.transitions += ", "


    def generate(self, _project_name, _json):

        states_folder = 'movai_states'
        self.parse_sm_json(_json)

        file = open('%s_sm.py'%_project_name, "w")
        file.write(self.head())
        file.write(self.state_import(self.states_names, states_folder))
        file.write(self.information())
        file.write(self.init_class(_project_name))
        file.write(self.init_sm())

        #Create container

        #States from container/unique state machine
        for s in self.states_list:
            file.write(self.state(s.node_name, s.name, s.x, s.y, s.params, s.transitions, s.outcomes))

        #file.write(self.state(self.states_list[1].node_name, self.states_list[1].name, self.states_list[1].x, self.states_list[1].y, 
        #            self.states_list[1].params, self.states_list[1].transitions, self.states_list[1].outcomes))


        #file.write(self.sm_container("container_test",outcomes,"123","145"))
        #file.write(self.state("GoTo", "Going to B", "15", "25", parameters, transitions, outcomes))
        file.write(self.end_sm())
        file.close()






    
        