#!/usr/bin/env python


#from GD_Redis import *
#from GD import *
#import god
import rospy
#from geometry_msgs.msg import Twist
#import time
import json
from new_god import Node

if __name__ == "__main__":
    #setVariable
    rospy.init_node('Test_Generator', anonymous=True)
    
    with open('flow.json') as json_data:
        data = json.load(json_data,) 
    
    #print json.dumps(data, indent=2)

    file = open('generated.py', "w")
    node = Node()
    file.write(node.generateHead())
    file.write(node.generateImports("my_package",["UInt8","String","Bool"]))
    file.write(node.generateInformation())
    file.write(node.generateClass("HelloWorld"))

    file.write(node.generateSub("input_laser","/base_scan", "String"))
    file.write(node.generatePub("output_thing","/blocked", "Bool"))
    file.write(node.generateSrvClient("ouput_service","/srv_topic", "SetBool"))
    file.write(node.generateSrvServer("input_service","/srv_topic", "SetBool"))
    file.write(node.generateActionServer("input_action","/action_topic", "GoTo"))
    file.write(node.generateActionClient("output_action","/action_topic", "GoTo"))

    file.write(node.generateSubCb("input_laser"))
    file.write(node.generateSrvServerCb("input_service","SetBool"))
    file.write(node.generateActionServerCb("input_action","GoTo"))
    file.write(node.generateActionClientCb("output_action","GoTo"))

    file.write(node.generateMain("HelloWorld"))

    #print e
    #file.write(a+b+c+d+e+f+g+h+i+j+k+l+m+n) 
    file.close()
    rospy.spin()

