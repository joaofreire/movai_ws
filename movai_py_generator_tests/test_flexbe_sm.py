#!/usr/bin/env python

import rospy
import json
from sm_god import GenerateSM

class SMState():
    def __init__(self):
        self.name = ''
        self.node_name = ''
        self.x = ''
        self.y = ''
        self.parameters = list()
        self.params = ''
        self.transit = list()
        self.outcomes = list()
        self.transitions = ''


if __name__ == "__main__":
    #setVariable
    rospy.init_node('Test_Generator_SM', anonymous=True)
    '''
    with open('flow.json') as json_data:
        data = json.load(json_data,) 
    
    #print json.dumps(data, indent=2)
    
    states_names = list()
    states_list = list()

    for s in data['processes']:
        states_names.append(str(s))

        state = SMState()
        state.name = str(s)
        state.node_name = str(data['processes'][state.name]['component'])
        state.x = str(data['processes'][state.name]['metadata']['x'])
        state.y = str(data['processes'][state.name]['metadata']['y'])


        for idx, p in enumerate(data['processes'][state.name]['parameters']):
            state.parameters.append(str(data['processes'][state.name]['parameters'][p]))

            value = data['processes'][state.name]['parameters'][p]
            
            if type(value) is unicode or type(value) is str:
                state.params += p + "=" + "'%s'"%(value)
            else:
                state.params += p + "=" + "%s"%(value)
            if idx != len(data['processes'][state.name]['parameters'])-1:
                state.params += ", "

        states_list.append(state)

    for c in data['connections']:
        for state in states_list:
            if c['src']['process'] == state.name:
                state.outcomes.append(str(c['tgt']['process']))
                state.transit.append(str(c['src']['port']))

    for state in states_list:
        for idx, out in enumerate(state.outcomes):
            state.transitions += "'%s': '%s'"%(state.transit[idx], out)
            if idx != len(state.outcomes)-1:
                state.transitions += ", "


    # PRINT ALL VALUES OFF THE STATE 

    for elem in states_list:
        attrs = vars(elem)
        print ', '.join("%s: %s" % item for item in attrs.items())

    
    # THING NEEDED TO CREATE THE SM
    # PROJECT_NAME
    # STATES FOLDER
    # LIST OF STATES NAMES AND RESPECTIVE FOLDER
    # EACH STATE INSTANCE
    #   - Name
    #   - State Name
    #   - x and y
    #   - parameters
    #   - transitions
    #   - outcomes

    states_folder = 'movai_states'
    project_name = 'MyFirstProject'
    states_names = ["GoTo", "Grab"]

    parameters = "log_description='moving...',log_level='0', timeout=40"
    transitions = "'success': 'Wait victory', 'failed': 'Go B'"
    outcomes = ["success", "failed"]

    file = open('%s_sm.py'%project_name, "w")
    sm = GenerateSM()
    file.write(sm.head())
    file.write(sm.state_import(states_names, states_folder))
    file.write(sm.information())
    file.write(sm.init_class(project_name))
    file.write(sm.init_sm())
    file.write(sm.state("GoTo", "Going to A", "10", "20", parameters, transitions, outcomes))
    #State from json
    file.write(sm.state(states_list[1].node_name, states_list[1].name, states_list[1].x, states_list[1].y, states_list[1].params, states_list[1].transitions, states_list[1].outcomes))


    file.write(sm.sm_container("container_test",outcomes,"123","145"))
    file.write(sm.state("GoTo", "Going to B", "15", "25", parameters, transitions, outcomes))
    file.write(sm.end_sm())
    file.close()
    '''
    sm = GenerateSM()
    sm.generate("MovAiTests", 'movai_flow.json')

    rospy.spin()

