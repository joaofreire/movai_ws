#!/usr/bin/env python

import rospy
import json
from node_god import GenerateNode

if __name__ == "__main__":
    #setVariable
    rospy.init_node('Test_Generator', anonymous=True)
    
    node = GenerateNode()

    node.parse_node_json('library_flexbe.json')

    node.generate_nodes()
    rospy.spin()

