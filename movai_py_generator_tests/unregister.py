#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from GD import GD
from std_msgs.msg import Bool 
from std_srvs.srv import Empty 
from std_srvs.srv import SetBool, SetBoolRequest, SetBoolResponse 


###############################################
#                                             #
#     THIS CODE WAS AUTO-GENERATED FROM       #
#        MOV.AI SOFTWARE FOR ROBOTICS         #
#     FOR MORE INFORMATION VISIT MOV.AI       #
#                                             #
###############################################


gd = GD('GripperGen')

class GripperGenNode(object):
	'''
	Generated Gripper State
	'''

	def __init__(self, name):
		# Init Imputs
		gd.sub['up_sensor'] = rospy.Subscriber("GripperGen/up_sensor", Bool, self.up_sensor_cb)
		gd.sub['down_sensor'] = rospy.Subscriber("GripperGen/down_sensor", Bool, self.down_sensor_cb)
		# Init Outputs
		gd.srv_client['attach_gripper'] = rospy.ServiceProxy("GripperGen/attach_gripper", SetBool)
		gd.srv_client['center_gripper'] = rospy.ServiceProxy("GripperGen/center_gripper", Empty)
		exec(str(gd.getCb('project/GripperGen/cb/init')))


	# Subscriber callback for input 'up_sensor' 
	def up_sensor_cb(self, data):
		gd.setVarNode('up_sensor', data)
		exec(str(gd.getCb('project/GripperGen/cb/up_sensor')))
		gd.sub['down_sensor'].unregister()

	# Subscriber callback for input 'down_sensor' 
	def down_sensor_cb(self, data):
		gd.setVarNode('down_sensor', data)
		print 'DOWN'
		exec(str(gd.getCb('project/GripperGen/cb/down_sensor')))

if __name__ == '__main__':
	rospy.init_node('GripperGen')
	node = GripperGenNode(rospy.get_name())
	rospy.spin()
