// Init Websockets
var socket = io.connect("http://10.10.0.60:5000");

var policyPath = "http://10.10.0.60:5000/policy"

//Init CodeMirror
var myCodeMirror = null;

//Variables
var isRunning = false;


////////////////// SOCKETS //////////////////

//Socket to update the code of the software node
socket.on('cb_code_update', function(data) {
    
    $("#cb_code").val(data)
    
    myCodeMirror.getDoc().setValue(data);
    myCodeMirror.setSize($("#div-code-input").width(), $(window).height() * 0.6);
    myCodeMirror.refresh();
    
});

//Socket to update the lib and the graph
socket.on('load_project', function(data) {

    var graphData = data.graph;
    appState.library = data.lib;
    
    fbpGraph.graph.loadJSON(graphData, function(err, graph) {
        appState.graph = graph;
        appState.graph.on('endTransaction', renderApp); // graph changed
        renderApp();
    });
});

//Socket to update the terminal
socket.on('terminal_cb', function(data) {
    console.log(data);
    $('#terminal').append(data)
    $("#terminal").scrollTop($("#terminal")[0].scrollHeight);
});

//Socket to updates the ui when the project is stopped
socket.on('cb_stop_project', function(data) {
    isRunning = false;
    $('#nav-run').text("Run")
});

//Socket to open an alert pop-up
socket.on('alert_popup', function(data) {
    console.log(data)
    createAlert(data["title"], data['msg'])
});

////////////////// FUNCTIONS //////////////////

/**
 * Function to create a Alert pop-up
 **/ 
function createAlert(title, msg){
    $('#alert_title').text(title);
    $('#alert_msg').text(msg);
    $("#modalAlert").modal('show');
}
 

/**
 * Clear the terminal
 **/
function clearTerminal() {
    $('#terminal').empty();
}

/**
 * Open the terminal window
 **/
function openTerminal() {

    $("#modalTerminal").modal('show');
}


/**
 * To send to the server to study the flo-graph connections
 **/
function studyNet() {
    console.log("Clic - Study Flo");
    data = {
        graph: JSON.stringify(appState.graph)
    }
    socket.emit('study_net', data);
}


/**
 * Send to the server delete a node in Redis
 **/
function deleteNodeServer(component) {
    console.log("Clic - Delete Node in Server");
    socket.emit('delete_node', component);
    saveProject();
}


/**
 * Send the code for save the callback code of a sw node
 **/
function saveCallback() {
    console.log("Clic - Save Code");
    var data = {
        nodename: $("#edit_node_name").text(),
        callback: $("#cb_data").text(),
        code: myCodeMirror.getValue()
    };
    socket.emit('save_cb', data);
}

/**
 * Send all data to save in Redis 
 **/
function saveProject() {
    console.log("Clic - Save Project");
    var graph = JSON.stringify(appState.graph)
    var lib = JSON.stringify(appState.library)
    data = {
        graph: graph,
        lib: lib
    }
    socket.emit('saveproject', data);
}

/**
 * Send a request to load the project
 **/
function loadProject() {
     console.log("Clic - Load Project");
    socket.emit('loadproject', "");
}

/**
 * Send a request to creates the python files in the server
 **/
function god() {
    console.log("Clic - Create Project");
    data = {}
    socket.emit('createFiles', data);
}


/**
 * Send a request to Run the project
 **/
function run() {
    if (!isRunning) {
        //Run
        console.log("Clic - Run Project");
        data = {
            graph: JSON.stringify(appState.graph),
            lib: JSON.stringify(appState.library)
        }

        socket.emit('runcode_cb', data);
        isRunning = true;
        $('#nav-run').text("Stop")
        
    } else {
        //Stop 
        console.log("Clic - Stop Project");
        socket.emit('stopcode_cb', "");
        isRunning = false;
        $('#nav-run').text("Run")
    }
}

/**
 * Read Map From your computer
 **/ 
function readSingleFileMap(e) {
  var file = e.target.files[0];
  if (!file) {
    return;
  }
  var reader = new FileReader();
  reader.onload = function(e) {
    var contents = e.target.result;
    displayContents(contents);
  };
  reader.readAsText(file);
}

/**
 * Upload the map in the database throught Server
 **/ 
function uploadMap(){

    if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
      alert('The File APIs are not fully supported in this browser.');
      return;
    }   

    input_pgm = document.getElementById('file-input-pgm');
    input_yaml = document.getElementById('file-input-yaml');
    
    if (!input_pgm && !input_yaml) {
      alert("Um, couldn't find the fileinput element.");
    }
    else if (!input_pgm.files && !input_yaml.files) {
      alert("This browser doesn't seem to support the `files` property of file inputs.");
    }
    else if (!input_pgm.files[0] && !input_yaml.files[0]) {
      alert("Please select a file before clicking 'Load'");               
    }
    else {
      file_pgm = input_pgm.files[0];
      file_yaml = input_yaml.files[0];
      var fileName_pgm = input_pgm.files[0].name;
      fr = new FileReader();
      fr.onload = function(e) {
        var contents = e.target.result;
        fr2 = new FileReader();
        fr2.onload = function(e2) {
            
             var fileName_yaml = input_pgm.files[0].name;
             var contents2 = e2.target.result;
             data = {
                name_pgm: fileName_pgm,
                pgm:contents,
                yaml:contents2,
                name_yaml: fileName_yaml};
            socket.emit('save_map', data);
            input_yaml.value = "";
            input_pgm.value = "";
        }
        fr2.readAsArrayBuffer(file_yaml);
      };
      fr.readAsArrayBuffer(file_pgm);
    }
    
}

/**
 * Save map in DB
 **/ 
function saveMap(){
    $('#modalMap').modal('show')
}

/**
 * Download the map in the server
 **/ 
function getMap(){
    socket.emit('get_map', {});
}
/**
 * Request to delete all in Redis and the server
 **/
function deleteAllInServer() 
{
    socket.emit('deleteall_cb', "");
}

/**
 * Request to create a Node in the server
 **/
function createNodeInServer(json) 
{
    data = json;
    socket.emit('createNode_cb', data);
}

/**
 * Show modal and reset to create a new SW node
 **/
function showModalCreateNode(){

    $("#modalCustomNode").modal('show');
    $("#input-node-name").val("");
    $("#input-pkg").val("");
    $("#input-args").val("");
    $("#input-file").val("");
    $('#checkbox_hw').prop('checked', false);

    clearTable(document.getElementById("table-outports"));
    clearTable(document.getElementById("table-inports"));
    clearTable(document.getElementById("table-params"));
}

/**
 * Create a new node in the graph, and after send it to the server
 **/
function createNode() 
{
    var name = $("#input-node-name").val();
    var isHw = $('#selector-node-type').val() === 'HW';
    var isROS = $('#selector-node-type').val() === 'ROS';
    var isState = $('#selector-node-type').val() === 'STATE';
    var args = $('#input-args').val()


    var inportsList = getInportsOrOutportsList('table-inports-tbody');
    var outportsList = getInportsOrOutportsList('table-outports-tbody');
    var paramsList = getParamsList('table-params-tbody');

    //Add port Init in SW nodes
    if (!isHw && !isROS) 
    {
        inportsList.push(
        {
            'name': "init",
            'port_type': "DB",
            "type": "String"
        });
        inportsList.push({
            'name': "exit",
            'port_type': "DB",
            "type": "String"
        }); 
    }

    if(isState)
    {
        inportsList.push(
        {
            'name': "start",
            'port_type': "Transition",
            "type": ""
        });
        outportsList.push({
            'name': "sucess",
            'port_type': "Transition",
            "type": ""
        }); 
    }
    //Only ROS NODES
    var pkg = ""
    var file = ""
    if (isROS) 
    {
        pkg = $('#input-pkg').val();
        file = $('#input-file').val();

    }

    var component = {};
    //var id = Math.round(Math.random() * 100000).toString(36);
    var id = name;
    component = 
    {
        name: name,
        description: name + "_" + id,
        icon: 'cog',
        inports: inportsList,
        outports: outportsList,
        nodeType: $('#selector-node-type').val(),
        params: paramsList,
        args: args,
        pkg: pkg,
        file: file
    };
    
    
    var isEdit = false;
    //Danger! here override the nodes
    if (appState.library[name] == null) 
    {

        //Dont do anything

    } 
    else 
    {
        isEdit = true
        //1. Delete node previous in the library
        appState.graph.removeNode(name);
        deleteNodeServer(appState.library[name]);
        delete appState.library[name]; //To delete from JSON
        appState.library = appState.library

    }
    appState.library[name] = component;

    console.log(appState.graph);

    var metadata = {
        label: name,
        x: Math.round(Math.random() * 800),
        y: Math.round(Math.random() * 600)
    };

    appState.graph.addNode(name, name, metadata);
    // GIVE DE ID to save better
    //editor.nofloGraph.addNode(id, name, metadata);

    //Create Node in the server
    createNodeInServer(JSON.stringify(component));


    //Save and Load
    saveProject();
    if (isEdit) 
    {
        location.reload();
    }


    //getComponents(projectName);
}


/**
 * Create a Selector for ROS Msg type: Std_msgs.msg
 **/
function createSelectorRosMSG() {
    var selector = "<select class='form-control' id='selector-ros-msgs'><option value='std_msgs.msg.String'>String</option><option value='std_msgs.msg.Bool'>Bool</option><option value='std_msgs.msg.Byte'>Byte</option><option value='std_msgs.msg.ByteMultiArray'>ByteMultiArray</option> <option value='std_msgs.msg.Char'>Char</option> <option value='std_msgs.msg.ColorRGBA'>ColorRGBA</option> <option value='std_msgs.msg.Duration'>Duration</option> <option value='std_msgs.msg.Empty'>Empty</option> <option value='std_msgs.msg.Float32'>Float32</option> <option value='std_msgs.msg.Float32MultiArray'>Float32MultiArray</option> <option value='std_msgs.msg.Float64'>Float64</option> <option value='std_msgs.msg.Float64MultiArray'>Float64MultiArray</option> <option value='std_msgs.msg.Header'>Header</option> <option value='std_msgs.msg.Int16'>Int16</option> <option value='std_msgs.msg.Int16MultiArray'>Int16MultiArray</option> <option value='std_msgs.msg.Int32'>Int32</option> <option value='std_msgs.msg.Int32MultiArray'>Int32MultiArray</option><option value='std_msgs.msg.Int64'>Int64</option><option value='std_msgs.msg.Int64MultiArray'>Int64MultiArray</option> <option value='std_msgs.msg.Int8'>Int8</option> <option value='std_msgs.msg.Int8MultiArray'>Int8MultiArray</option> <option value='std_msgs.msg.MultiArrayDimension'>MultiArrayDimension</option> <option value='std_msgs.msg.MultiArrayLayout'>MultiArrayLayout</option> <option value='std_msgs.msg.String'>String</option>	 <option value='std_msgs.msg.Time'>Time</option>	<option value='std_msgs.msg.UInt16'>UInt16</option>	<option value='std_msgs.msg.UInt16MultiArray'>UInt16MultiArray</option>	<option value='std_msgs.msg.UInt32'>UInt32</option>	<option value='std_msgs.msg.UInt32MultiArray'>UInt32MultiArray</option>	<option value='std_msgs.msg.UInt64'>UInt64</option>	<option value='std_msgs.msg.UInt64MultiArray'>UInt64MultiArray</option>	<option value='std_msgs.msg.UInt8'>UInt8</option>	<option value='std_msgs.msg.UInt8MultiArray'>UInt8MultiArray</option></select>";
    return selector;
}

/**
 * Create a selector for port type as Topic, Service, ActionLib, Timer
 **/
function createSelectorPortType() {
    var selector = "<select class='form-control' id='selector-port-type'><option value='Actionlib'>Actionlib</option><option value='Database'>Database</option><option value='Service'>Service</option><option value='Timer'>Timer</option><option value='Topic'>Topic</option><option value='Transition'>Transition</option></select>";
    return selector;
}

/**
 * 
 * Create a selector for node library 
 */
function createSelectorNodeType()
{
    socket.emit('get_nodes_library', "");
    socket.on('get_nodes_library_reply', function (data) {
        console.log(data);
      });
}


/**
 * Function to delete the rows table
 **/ 
function clearTable(table) {
    var rows = table.rows;
    var i = table.rows.length;
    while (--i) {
        rows[i].parentNode.removeChild(rows[i]);
    }
}

/**
 * Get a list of inports and outports of each node
 **/ 
function getInportsOrOutportsList(tableTbodyName) {
    var table = document.getElementById(tableTbodyName);

    if (table == null || table.rows.length == 0) {
        return [];
    }
    var list = [];
    for (var i = 0; i < table.rows.length; i++) {
        var portName = table.rows[i].cells[0].children[0].value;
        var portType = table.rows[i].cells[2].children[0].value;
        var portPortType = table.rows[i].cells[1].children[0].value;
        list.push({
            'name': portName,
            'type': portType,
            'port_type': portPortType
        });
    }

    return list;
}

/**
 * Get a list of params of a node
 **/ 
function getParamsList(tableTbodyName) {
    var table = document.getElementById(tableTbodyName);

    if (table == null || table.rows.length == 0) {
        return [];
    }
    var list = [];
    for (var i = 0; i < table.rows.length; i++) {
        var paramName = table.rows[i].cells[0].children[0].value;
        var paramVal = table.rows[i].cells[1].children[0].value;
        list.push({
            'name': paramName,
            'value': paramVal,
        });
    }

    return list;
}

/**
 * Add a new port "row" (inport or outport) for a table when the user creates a new Node
 */  
function addPort(table_name, tbody_name) {

    var table = document.getElementById(table_name);
    var tbody = document.getElementById(tbody_name);

    var row_num = table.rows.length;
    var row = table.insertRow(table.rows.length);
    var cell0 = row.insertCell(0); //Order
    cell0.innerHTML = "<input type='text' class='form-control' id='name-port'>";
    cell0.setAttribute("class", "col-md-4");


    var cell1 = row.insertCell(1); //name
    cell1.innerHTML = createSelectorPortType();
    cell1.setAttribute("class", "col-md-3");

    var cell2 = row.insertCell(2); //name
    cell2.innerHTML = "<input type='text' class='form-control' id='type-port'>";
    //cell2.innerHTML = createSelectorRosMSG();
    cell2.setAttribute("class", "col-md-3");

    var cell3 = row.insertCell(3);
    console.log("ROW num" + row_num);
    cell3.innerHTML = "<button type='button' class='btn btn-danger btn-sm' onclick='deleteRowTable(\"" + table_name + "\",  this)'><span class='glyphicon glyphicon-trash'></span></button>";
    cell3.setAttribute("class", "col-md-2");
    tbody.appendChild(row);
}


/**
 * Add a new row for the table params
 **/ 
function addParam(table_name, tbody_name) {

    var table = document.getElementById(table_name);
    var tbody = document.getElementById(tbody_name);

    var row_num = table.rows.length;
    var row = table.insertRow(table.rows.length);
    var cell0 = row.insertCell(0); //Name
    cell0.innerHTML = "<input type='text' class='form-control' id='name-param'>";
    cell0.setAttribute("class", "col-md-4");
    var cell1 = row.insertCell(1); //Value
    cell1.innerHTML = "<input type='text' class='form-control' id='value-param'>";
    cell1.setAttribute("class", "col-md-4");

    var cell2 = row.insertCell(2);
    cell2.innerHTML = "<button type='button' class='btn btn-danger btn-sm' onclick='deleteRowTable(\"" + table_name + "\",this)'><span class='glyphicon glyphicon-trash'></span></button>";
    cell2.setAttribute("class", "col-md-2");
    tbody.appendChild(row);
}



/**
 * Edit the code of a SW node
 **/  
function editCode(component) 
{

    if (component["nodeType"] === 'SW') 
    {
        clearModalEditCode();

        for (var i = 0; i < component.inports.length; i++) 
        {
            var inport = component.inports[i]
            console.log(inport)
            var html_code = "<li ><a id=\"inport-" + inport["name"] + "\" href=\"#\">" + inport["name"] + "</a></li>";
            $("#nav-node-inputs").append(html_code);
        }
        for (var i = 0; i < component.outports.length; i++) 
        {
            var outport = component.outports[i]
            var html_code = "<li ><a id=\"outport-" + outport["name"] + "\" href=\"#\">" + outport["name"] + "</a></li>";
            $("#nav-node-outputs").append(html_code);
        }

        getCallbackCode(component.name, "start")
        $("#cb_data").text("start");
        $("#edit_node_name").text(component.name)

        $("#modalEditNode").modal('show');
    }
    else 
    {

        alert('It is not possible to show the code, because this node is not a SW node')
    }
}

/**
 * Clear the modal for a Edit Code modal
 **/ 
function clearModalEditCode()
{

    $("#nav-node-inputs li").remove();
    $("#nav-node-outputs li").remove();

}

/**
 * Request to the server for SW node code
 **/ 
function getCallbackCode(nodename, inportname) 
{
    var data = {
        nodename: nodename,
        inportname: inportname
    };
    socket.emit('get_cb_code', data);
}

/**
 * Get Event Target
 **/ 
function getEventTarget(e) 
{
    e = e || window.event;
    return e.target || e.srcElement;
}


/**
 * Open a new window with the Redis Database View
 **/  
function openDatabaseView() 
{

    window.open("http://10.10.0.60:4567/");
}

/**
 * Open a new window with the Redis Database View
 **/  
function openPolicyView()
 {

    window.open(policyPath);
}


/**
 * Delete a row in a table
 **/
function deleteRowTable(tablename, r) 
{
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById(tablename).deleteRow(i);
}


/**
 * Edit a Node, adding new inports or outpors, params, change type etc..
 * It shows the modal with the component values
 **/ 
function editNode(component)
 {
    //Clear
    $("#input-node-name").val("");
    $("#input-pkg").val("");
    $("#input-file").val("");
    $("#input-args").val("");

    clearTable(document.getElementById("table-outports"));
    clearTable(document.getElementById("table-inports"));
    clearTable(document.getElementById("table-params"));

    //edit fields
    $("#input-node-name").val(component["name"]);
    $("#selector-node-type").val(component["nodeType"]);
    $("#input-pkg").val(component["pkg"]);
    $("#input-file").val(component["file"]);
    $("#input-args").val(component["args"]);


    if (component["nodeType"] === 'ROS') 
    {
        $('#form-type-node').show()
    } 
    else 
    {
        $('#form-type-node').hide()
    }

    var inports = component["inports"]
    for (var i = 0; i < inports.length; i++) 
    {
        if (inports[i]["name"] !== 'init') 
        {
            console.log("Add port")
            addPortEdit('table-inports', 'table-inports-tbody', inports[i])
        }
    }

    var outports = component["outports"]
    for (var i = 0; i < outports.length; i++) 
    {
        addPortEdit('table-outports', 'table-outports-tbody', outports[i])
    }
    var params = component["params"]
    if (params != null) 
    {
        for (var i = 0; i < params.length; i++) 
        {
            addParamEdit('table-params', 'table-params-tbody', params[i])
        }
    }
    $("#modalCustomNode").modal('show');

}


/**
 * Add port in a row table with data
 **/ 
function addPortEdit(table_name, tbody_name, port) 
{

    var table = document.getElementById(table_name);
    var tbody = document.getElementById(tbody_name);

    var row_num = table.rows.length;
    var row = table.insertRow(table.rows.length);
    var cell0 = row.insertCell(0); //Order
    cell0.innerHTML = "<input type='text' class='form-control' id='name-port' value='" + port["name"] + "'>";
    cell0.setAttribute("class", "col-md-4");


    var cell1 = row.insertCell(1); //name
    cell1.innerHTML = createSelectorPortType();
    cell1.setAttribute("class", "col-md-3");
    //cell1.setAtrribute("value", port["port_type"])

    var cell2 = row.insertCell(2); //name
    cell2.innerHTML = "<input type='text' class='form-control' id='type-port' value='" + port["type"] + "'>";
    //cell2.innerHTML = createSelectorRosMSG();
    cell2.setAttribute("class", "col-md-3");

    var cell3 = row.insertCell(3);
    cell3.innerHTML = "<button type='button' class='btn btn-danger btn-sm' onclick='deleteRowTable(\"" + table_name + "\", this)'><span class='glyphicon glyphicon-trash'></span></button>";
    cell3.setAttribute("class", "col-md-2");

    row.cells[1].children[0].value = port["port_type"];
    tbody.appendChild(row);

}

/**
 * Add poram in a row table with data
 **/ 
function addParamEdit(table_name, tbody_name, param) 
{

    var table = document.getElementById(table_name);
    var tbody = document.getElementById(tbody_name);

    var row_num = table.rows.length;
    var row = table.insertRow(table.rows.length);
    var cell0 = row.insertCell(0); //Name
    cell0.innerHTML = "<input type='text' class='form-control' id='name-param' value='" + param["name"] + "'>";
    cell0.setAttribute("class", "col-md-5");
    var cell1 = row.insertCell(1); //Value
    cell1.innerHTML = "<input type='text' class='form-control' id='value-param' value='" + param["value"] + "'>";
    cell1.setAttribute("class", "col-md-5");

    var cell2 = row.insertCell(2);
    cell2.innerHTML = "<button type='button' class='btn btn-danger btn-sm' onclick='deleteRowTable(\"" + table_name + "\",this)'><span class='glyphicon glyphicon-trash'></span></button>";
    cell2.setAttribute("class", "col-md-2");
    tbody.appendChild(row);
}


$(document).ready(function() 
{
    
    // UL- NAV - Inporst
    var ul = document.getElementById('nav-node-inputs');
    ul.onclick = function(event) 
    {
        var target = getEventTarget(event);
        var inport = target.innerHTML;
        var nodename = $("#edit_node_name").text()
        getCallbackCode(nodename, inport);
        $("#cb_data").text(inport)

    };

    //Trigger on Change the selector type of a new node, adding 2 forms
    $('#selector-node-type').on('change', function()
     {
        if (this.value === 'ROS') 
        {
            $('#form-type-node').show()
        } 
        else 
        {
            $('#form-type-node').hide()
        }
    });

    //Code mirror init to show the code more beautify
    myCodeMirror = CodeMirror.fromTextArea(document.getElementById("cb_code"), 
    {
        mode: 'python',
        theme: "ambiance",
        lineNumbers: true,
        matchBrackets: true
        
    });
});



// Start the app with a request for the data
loadProject();
